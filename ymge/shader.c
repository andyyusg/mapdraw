#include <stdlib.h>

#include <GL/glew.h>

#include <file.h>
#include <logger.h>

#include "ymge_error.h"
#include "shader.h"


//return shader id
//type:
// GL_VERTEX_SHADER
// GL_FRAGMENT_SHADER
static uint shader_compile(shader_source_t* src)
{
	GLuint			shader = 0;
	file_t			file = { 0 };
	buf_t*			code = 0;
	const char*		codes[1];
	GLint			result = 0;

	//create shader
	shader = glCreateShader(src->type);
	if (shader == 0)
	{
		LOG4("[shader] build shader create error.");
		return 0;
	}

	//load shader from file
	if (file_open(&file, src->path, "rb"))
	{
		code = file_read_all(&file);
		file_close(&file);
	}
	else
	{
		//clean
		glDeleteShader(shader);
		LOG4("[shader] build shader read error.");
		return 0;
	}
	codes[0] = code->data;

	//specify shader source code
	glShaderSource(shader, 1, codes, 0);

	//compile
	glCompileShader(shader);

	//clean
	buf_del(code);

	//check compile result
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE)
	{
		int log_len = 0;

		LOG4("[shader] compile failed: %s", src->path);

		//get build log len
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_len);
		if (log_len > 0)
		{
			GLsizei		log_written = 0;
			char*		log = malloc(log_len);
			glGetShaderInfoLog(shader, log_len, &log_written, log);
			LOG4("[shader] compile error:\n%.*s", log_len, log);
			free(log);
		}

		//clean
		glDeleteShader(shader);
		return 0;
	}

	LOG0("[shader] compile success: %s", src->path);

	//success
	return shader;
}


//NOTE: shaders will be detached and deleted after linking
static uint shader_link(uint num_shaders, uint* shaders)
{
	char	error[256];
	uint	program = 0;
	int		result = 0;
	uint	i;

	//create program
	program = glCreateProgram();

	//attach shaders
	for (i = 0; i < num_shaders; i++)
	{
		glAttachShader(program, shaders[i]);
	}

	//link program
	glLinkProgram(program);

	//detach shaders after link
	for (i = 0; i < num_shaders; i++)
	{
		glDetachShader(program, shaders[i]);
	}

	//delete shaders after link
	for (i = 0; i < num_shaders; i++)
	{
		glDeleteShader(shaders[i]);
	}

	//check result
	glGetProgramiv(program, GL_LINK_STATUS, &result);
	if (result == GL_FALSE)
	{
		LOG4("[shader] link program error.");

		//get error text
		GLsizei len = 0;
		glGetProgramInfoLog(program, 256, &len, error);
		LOG4("[shader] link error:\n%.*s", len, error);

		//clean and return
		glDeleteProgram(program);
		return 0;
	}

	//success
	LOG0("[shader] link program OK.");
	return program;
}






uint shader_build(uint num_srcs, shader_source_t* srcs)
{
	uint* shaders = 0;
	uint program = 0;

	//calloc
	shaders = calloc(num_srcs, sizeof(uint));

	//compile shaders
	for (uint i = 0; i < num_srcs; i++)
	{
		shaders[i] = shader_compile(srcs + i);
		if (shaders[i] == 0)
		{
			free(shaders);
			return 0;
		}
	}

	//link shaders
	program = shader_link(num_srcs, shaders);

	//clean
	free(shaders);

	//success
	return program;
}



