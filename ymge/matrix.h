#pragma once

#include <intrin.h>
#include <string.h>
#include <math.h>

#include "ymge_type.h"
#include "tri.h"

// model -> world:
// mat_translate * mat_scale * mat_rotate * v_pos

// world -> view
// 

// view -> projection
//






//#define QUATERNION_DOT_THRESHOLD	0.9995f
#define QUATERNION_DOT_THRESHOLD	0.99999f



#define M44_IDENTITY \
{ \
	1, 0, 0, 0, \
	0, 1, 0, 0, \
	0, 0, 1, 0, \
	0, 0, 0, 1, \
}


extern float m44_identity[16];



inline void m44_set_identity(float* m)
{
	memcpy(m, m44_identity, 64);
}

inline void m44_transpose(float* m, float* r)
{
	float t[16];

	t[0] = m[0];
	t[1] = m[4];
	t[2] = m[8];
	t[3] = m[12];

	t[4] = m[1];
	t[5] = m[5];
	t[6] = m[9];
	t[7] = m[13];

	t[8] = m[2];
	t[9] = m[6];
	t[10] = m[10];
	t[11] = m[14];

	t[12] = m[3];
	t[13] = m[7];
	t[14] = m[11];
	t[15] = m[15];

	//copy result
	memcpy(r, t, sizeof(float) * 16);
}


//incorrect, to be obsolete
// Translate
// 1 0 0 tx
// 0 1 0 ty
// 0 0 1 tz
// 0 0 0 1
inline void m44_set_t(float* m, float tx, float ty, float tz)
{
	m[3] = tx;
	m[7] = ty;
	m[11] = tz;
}


// Translate
// 1 0 0 0
// 0 1 0 0
// 0 0 1 0
// x y z 1
inline void m44c_set_t(float* m, float* t)
{
	m[3] = t[0];
	m[7] = t[1];
	m[11] = t[2];
}

inline void m44r_set_t(float* m, float* t)
{
	m[12] = t[0];
	m[13] = t[1];
	m[14] = t[2];
}



// RotationY
// cos	0	sin	0
// 0	1	0	0
// -sin	0	cos	0
// 0	0	0	1
inline void m44_set_ry(float* m, float angle)
{
	float sin, cos;

	tri_sin_cos(angle, &sin, &cos);

	m[0] = cos;
	m[2] = sin;
	m[8] = -sin;
	m[10] = cos;
}


// Translation
// 1		0		0		0
// 0		1		0		0
// 0		0		1		0
// tx		ty		tz		1


// Rotation X
// 1		0		0		0
// 0		cosx	sinx	0
// 0		-sinx	cosx	0
// 0		0		0		1

// Rotation Y
// cosy		0		-siny	0
// 0		1		0		0
// siny		0		cosy	0
// 0		0		0		1

// Rotation Z
// cosz		sinz	0		0
// -sinz	cosz	0		0
// 0		0		1		0
// 0		0		0		1



// Rotation y.x.z * Translation
// cosy.cosz - sinx.siny.sinz		cosy.sinz + sinx.siny.cosz		-cosx.siny		0
// -cosx.sinz						cosx.cosz						sinx			0
// siny.cosz + sinx.cosy.sinz		siny.sinz - sinx.cosy.cosz		cosx.cosy		0
// tx								ty								tz				1


inline void m44r_set_rt(float* m, float* t, float* r)
{
	float sin[3], cos[3];
	int i;

	//get sin cos
	for (i = 0; i < 3; i++)
	{
		tri_sin_cos(r[i], &sin[i], &cos[i]);
	}

	m[0] = cos[1] * cos[2] - sin[0] * sin[1] * sin[2];
	m[1] = cos[1] * sin[2] + sin[0] * sin[1] * cos[2];
	m[2] = -cos[0] * sin[1];
	m[3] = 0.f;

	m[4] = -cos[0] * sin[2];
	m[5] = cos[0] * cos[2];
	m[6] = sin[0];
	m[7] = 0.f;

	m[8] = sin[1] * cos[2] + sin[0] * cos[1] * sin[2];
	m[9] = sin[1] * sin[2] - sin[0] * cos[1] * cos[2];
	m[10] = cos[0] * cos[1];
	m[11] = 0.f;

	m[12] = t[0];
	m[13] = t[1];
	m[14] = t[2];
	m[15] = 1.f;
}

inline void m44c_set_rt(float* m, float* t, float* r)
{
	float sin[3], cos[3];
	int i;

	//get sin cos
	for (i = 0; i < 3; i++)
	{
		tri_sin_cos(r[i], &sin[i], &cos[i]);
	}

	m[0] = cos[1] * cos[2] - sin[0] * sin[1] * sin[2];
	m[1] = -cos[0] * sin[2];
	m[2] = sin[1] * cos[2] + sin[0] * cos[1] * sin[2];
	m[3] = t[0];

	m[4] = cos[1] * sin[2] + sin[0] * sin[1] * cos[2];
	m[5] = cos[0] * cos[2];
	m[6] = sin[1] * sin[2] - sin[0] * cos[1] * cos[2];
	m[7] = t[1];

	m[8] = -cos[0] * sin[1];
	m[9] = sin[0];
	m[10] = cos[0] * cos[1];
	m[11] = t[2];

	m[12] = 0.f;
	m[13] = 0.f;
	m[14] = 0.f;
	m[15] = 1.f;
}

inline void m44c_set_r(float* m, float* r)
{
	float sin[3], cos[3];
	int i;

	//get sin cos
	for (i = 0; i < 3; i++)
	{
		tri_sin_cos(r[i], &sin[i], &cos[i]);
	}

	m[0] = cos[1] * cos[2] - sin[0] * sin[1] * sin[2];
	m[1] = -cos[0] * sin[2];
	m[2] = sin[1] * cos[2] + sin[0] * cos[1] * sin[2];
	m[3] = 0.f;

	m[4] = cos[1] * sin[2] + sin[0] * sin[1] * cos[2];
	m[5] = cos[0] * cos[2];
	m[6] = sin[1] * sin[2] - sin[0] * cos[1] * cos[2];
	m[7] = 0.f;

	m[8] = -cos[0] * sin[1];
	m[9] = sin[0];
	m[10] = cos[0] * cos[1];
	m[11] = 0.f;

	m[12] = 0.f;
	m[13] = 0.f;
	m[14] = 0.f;
	m[15] = 1.f;
}


// Rotation z.x.y * Translation
// cosy.cosz + sinx.siny.sinz		cosx.sinz		sinx.cosy.sinz - siny.cosz		0
// sinx.siny.cosz - cosy.sinz		cosx.cosz		siny.sinz + sinx.cosy.cosz		0
// cosx.siny						-sinx			cosx.cosy						0
// tx								ty				tz								1
//
// NOTE: this is the reverse matrix of m44_set_tr (Translation * Rotation y.x.z)
// for ray casting revert camera mouse ray from eye space to world space
inline void m44r_set_RzxyT(float* m, float* t, float* r)
{
	float sin[3], cos[3];

	//get sin cos
	tri_sin_cos(r[0], &sin[0], &cos[0]);
	tri_sin_cos(r[1], &sin[1], &cos[1]);
	tri_sin_cos(r[2], &sin[2], &cos[2]);


	m[0] = cos[1] * cos[2] + sin[0] * sin[1] * sin[2];
	m[1] = cos[0] * sin[2];
	m[2] = sin[0] * cos[1] * sin[2] - sin[1] * cos[2];
	m[3] = 0.f;

	m[4] = sin[0] * sin[1] * cos[2] - cos[1] * sin[2];
	m[5] = cos[0] * cos[2];
	m[6] = sin[1] * sin[2] + sin[0] * cos[1] * cos[2];
	m[7] = 0.f;

	m[8] = cos[0] * sin[1];
	m[9] = -sin[0];
	m[10] = cos[0] * cos[1];
	m[11] = 0.f;

	m[12] = t[0];
	m[13] = t[1];
	m[14] = t[2];
	m[15] = 1.f;
}



inline void m44c_set_RzxyT(float* m, float* t, float* r)
{
	float sin[3], cos[3];

	//get sin cos
	tri_sin_cos(r[0], &sin[0], &cos[0]);
	tri_sin_cos(r[1], &sin[1], &cos[1]);
	tri_sin_cos(r[2], &sin[2], &cos[2]);


	m[0] = cos[1] * cos[2] + sin[0] * sin[1] * sin[2];
	m[1] = sin[0] * sin[1] * cos[2] - cos[1] * sin[2];
	m[2] = cos[0] * sin[1];
	m[3] = t[0];

	m[4] = cos[0] * sin[2];
	m[5] = cos[0] * cos[2];
	m[6] = -sin[0];
	m[7] = t[1];

	m[8] = sin[0] * cos[1] * sin[2] - sin[1] * cos[2];
	m[9] = sin[1] * sin[2] + sin[0] * cos[1] * cos[2];
	m[10] = cos[0] * cos[1];
	m[11] = t[2];

	m[12] = 0.f;
	m[13] = 0.f;
	m[14] = 0.f;
	m[15] = 1.f;
}

// Rotation z.x.y Only
inline void m44_set_Rzxy_transposed(float* m, float* r)
{
	float sin[3], cos[3];

	//get sin cos
	tri_sin_cos(r[0], &sin[0], &cos[0]);
	tri_sin_cos(r[1], &sin[1], &cos[1]);
	tri_sin_cos(r[2], &sin[2], &cos[2]);


	m[0] = cos[1] * cos[2] + sin[0] * sin[1] * sin[2];
	m[1] = sin[0] * sin[1] * cos[2] - cos[1] * sin[2];
	m[2] = cos[0] * sin[1];
	m[3] = 0.f;

	m[4] = cos[0] * sin[2];
	m[5] = cos[0] * cos[2];
	m[6] = -sin[0];
	m[7] = 0.f;

	m[8] = sin[0] * cos[1] * sin[2] - sin[1] * cos[2];
	m[9] = sin[1] * sin[2] + sin[0] * cos[1] * cos[2];
	m[10] = cos[0] * cos[1];
	m[11] = 0.f;

	m[12] = 0.f;
	m[13] = 0.f;
	m[14] = 0.f;
	m[15] = 1.f;
}






inline void m44c_set_tr(float* m, float* t, float* r)
{
	float sin[3], cos[3];
	int i;

	//get sin cos
	for (i = 0; i < 3; i++)
	{
		tri_sin_cos(r[i], &sin[i], &cos[i]);
	}

	m[0] = cos[1] * cos[2] - sin[0] * sin[1] * sin[2];
	m[1] = -cos[0] * sin[2];
	m[2] = sin[1] * cos[2] + sin[0] * cos[1] * sin[2];
	m[3] = 0.f;

	m[4] = cos[1] * sin[2] + sin[0] * sin[1] * cos[2];
	m[5] = cos[0] * cos[2];
	m[6] = sin[1] * sin[2] - sin[0] * cos[1] * cos[2];
	m[7] = 0.f;

	m[8] = -cos[0] * sin[1];
	m[9] = sin[0];
	m[10] = cos[0] * cos[1];
	m[11] = 0.f;

	m[12] = 0.f;
	m[13] = 0.f;
	m[14] = 0.f;
	m[15] = 1.f;

	//calculate 4th column - translation column
	{
		__m128 t2, r1, r2, r3;
		v4 res[4];

		//load tx ty tz
		t2 = _mm_load_ps(t);

		//load rows and multiply
		r1 = _mm_mul_ps(t2, _mm_load_ps(m));
		r2 = _mm_mul_ps(t2, _mm_load_ps(m + 4));
		r3 = _mm_mul_ps(t2, _mm_load_ps(m + 8));

		//add results
		r1 = _mm_hadd_ps(r1, r2);
		r1 = _mm_hadd_ps(r1, r3);

		//store reuslt
		//layout(little endian): tx, ty, tz(part1), tz(part2)
		_mm_store_ps(res, r1);

		m[3] = res[0];
		m[7] = res[1];
		m[11] = res[2] + res[3];
	}
}


// Scale
// s
// sx 0  0  0
// 0  sy 0  0
// 0  0  sz 0
// 0  0  0  1
inline void m44_set_s(float* r, float sx, float sy, float sz)
{
	r[0] = sx;
	r[5] = sy;
	r[10] = sz;
}

// Translate * RotationY
// cos	0	sin	tx
// 0	1	0	ty
// -sin	0	cos	tz
// 0	0	0	1
inline void m44_set_t_ry(float* m, float tx, float ty, float tz, float ry)
{
	float sin, cos;

	tri_sin_cos(ry, &sin, &cos);

	m[0] = cos;
	m[2] = sin;
	m[3] = tx;
	m[7] = ty;
	m[8] = -sin;
	m[10] = cos;
	m[11] = tz;
}

inline void m44_set_t_ry_transposed(float* m, float tx, float ty, float tz, float ry)
{
	float sin, cos;

	tri_sin_cos(ry, &sin, &cos);

	m[0] = cos;
	m[2] = -sin;
	m[8] = sin;
	m[10] = cos;
	m[12] = tx;
	m[13] = ty;
	m[14] = tz;
}


// Projection Matrix
// --------------------------------------------
//	ctan/ar		0		0				0
//	0			ctan	0				0
//	0			0		-(f+n)/(f-n)	-1
//	0			0		-2*f*n/(f-n)	0
// --------------------------------------------
inline void m44_set_proj(float* m, float fov_half, float ar, float near_plane, float far_plane)
{
	float ctan = tri_ctan(fov_half);
	float f_n = far_plane - near_plane;

	//zero
	memset(m, 0, 64);

	//set value
	m[0] = ctan / ar;
	m[5] = ctan;
	m[10] = -(far_plane + near_plane) / f_n;
	m[11] = -1.f;
	m[14] = -(2.f*far_plane*near_plane) / f_n;
}




inline void m44c_set_proj(float* m, float fov_half_ctan, float ar, float near_plane, float far_plane)
{
	float f_n = far_plane - near_plane;

	//zero
	memset(m, 0, 64);

	//set value
	m[0] = fov_half_ctan / ar;
	m[5] = fov_half_ctan;
	m[10] = - (far_plane + near_plane) / f_n;
	m[11] = - (2.f*far_plane*near_plane) / f_n;
	m[14] = -1.f;
}







//matrix multiply transposed matrix 
inline void m44_x_m44t(float* a, float* b, float* r)
{
	int i;

	for (i = 0; i < 16; i += 4)
	{
		__m128 ar = _mm_load_ps(&a[i]);

		//multiplication
		__m128 t = _mm_load_ps(&b[0]);
		__m128 r1 = _mm_mul_ps(ar, t);
		//__m128 r1 = _mm_mul_ps(ar, _mm_load_ps(&b[0]));
		__m128 r2 = _mm_mul_ps(ar, _mm_load_ps(&b[4]));
		__m128 r3 = _mm_mul_ps(ar, _mm_load_ps(&b[8]));
		__m128 r4 = _mm_mul_ps(ar, _mm_load_ps(&b[12]));

		//add results
		r1 = _mm_hadd_ps(r1, r2);
		r2 = _mm_hadd_ps(r3, r4);

		//add again, store to r
		_mm_store_ps(&r[i], _mm_hadd_ps(r1, r2));
	}
}






// row matrix
// 0  1  2  3
// 4  5  6  7
// 8  9  10 11
// 12 13 14 15

// column matrix
// 0  4  8  12
// 1  5  9  13
// 2  6  10 14
// 3  7  11 15

// row matrix * column matrix = column matrix
inline void m44r_x_m44c_c(float* a, float* b, float* r)
{
	__m128 a0 = _mm_load_ps(a);
	__m128 a1 = _mm_load_ps(a + 4);
	__m128 a2 = _mm_load_ps(a + 8);
	__m128 a3 = _mm_load_ps(a + 12);

	__m128 b0 = _mm_load_ps(b);
	__m128 b1 = _mm_load_ps(b + 4);
	__m128 b2 = _mm_load_ps(b + 8);
	__m128 b3 = _mm_load_ps(b + 12);

	//column 0
	__m128 r0x = _mm_mul_ps(a0, b0);
	__m128 r1x = _mm_mul_ps(a1, b0);
	__m128 r2x = _mm_mul_ps(a2, b0);
	__m128 r3x = _mm_mul_ps(a3, b0);

	__m128 rc = _mm_hadd_ps(_mm_hadd_ps(r0x, r1x), _mm_hadd_ps(r2x, r3x));
	_mm_store_ps(r, rc);

	//column 1
	r0x = _mm_mul_ps(a0, b1);
	r1x = _mm_mul_ps(a1, b1);
	r2x = _mm_mul_ps(a2, b1);
	r3x = _mm_mul_ps(a3, b1);

	rc = _mm_hadd_ps(_mm_hadd_ps(r0x, r1x), _mm_hadd_ps(r2x, r3x));
	_mm_store_ps(r + 4, rc);

	//column 2
	r0x = _mm_mul_ps(a0, b2);
	r1x = _mm_mul_ps(a1, b2);
	r2x = _mm_mul_ps(a2, b2);
	r3x = _mm_mul_ps(a3, b2);

	rc = _mm_hadd_ps(_mm_hadd_ps(r0x, r1x), _mm_hadd_ps(r2x, r3x));
	_mm_store_ps(r + 8, rc);

	//column 3
	r0x = _mm_mul_ps(a0, b3);
	r1x = _mm_mul_ps(a1, b3);
	r2x = _mm_mul_ps(a2, b3);
	r3x = _mm_mul_ps(a3, b3);

	rc = _mm_hadd_ps(_mm_hadd_ps(r0x, r1x), _mm_hadd_ps(r2x, r3x));
	_mm_store_ps(r + 12, rc);
}



// row matrix * column matrix = row matrix
inline void m44r_x_m44c_r(float* a, float* b, float* r)
{
	__m128 a0 = _mm_load_ps(a);
	__m128 a1 = _mm_load_ps(a + 4);
	__m128 a2 = _mm_load_ps(a + 8);
	__m128 a3 = _mm_load_ps(a + 12);

	__m128 b0 = _mm_load_ps(b);
	__m128 b1 = _mm_load_ps(b + 4);
	__m128 b2 = _mm_load_ps(b + 8);
	__m128 b3 = _mm_load_ps(b + 12);

	//row 0
	__m128 r0x = _mm_mul_ps(a0, b0);
	__m128 r1x = _mm_mul_ps(a0, b1);
	__m128 r2x = _mm_mul_ps(a0, b2);
	__m128 r3x = _mm_mul_ps(a0, b3);

	__m128 rr = _mm_hadd_ps(_mm_hadd_ps(r0x, r1x), _mm_hadd_ps(r2x, r3x));
	_mm_store_ps(r, rr);

	//row 1
	r0x = _mm_mul_ps(a1, b0);
	r1x = _mm_mul_ps(a1, b1);
	r2x = _mm_mul_ps(a1, b2);
	r3x = _mm_mul_ps(a1, b3);

	rr = _mm_hadd_ps(_mm_hadd_ps(r0x, r1x), _mm_hadd_ps(r2x, r3x));
	_mm_store_ps(r + 4, rr);

	//row 2
	r0x = _mm_mul_ps(a2, b0);
	r1x = _mm_mul_ps(a2, b1);
	r2x = _mm_mul_ps(a2, b2);
	r3x = _mm_mul_ps(a2, b3);

	rr = _mm_hadd_ps(_mm_hadd_ps(r0x, r1x), _mm_hadd_ps(r2x, r3x));
	_mm_store_ps(r + 8, rr);

	//row 3
	r0x = _mm_mul_ps(a3, b0);
	r1x = _mm_mul_ps(a3, b1);
	r2x = _mm_mul_ps(a3, b2);
	r3x = _mm_mul_ps(a3, b3);

	rr = _mm_hadd_ps(_mm_hadd_ps(r0x, r1x), _mm_hadd_ps(r2x, r3x));
	_mm_store_ps(r + 12, rr);
}



inline void v4_x_m44c(float* v, float* b, float* r)
{
	__m128 vr = _mm_load_ps(v);

	//multiplication
	__m128 c0 = _mm_mul_ps(vr, _mm_load_ps(b));
	__m128 c1 = _mm_mul_ps(vr, _mm_load_ps(b + 4));
	__m128 c2 = _mm_mul_ps(vr, _mm_load_ps(b + 8));
	__m128 c3 = _mm_mul_ps(vr, _mm_load_ps(b + 12));

	//add results
	__m128 r1 = _mm_hadd_ps(c0, c1);
	__m128 r2 = _mm_hadd_ps(c2, c3);

	//add again, store to r
	_mm_store_ps(r, _mm_hadd_ps(r1, r2));
}





//-------------------------------------------------------------------------------------------
// Quaternion

//angle:	w
//axis:		xyz (must be unit vector)
inline void quaternion(float* q, float degree, float* axis)
{
	float sin, cos;

	tri_sin_cos(degree/2.f, &sin, &cos);

	q[0] = cos;					// w
	q[1] = sin * axis[0];		// x
	q[2] = sin * axis[1];		// y
	q[3] = sin * axis[2];		// z
}


inline void quaternion_from_m44c(float* q, float* m)
{
	float diagonal = m[0] + m[5] + m[10];
	if (diagonal > 0) {
		float w4 = sqrtf(diagonal + 1.f) * 2.f;
		q[0] = 0.25f * w4;
		q[1] = (m[9] - m[6]) / w4;
		q[2] = (m[2] - m[8]) / w4;
		q[3] = (m[4] - m[1]) / w4;
	}
	else if ((m[0] > m[5]) && (m[0] > m[10])) {
		float x4 = sqrtf(1.f + m[0] - m[5] - m[10]) * 2.f;
		q[0] = (m[9] - m[6]) / x4;
		q[1] = 0.25f * x4;
		q[2] = (m[1] + m[4]) / x4;
		q[3] = (m[2] + m[8]) / x4;
	}
	else if (m[5] > m[10]) {
		float y4 = sqrtf(1.f + m[5] - m[0] - m[10]) * 2.f;
		q[0] = (m[2] - m[8]) / y4;
		q[1] = (m[1] + m[4]) / y4;
		q[2] = y4 / 4.f;
		q[3] = (m[6] + m[9]) / y4;
	}
	else {
		float z4 = sqrtf(1.f + m[10] - m[0] - m[5]) * 2.f;
		q[0] = (m[4] - m[1]) / z4;
		q[1] = (m[2] + m[8]) / z4;
		q[2] = (m[6] + m[9]) / z4;
		q[3] = z4 / 4.f;
	}
}






inline void m44c_from_quaternion(float* m, float* q)
{
	float wx = q[0] * q[1];
	float wy = q[0] * q[2];
	float wz = q[0] * q[3];
	float xx = q[1] * q[1];
	float xy = q[1] * q[2];
	float xz = q[1] * q[3];
	float yy = q[2] * q[2];
	float yz = q[2] * q[3];
	float zz = q[3] * q[3];

	m[0] = 1.f - 2.f * (yy + zz);
	m[1] = 2.f * (xy - wz);
	m[2] = 2.f * (xz + wy);
	m[3] = 0.f;

	m[4] = 2.f * (xy + wz);
	m[5] = 1.f - 2.f * (xx + zz);
	m[6] = 2.f * (yz - wx);
	m[7] = 0.f;

	m[8] = 2.f * (xz - wy);
	m[9] = 2.f * (yz + wx);
	m[10] = 1.f - 2.f * (xx + yy);
	m[11] = 0.f;

	m[12] = 0.f;
	m[13] = 0.f;
	m[14] = 0.f;
	m[15] = 1.f;
}





inline void m44r_from_quaternion(float* m, float* q)
{
	float wx = q[0] * q[1];
	float wy = q[0] * q[2];
	float wz = q[0] * q[3];
	float xx = q[1] * q[1];
	float xy = q[1] * q[2];
	float xz = q[1] * q[3];
	float yy = q[2] * q[2];
	float yz = q[2] * q[3];
	float zz = q[3] * q[3];

	m[0] = 1.f - 2.f * (yy + zz);
	m[4] = 2.f * (xy - wz);
	m[8] = 2.f * (xz + wy);
	m[12] = 0.f;

	m[1] = 2.f * (xy + wz);
	m[5] = 1.f - 2.f * (xx + zz);
	m[9] = 2.f * (yz - wx);
	m[13] = 0.f;

	m[2] = 2.f * (xz - wy);
	m[6] = 2.f * (yz + wx);
	m[10] = 1.f - 2.f * (xx + yy);
	m[14] = 0.f;

	m[3] = 0.f;
	m[7] = 0.f;
	m[11] = 0.f;
	m[15] = 1.f;
}


inline void quaternion_normalize(v4* a)
{
	float vsq, vsqrt;
	
	//square
	__m128 aa = _mm_load_ps(a);
	__m128 rr = _mm_mul_ps(aa, aa);

	//add
	rr = _mm_hadd_ps(rr, rr);
	rr = _mm_hadd_ps(rr, rr);
	_mm_store_ss(&vsq, rr);

	//square root
	vsqrt = sqrtf(vsq);

	//divide
	__m128 msqrt = _mm_load_ps1(&vsqrt);
	__m128 mdiv = _mm_div_ps(aa, msqrt);

	//return result
	_mm_store_ps(a, mdiv);
}


inline void quaternion_copy(v4* a, v4* r)
{
	_mm_store_ps(r, _mm_load_ps(a));
}

inline void quaternion_add(v4* a, v4* b, v4* r)
{
	__m128 aa = _mm_load_ps(a);
	__m128 bb = _mm_load_ps(b);
	__m128 rr = _mm_add_ps(aa, bb);
	_mm_store_ps(r, rr);
}

inline void quaternion_sub(v4* a, v4* b, v4* r)
{
	__m128 aa = _mm_load_ps(a);
	__m128 bb = _mm_load_ps(b);
	__m128 rr = _mm_sub_ps(aa, bb);
	_mm_store_ps(r, rr);
}


inline void quaternion_x_float(v4* a, float b, v4* r)
{
	__m128 aa = _mm_load_ps(a);
	__m128 bb = _mm_load_ps1(&b);
	__m128 rr = _mm_mul_ps(aa, bb);
	_mm_store_ps(r, rr);
}


inline void quaternion_dot(v4* a, v4* b, float* r)
{
	__m128 rr = _mm_mul_ps(_mm_load_ps(a), _mm_load_ps(b));
	rr = _mm_hadd_ps(rr, rr);
	rr = _mm_hadd_ps(rr, rr);
	_mm_store_ss(r, rr);
}

inline void ymge_clamp(float* a, float minval, float maxval)
{
	_mm_store_ss(a, _mm_min_ss(_mm_max_ss(_mm_set_ss(*a), _mm_set_ss(minval)), _mm_set_ss(maxval)));
}

inline void quaternion_slerp(v4* a, v4* b, float t, v4* r)
{
	float dot;
	float theta_t;
	v4 aa[4];
	v4 bb[4];
	v4 v[4];

	
	quaternion_dot(a, b, &dot);

	if (dot > QUATERNION_DOT_THRESHOLD)
	{
		quaternion_sub(a, b, r);
		quaternion_x_float(r, t, r);
		quaternion_add(a, r, r);
		quaternion_normalize(r);
		return;
	}


	if (dot < 0)
	{
		quaternion_x_float(b, -1.f, bb);
		dot = -dot;
	}
	else
	{
		quaternion_copy(b, bb);
	}

	ymge_clamp(&dot, -1.f, 1.f);
	theta_t = acosf(dot) * t;

	//get v
	quaternion_x_float(a, dot, v);
	quaternion_sub(b, v, v);
	quaternion_normalize(v);

	quaternion_x_float(a, cosf(theta_t), aa);
	quaternion_x_float(v, sinf(theta_t), v);

	quaternion_add(aa, v, r);
}


inline void v4_lerp(v4* a, v4* b, float t, v4* r)
{
	__m128 aa = _mm_load_ps(a);
	__m128 bb = _mm_load_ps(b);

	__m128 rr = _mm_add_ps(aa, _mm_mul_ps(_mm_sub_ps(bb, aa), _mm_load_ps1(&t)));
	_mm_store_ps(r, rr);
}


