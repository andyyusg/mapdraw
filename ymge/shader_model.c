#include <GL/glew.h>

#include "shader_model.h"
#include "shader.h"

void shader_model_init(shader_model_t* me, const char* vertex_shader_path, const char* fragment_shader_path)
{
	shader_source_t		shaders[] = {
		{GL_VERTEX_SHADER, vertex_shader_path},
		{GL_FRAGMENT_SHADER, fragment_shader_path}
	};

	//build shader
	me->program = shader_build(2, shaders);
	if (!me->program) return;

	//get uniform locations
	me->light_normal = glGetUniformLocation(me->program, "light_normal");
}


void shader_model_clean(shader_model_t* me)
{
	glDeleteProgram(me->program);
}

