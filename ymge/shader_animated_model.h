#pragma once

#include "ymge_type.h"


typedef struct
{
	uint			program;
	
	//uniform
	int				mat_transform;
	int				mat_model;
	int				light_normal;
	int				mat_joint_transform;
}
shader_animated_model_t;



//function
void shader_animated_model_init(shader_animated_model_t* me, const char* vertex_shader_path, const char* fragment_shader_path);
void shader_animated_model_clean(shader_animated_model_t* me);