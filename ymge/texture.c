#include <stdlib.h>
#include <string.h>


#include <GL/glew.h>

#include <logger.h>
#include <buf.h>

#include "texture.h"
#include "ymge_error.h"
#include "image.h"
#include "render_res.h"
#include "ymge.h"

void texture_load(texture_t* me, const char* path, render_res_t* render_res)
{
	image_t img[1];

	//load image
	image_load(img, path);

	//get image info
	me->w = img->w;
	me->h = img->h;
	me->bit_depth = img->bit_depth;
	me->color_type = img->color_type;

	if (render_res)
	{
		//render resource
		if (img->format == GL_RGB &&
			img->w == 1024 &&
			img->h == 1024)
		{
			me->render_res = render_res;
			me->render_res_texture_slot = (float)(render_res->num_texture);
			render_res_add_texture(render_res, path);
		}
	}
	else
	{
		//create OpenGL texture;
		glGenTextures(1, &me->id);
		glBindTexture(GL_TEXTURE_2D, me->id);
		glTexImage2D(GL_TEXTURE_2D, 0, img->format, me->w, me->h, 0, img->format, GL_UNSIGNED_BYTE, img->buf_img->data);

		//GL_TEXTURE_MIN_FILTER must be set, or else the texture appears black on screen
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}




	//clean
	image_unload(img);
}



static void text_calc_dimension(const char* text, int* tw, int* th)
{
	int w = 1;				//some character has left -1, init w to 1 to cater for this
	int h = 0;
	int chh;	//character height
	int i;
	char ch;
	font_t* font = ymge->font;
	char_info_t* ci;

	for (i=0;;i++)
	{
		//get char until NULL
		ch = text[i];
		if (ch == 0)
			break;

		ci = &font->char_info[ch - 32];			//ascii char is rendered in table before hand

		w += ci->adv_x;
		chh = ci->top + ci->height;
		if (chh > h)
			h = chh;
	}

	*tw = w;
	*th = h;
}

static void text_draw(GLuint tid, const char* text)
{
	int i;
	char ch;
	int cur_x = 1;
	font_t* font = ymge->font;
	char_info_t* ci;

	for (i = 0;; i++)
	{
		//get char until NULL
		ch = text[i];
		if (ch == 0)
			break;

		ci = &font->char_info[ch - 32];			//ascii char is rendered in table before hand

		//draw char
		glBindTexture(GL_TEXTURE_2D, tid);
		glTexSubImage2D(GL_TEXTURE_2D, 0, cur_x + ci->left, ci->top, ci->width, ci->height, GL_RED, GL_UNSIGNED_BYTE, ci->img);

		//cursor advance
		cur_x += ci->adv_x;
	}
}

void texture_gen_text(texture_t* me, const char* text)
{
	int tw, th;

	//calculate texture dimension
	text_calc_dimension(text, &tw, &th);
	me->w = tw;
	me->h = th;

	//create OpenGL texture
	//glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &me->id);
	glBindTexture(GL_TEXTURE_2D, me->id);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, tw, th, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
	me->error = glGetError();
	if (me->error)
	{
		LOG4("[texture] generate text, create texture failed: glTexImage2D, %d", me->error);
		return;
	}

	//these 2 must be set, or else the texture appeas black on screen
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//draw text
	text_draw(me->id, text);
}


void texture_unload(texture_t* me)
{
	glDeleteTextures(1, &me->id);
}

texture_t* texture_new(const char* uid)
{
	texture_t* me = malloc(sizeof(texture_t));
	memset(me, 0, sizeof(texture_t));
	strcpy_s(me->uid, 100, uid);
	me->uid_ptr = me->uid;
	return me;
}


void texture_del(texture_t* me, void* param)
{
	texture_unload(me);
	free(me);
}


int texture_compare(const texture_t* a, const texture_t* b, void* param)
{
	return strcmp(a->uid_ptr, b->uid_ptr);
}

