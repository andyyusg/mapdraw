#pragma once

#include "ymge_type.h"

#define YMGE_UI_WINDOW_POS_X_LEFT				1
#define YMGE_UI_WINDOW_POS_X_RIGHT				2
#define YMGE_UI_WINDOW_POS_Y_TOP				4
#define YMGE_UI_WINDOW_POS_Y_BOTTOM				8

//type
typedef struct ui_tpl_img_s ui_tpl_img_t;
typedef struct ui_tpl_wnd_s ui_tpl_wnd_t;
typedef struct ui_wnd_s ui_wnd_t;


typedef union ui_layout_u
{
	struct
	{
		int			style;

		int			top;
		int			bottom;
		int			left;
		int			right;

		int			w;
		int			h;
	};

	int i[7];
}
ui_layout_t;


typedef struct ui_img_s
{
	//layout
	ui_layout_t		layout;

	//image template
	ui_tpl_img_t*	tpl;
}
ui_img_t;

struct ui_wnd_s
{
	int				initialized;

	//template
	ui_tpl_wnd_t*	tpl;

	//layout
	int				pos_style;
	int				x;
	int				y;
	int				w;
	int				h;

	//buffer pos
	int				pos_vertices;
	int				pos_uvs;
	int				pos_indices;

	//child images
	ui_img_t*		imgs;
	u32				num_imgs;

	//child windows
	ui_wnd_t*		wnds;
	u32				num_wnds;
};




//function
void ui_wnd_init(ui_wnd_t* me, ui_tpl_wnd_t* tpl, int pos_style, int x, int y, int w, int h, u32 num_imgs, u32 num_wnds);
void ui_wnd_clean(ui_wnd_t* me);


void ui_wnd_set_img(ui_wnd_t* me, u32 slot, ui_tpl_img_t* tpl, ui_layout_t* layout);


void ui_wnd_count_wnds(ui_wnd_t* me, int* num_wnds);
