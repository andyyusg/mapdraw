#pragma once

#include <math.h>


#define YMGE_TRI_PI					3.14159265358979f
#define YMGE_TRI_PI_DIV_2			YMGE_TRI_PI / 2.f
#define YMGE_TRI_PI_DIV_4			0.7853981633974475f
#define YMGE_TRI_180_DIV_PI			57.29577951308238f
#define YMGE_TRI_TABLE_QUARTERS		5
#define YMGE_TRI_TABLE_DEGREES		YMGE_TRI_TABLE_QUARTERS * 90
#define YMGE_TRI_TABLE_PRECISION	100
#define YMGE_TRI_TABLE_UNITS		YMGE_TRI_TABLE_DEGREES * YMGE_TRI_TABLE_PRECISION


//variable
extern float	tri_table[YMGE_TRI_TABLE_UNITS + 1];
extern float*	tri_table_sin;
extern float*	tri_table_cos;

extern float	tri_table_degrees;
extern float	tri_table_precision;
extern int		tri_table_units;
extern int		tri_table_4q_units;



//init trigonometry
void tri_init();


inline float tri_sin(float deg)
{
	float d = deg / 360.f;
	int units = ((int)(deg*tri_table_precision));

	if(units < 0)
		return -tri_table_sin[-units];
	else
		return tri_table_sin[units];
}




inline void tri_sin_cos(float deg, float* sin, float* cos)
{
	float d = deg / 360.f;
	int units = ((int)(deg*tri_table_precision));

	if (units < 0)
	{
		*sin = -tri_table_sin[-units];
		*cos = tri_table_cos[-units];
	}
	else
	{
		*sin = tri_table_sin[units];
		*cos = tri_table_cos[units];
	}
}


inline float tri_tan(float deg)
{
	float sin, cos;

	tri_sin_cos(deg, &sin, &cos);
	return sin / cos;
}

inline float tri_ctan(float deg)
{
	float sin, cos;

	tri_sin_cos(deg, &sin, &cos);
	return cos / sin;
}

//fast atan
// v range -1 to 1  (equivalent to result -45 degrees to 45 degrees)
inline float tri_atan(float v)
{
	return YMGE_TRI_PI_DIV_4 * v - v*(fabsf(v) - 1.f)*(0.2447f + 0.0663f * fabsf(v));
}
