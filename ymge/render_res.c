#include <string.h>
#include <GL/glew.h>
#include <logger.h>
#include "render_res.h"
#include "ymge.h"


void render_res_init(render_res_t* me, int num_vbo, int size_vertices, vbo_info_t* vbo_info, i64 size_indices)
{
	//copy settings
	me->num_vbo = num_vbo;
	me->size_vertices = size_vertices;
	me->size_indices = size_indices;
	memcpy(me->vbo_info, vbo_info, sizeof(vbo_info_t)*num_vbo);


	//vertex buffer
	glGenBuffers(num_vbo, me->vbo);


	for (int i = 0; i < num_vbo; i++)
	{
		//init vbo size and pointer
		//per vertex vbo
		me->vbo_size[i] = size_vertices * vbo_info[i].unit_bytes;
		me->vbo_ptr[i] = 0;
		
		//allocate vbo buffer
		glBindBuffer(GL_ARRAY_BUFFER, me->vbo[i]);		
		glBufferStorage(GL_ARRAY_BUFFER, me->vbo_size[i], 0, GL_DYNAMIC_STORAGE_BIT);
		//glBufferData(GL_ARRAY_BUFFER, me->vbo_size[i], 0, GL_STATIC_DRAW);
	}


	//vertex index
	me->vio_size = size_indices * sizeof(GLshort);
	glGenBuffers(1, &me->vio);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->vio);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, me->vio_size, 0, GL_STATIC_DRAW);


	//texture array
	texture_array_init(me->texture_array, 1024, 1024, 100);

	me->error = glGetError();
	if (me->error)
	{
		LOG4("[render_res] init, error: %d", me->error);
		return;
	}
}



void render_res_clean(render_res_t* me)
{
	glDeleteBuffers(me->num_vbo, me->vbo);
	glDeleteBuffers(1, &me->vio);


	//texture array
	texture_array_clean(me->texture_array);
}




void render_res_add_model(render_res_t* me, int num_vertices, const float* vertices_ptr, const float* uvs_ptr, const float* normals_ptr, int num_indices, const u16* indices_ptr)
{
	int vertices_size = num_vertices * 3 * sizeof(float);
	int uvs_size = num_vertices * 2 * sizeof(float);
	int normals_size = num_vertices * 3 * sizeof(float);
	int indices_size = num_indices * sizeof(u16);

	//check vertex_ptr
	int vertex_ptr_new = me->vertex_ptr + num_vertices;
	if (vertex_ptr_new > me->size_vertices)
	{
		LOG4("[render_res] add_model error: vertex_ptr overflow");
		return;
	}

	//check index_ptr
	int index_ptr_new = me->index_ptr + num_indices;
	if (index_ptr_new > me->size_indices)
	{
		LOG4("[render_res] add_model error: index_ptr overflow");
		return;
	}

	//vertex pos
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[0]);
	glBufferSubData(GL_ARRAY_BUFFER, me->vbo_ptr[0], vertices_size, vertices_ptr);
	me->vbo_ptr[0] += vertices_size;


	//uvs
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[1]);
	glBufferSubData(GL_ARRAY_BUFFER, me->vbo_ptr[1], uvs_size, uvs_ptr);
	me->vbo_ptr[1] += uvs_size;


	//normals
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[2]);
	glBufferSubData(GL_ARRAY_BUFFER, me->vbo_ptr[2], normals_size, normals_ptr);
	me->vbo_ptr[2] += normals_size;


	//index
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->vio);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, me->vio_ptr, indices_size, indices_ptr);
	me->vio_ptr += indices_size;

	//vertex
	me->vertex_ptr = vertex_ptr_new;
	me->index_ptr = index_ptr_new;
}


void render_res_add_texture(render_res_t* me, const char* texture_file_path)
{
	texture_array_load_texture(me->texture_array, me->num_texture++, texture_file_path);
}

