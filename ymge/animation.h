#pragma once

#include "ymge_type.h"
#include "joint.h"

typedef struct
{
	u16				num_keyframes;
	u16				num_joints;

	float*			time;
	v4*				joint_rotation;
	v4*				joint_position;
}animation_t;


typedef struct
{
	u16				keyframe;			//switch animation at which keyframe
	int				piece_slot;			//destination animation piece slot
}
animation_sequence_switch_t;


typedef struct
{
	int								slot;
	animation_t*					anim;

	animation_sequence_switch_t		switcher[2];
}
animation_sequence_piece_t;


typedef struct
{
	animation_sequence_piece_t		start;
	animation_sequence_piece_t		loop;
	animation_sequence_piece_t		stop[2];

	//state
	int								is_looping;
	int								is_stopping;
	int								is_stopped;

	//next sequence to play if stopped
	int								next_seq_slot;
}
animation_sequence_t;


typedef struct model_s model_t;

//function
void animation_init(animation_t* me);
void animation_clean(animation_t* me);

void animation_load_from_file(animation_t* me, model_t* model, const char* path);
void animation_update_joint_transformation(animation_t* me, joint_t* joints, u8 joint_id, u16 curr_keyframe, float t, m44* parent_rt_transform, m44* joint_transformation);

