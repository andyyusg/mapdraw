#pragma once

#include "model.h"
#include "texture.h"

//macro
#define YMGE_ENTITY_NUM_MODEL				2
#define YMGE_ENTITY_NUM_ANIM_SEQ			3


//type declaration
typedef struct ymge_s ymge_t;
typedef struct render_group_s render_group_t;

typedef struct entity_s
{
	char			uid[255];				//unique id

	//uid pointer - for comparison
	const char*		uid_ptr;

	m44				mat[16];

	//transformation
	float			t[3];
	float			r[3];

	//bounding info
	float			aabb_local[6];			//axis aligned bounding box: Xmin, Xmax, Ymin, Ymax, Zmin, Zmax
	float			aabb[6];				//axis aligned bounding box: Xmin, Xmax, Ymin, Ymax, Zmin, Zmax

	v4				bounding_radius[4];		// 0 -> x radius, 1 -> y radius, 2 -> z radius, 3 -> overall 3d sphere radius

	v4				aabb_center[4];


	//model & texture
	model_t*		model;
	texture_t*		texture;

	float			model_animation_time;
	u16				model_animation_keyframe;
	float			model_animation_root_delta_pos[3];
	v4				model_animation_root_pos_curr[4];		//current root position


	animation_t*			animation;												//current animation
	int						anim_seq_slot_curr;										//current animation sequence slot
	animation_sequence_t*	anim_seq_curr;											//current animation sequence
	animation_sequence_t	anim_seq[YMGE_ENTITY_NUM_ANIM_SEQ];						//all animation sequences


	//collide with ray
	int				is_collided;
	int				is_selected;

	//AABB rendering
	uint			vbo_aabb[2];
	uint			vao_aabb;


	//NEW: render resource
	int				render_group_instance_id;
	render_group_t*	render_group;
}
entity_t;




entity_t* entity_new(const char* uid);
void entity_clean(entity_t* me, void* param);
void entity_del(entity_t* me, void* param);

//function
void entity_init(entity_t* me, model_t* model, texture_t* texture);
void entity_set_uid(entity_t* me, const char* uid);


void entity_update_bounding(entity_t* me);
void entity_update_transformation(entity_t* me);
void entity_update(entity_t* me);

void entity_render_aabb(entity_t* me);
void entity_render(entity_t* me);


int entity_compare(const entity_t* a, const entity_t* b, void* param);

//animation
void entity_change_animation(entity_t* me, int anim_slot);

//animation sequence
void entity_set_animation_sequence(entity_t* me, int anim_seq_slot, int anim_slot_start, int anim_slot_loop, int anim_slot_stop_0, int anim_slot_stop_1);
void entity_set_animation_sequence_loop_stop_switch(entity_t* me, int anim_seq_slot, int switch_slot, u16 keyframe, int stop_piece_slot);
void entity_play_animation_sequence(entity_t* me, int anim_seq_slot);
void entity_stop_animation_sequence(entity_t* me);

int entity_get_current_animation_sequence_slot(entity_t* me);

