#include <GL/glew.h>
#include "image.h"
#include "texture_array.h"


void texture_array_init(texture_array_t* me, int width, int height, int count)
{
	//keep settings
	me->width = width;
	me->height = height;
	me->count = count;

	glGenTextures(1, &me->id);
	glBindTexture(GL_TEXTURE_2D_ARRAY, me->id);
	glTexStorage3D(GL_TEXTURE_2D_ARRAY,
		1,									//mipmap levels
		GL_RGB8,							//format
		width, height,						//texture width, height
		count								//textures count
		);

	//setting
	glTexParameterf(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}


void texture_array_clean(texture_array_t* me)
{
	glDeleteTextures(1, &me->id);
}


void texture_array_load_texture(texture_array_t* me, int slot, const char* path)
{
	image_t			img[1];

	//load image
	image_load(img, path);

	//load texture
	glBindTexture(GL_TEXTURE_2D_ARRAY, me->id);
	glTexSubImage3D(GL_TEXTURE_2D_ARRAY,
		0,								//mipmap level
		0, 0,							//width, height offsets
		slot,							//texture slot
		me->width,
		me->height,
		1,
		GL_RGB,
		GL_UNSIGNED_BYTE,
		img->buf_img->data
		);

	//clean
	image_unload(img);
}



