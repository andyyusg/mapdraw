#include <GL/glew.h>

#include "shader_text.h"
#include "shader.h"

void shader_text_init(shader_text_t* me, const char* vertex_shader_path, const char* fragment_shader_path)
{
	shader_source_t		shaders[] = {
		{GL_VERTEX_SHADER, vertex_shader_path},
		{GL_FRAGMENT_SHADER, fragment_shader_path}
	};

	//build shader
	me->program = shader_build(2, shaders);
	if (!me->program) return;
}


void shader_text_clean(shader_text_t* me)
{
	glDeleteProgram(me->program);
}

