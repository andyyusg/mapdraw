// ymge ui template
#pragma once

//ui template image dimension
typedef struct ui_tpl_img_s
{
	float		x;
	float		y;
	float		w;
	float		h;

	//uv
	float		u1;
	float		v1;
	float		u2;
	float		v2;
}
ui_tpl_img_t;

//template window
typedef struct ui_tpl_wnd_s
{
	int			x[4];
	int			y[4];

	float		u[4];
	float		v[4];
}
ui_tpl_wnd_t;



typedef struct ui_tpl_s
{
	int					initialized;

	//texture width, height
	//type float, for easy calculation, no type conversion
	float				width;
	float				height;

	//imgs
	ui_tpl_img_t*		imgs;
	int					num_imgs;

	//wnds
	ui_tpl_wnd_t*		wnds;
	int					num_wnds;
}
ui_tpl_t;



//function
void ui_tpl_init(ui_tpl_t* me, float width, float height, int num_imgs, int num_wnds);
void ui_tpl_clean(ui_tpl_t* me);

void ui_tpl_set_img(ui_tpl_t* me, int slot, float x, float y, float w, float h);
ui_tpl_wnd_t* ui_tpl_set_wnd(ui_tpl_t* me, int slot, float x0, float x1, float x2, float x3, float y0, float y1, float y2, float y3);

