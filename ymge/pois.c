#include <string.h>
#include <stdlib.h>
#include <GL/glew.h>

#include <file.h>

#include "ymge.h"
#include "pois.h"


//singleton
static pois_t me;


//function
void pois_load_from_file(void)
{
	file_t			file = { 0 };
	buf_t*			data = 0;
	char*			line = 0;
	char*			p = 0;
	char*			p2 = 0;
	float			f = 0.f;
	int				num_pois;
	int				index = 0;

	//read file
	if (file_open(&file, "data\\poi.txt", "r"))
	{
		data = file_read_all(&file);
		file_close(&file);

		line = data->data;

		//read num pois
		p = strchr(line, '\n'); *p = 0;
		p2 = strstr(line, " : ");
		num_pois = me.num_pois = atoi(p2 + 3);
		line = p + 1;

		//read roads data
		for (int i = 0; i < num_pois; i++)
		{
			poi_t* poi = me.pois + i;

			//read node coordinates
			p = strchr(line, '\n'); *p = 0;
			p2 = strchr(line, ','); *p2 = 0;
			poi->pos[0] = strtof(line, 0);
			poi->pos[1] = 0.f;
			poi->pos[2] = -strtof(p2 + 1, 0);
			poi->pos[3] = 1.f;
			line = p + 1;

			//increase index
			index++;
		}

		//total nodes
		me.num_pois = index;

		//clean
		free(data);
	}
}


static void poi_init(poi_t* poi)
{
	const static float w = 0.06f;
	float h = w * (2 - 1/ymge->ar);
	float w_half = w / 2.f;

	float pos[8] = {
		-w_half, h,
		-w_half, 0,
		w_half, 0,
		w_half, h,
	};

	float uv[8] = {
		0.4111f, 0.8506f,
		0.4111f, 0.6211f,
		0.6406f, 0.6211f,
		0.6406f, 0.8506f,
	};

	u16 indices[12] = {
		0, 1, 2,
		2, 3, 0,
	};

	glGenBuffers(2, poi->vbo);
	glBindBuffer(GL_ARRAY_BUFFER, poi->vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, 32, pos, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, poi->vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, 32, uv, GL_STATIC_DRAW);

	glGenBuffers(1, &poi->ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, poi->ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 12, indices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &poi->vao);
	glBindVertexArray(poi->vao);
	glBindBuffer(GL_ARRAY_BUFFER, poi->vbo[0]);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, poi->vbo[1]);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);
}

static void poi_clean(poi_t* poi)
{
	glDeleteBuffers(2, poi->vbo);
	glDeleteBuffers(1, &poi->ibo);
	glDeleteVertexArrays(1, &poi->vao);
}

static void pois_prepare_render(void)
{
	int num_pois = me.num_pois;

	//get texture
	me.texture = ymge_get_texture("ui.window")->id;

	for (int i = 0; i < num_pois; i++)
	{
		poi_t* poi = me.pois + i;

		poi_init(poi);
	}
}

static BOOL poi_cull(poi_t* me, m44* mat_vp, float* res)
{
	v4 r[4];

	//transform sign position
	v4_x_m44c(me->pos, mat_vp, r);

	//divide w
	r[0] /= r[3];
	r[1] /= r[3];
	r[2] /= r[3];

	if (fabsf(r[0]) < 1.f && fabsf(r[1]) < 1.f && fabsf(r[2]) < 1.f)
	{
		res[0] = r[0];
		res[1] = r[1];
		return FALSE;
	}
	else
		return TRUE;
}

static void poi_render(poi_t* poi, m44* mat_vp)
{
	float r[2];

	//cull
	if (poi_cull(poi, mat_vp, r))
		return;

	//set position
	glUniform2fv(0, 1, r);

	glBindVertexArray(poi->vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, poi->ibo);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
}


void pois_render(m44* mat_vp)
{
	glBindTexture(GL_TEXTURE_2D, me.texture);
	glUseProgram(ymge->shader_text_frame->program);

	for (int i = 0; i < me.num_pois; i++)
	{
		poi_render(me.pois + i, mat_vp);
	}
}



void pois_init(void)
{
	pois_load_from_file();
	pois_prepare_render();
}



void pois_clean(void)
{
	for (int i = 0; i < me.num_pois; i++)
	{
		poi_t* poi = me.pois + i;
		poi_clean(poi);
	}
}

