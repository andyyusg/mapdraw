#include <intrin.h>
#include <string.h>

#include <logger.h>

#include "ymge_type.h"
#include "hwinfo.h"


void hwinfo_print()
{
	int			cpuinfo[4];
	int*		cpuinfo2 = 0;
	int*		cpuinfo3 = 0;	//ext
	int*		curr = 0;
	int			max = 0;		//max cpu info id
	int			max_ext = 0;	//max cpu info ext id
	int			i = 0;
	int			size = 0;
	u32			size_int = 0;

	int			f_1_ecx = 0;
	int			f_1_edx = 0;
	int			f_7_ebx = 0;
	int			f_7_ecx = 0;
	int			f_81_ecx = 0;
	int			f_81_edx = 0;

	char		vendor[12];

	//get int size
	size_int = sizeof(int);
	LOG0("[hwinfo] int size: %d", size_int);

	//get cpu info count
	__cpuid(cpuinfo, 0);
	max = cpuinfo[0];
	LOG0("[hwinfo] cpu info count: %d", max+1);

	//mem alloc for cpu info
	curr = cpuinfo2 = calloc(max+1, size_int * 4);

	//loop - get cpu info
	for (i = 0; i <= max; i++)
	{
		__cpuidex(curr, i, 0);
		curr += 4;
	}

	//get vendor
	memcpy(vendor, &cpuinfo2[1], 4);
	memcpy(vendor + 4, &cpuinfo2[3], 4);
	memcpy(vendor + 8, &cpuinfo2[2], 4);
	LOG0("[hwinfo] cpu vendor: %.*s", 12, vendor);

	//get other cpu flags
	if (max >= 1)
	{
		f_1_ecx = cpuinfo2[1 * 4 + 2];
		f_1_edx = cpuinfo2[1 * 4 + 3];
	}
	if (max >= 7)
	{
		f_7_ebx = cpuinfo2[7 * 4 + 1];
		f_7_ecx = cpuinfo2[7 * 4 + 2];
	}

	//get cpu info ext - count
	__cpuid(cpuinfo, 0x80000000);
	max_ext = cpuinfo[0];

	size = 0x80000000;
	size = (max_ext - 0x80000000) + 1;
	curr = cpuinfo3 = calloc(size, size_int*4);
	if (size < 1)
	{
		LOG4("[hwinfo] max ext %d size %d", max_ext, size);
	}

	//get cpu info ext
	for (i = 0x80000000; i <= max_ext; i++)
	{
		__cpuidex(curr, i, 0);
		curr += 4;
	}

	//get ext flags
	if (max_ext >= 0x80000001)
	{
		f_81_ecx = cpuinfo3[1 * 4 + 2];
		f_81_edx = cpuinfo3[1 * 4 + 3];
	}


	//for more flags
	//refer to https://msdn.microsoft.com/en-us/library/hskdteyh.aspx
	LOG0("[hwinfo] supports SSE: %s", f_1_edx & 1 << 25 ? "yes" : "no");
	LOG0("[hwinfo] supports SSE2: %s", f_1_edx & 1 << 26 ? "yes" : "no");
	LOG0("[hwinfo] supports SSE3: %s", f_1_ecx & 1 ? "yes" : "no");
	LOG0("[hwinfo] supports SSSE3: %s", f_1_ecx & 1 << 9 ? "yes" : "no");
	LOG0("[hwinfo] supports SSE41: %s", f_1_ecx & 1 << 19 ? "yes" : "no");
	LOG0("[hwinfo] supports SSE42: %s", f_1_ecx & 1 << 20 ? "yes" : "no");
	LOG0("[hwinfo] supports RDRAND: %s", f_1_ecx & 1 << 30 ? "yes" : "no");
	LOG0("[hwinfo] supports _3DNOW: %s", f_81_edx & 1 << 31 ? "yes" : "no");
	LOG0("[hwinfo] supports _3DNOWEXT: %s", f_81_edx & 1 << 30 ? "yes" : "no");


	//clean
	free(cpuinfo2);
	free(cpuinfo3);
}