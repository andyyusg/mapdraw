#pragma once

#include "vector.h"



// formula
//-----------------------------------------------------------------------
// when plane direction normal vector is axis allied, for example 0,0,1
//
// tx: distance to plane X
// px: plane X
// rx: ray X
// rdx: ray direction normal vector X
//
// tx = (px - rx)/rdx
//-----------------------------------------------------------------------

// ray_dir: normal vector
// aabb: x min, x max, y min, y max, z min, z max
inline int ray_test_aabb(float* ray_dir, float* ray_pos, float* aabb, float* t, int* collide)
{
	float c[4];

	float tx_min = (aabb[0] - ray_pos[0]) / ray_dir[0];
	float tx_max = (aabb[1] - ray_pos[0]) / ray_dir[0];
	float ty_min = (aabb[2] - ray_pos[1]) / ray_dir[1];
	float ty_max = (aabb[3] - ray_pos[1]) / ray_dir[1];
	float tz_min = (aabb[4] - ray_pos[2]) / ray_dir[2];
	float tz_max = (aabb[5] - ray_pos[2]) / ray_dir[2];

	t[0] = min(tx_min, tx_max);
	t[1] = max(tx_min, tx_max);
	t[2] = min(ty_min, ty_max);
	t[3] = max(ty_min, ty_max);
	t[4] = min(tz_min, tz_max);
	t[5] = max(tz_min, tz_max);

	//xy collide
	c[0] = max(t[0], t[2]);		//1st collision distance
	c[1] = min(t[1], t[3]);		//2nd collision distance

	//xz collide
	c[2] = max(t[0], t[4]);		//1st collision distance
	c[3] = min(t[1], t[5]);		//2nd collision distance

	collide[0] = c[0] < c[1];		//xy collide
	collide[1] = c[2] < c[3];		//xz collide
	collide[2] = max(c[0], c[2]) < min(c[1], c[3]);
	collide[3] = collide[0] && collide[1] && collide[2];

	return collide[3];
}





inline void ray_eye_to_world(float* ray_eye, float* cam_r, float* ray_world)
{
	m44			m[16];
	v4			r[4];

	//matrix
	m44_set_Rzxy_transposed(m, cam_r);

	v4_x_m44c(ray_eye, m, r);

	//normalize vector
	v4_normalize(r, ray_world);
}