#include <string.h>

#include "light.h"
#include "vector.h"

//light dir needs to be reversed, meaning dir from surface to light
//for example, if you want to set light dir -0.707107, 0, -0.707107
//light dir value should be 0.707107, 0, 0.707107
void light_init(light_t* me)
{
	memset(me, 0, sizeof(light_t));

	//default direction
	me->dir[0] = 0.707107f;
	me->dir[1] = 0.707107f;
	me->dir[2] = 0.707107f;
	me->dir[3] = 0.f;

	v4_normalize(me->dir, me->dir);
}

