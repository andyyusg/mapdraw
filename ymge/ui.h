#pragma once

#include "ymge_type.h"
#include "ui_tpl.h"
#include "ui_wnd_group.h"





typedef struct ui_s
{
	ui_tpl_t*				templates;
	u32						num_templates;

	ui_wnd_group_t*			wnd_groups;
	u32						num_wnd_groups;
}
ui_t;



//function
void ui_init(ui_t* me, u32 num_templates, u32 num_wnd_groups);
void ui_clean(ui_t* me);

