#pragma once

#include "ymge_type.h"

typedef struct texture_array_s
{
	uint			id;			//OpenGL texture id

	int				width;		//texture size
	int				height;

	int				count;		//textures count
}
texture_array_t;


//function
void texture_array_init(texture_array_t* me, int width, int height, int count);
void texture_array_clean(texture_array_t* me);

void texture_array_load_texture(texture_array_t* me, int slot, const char* path);