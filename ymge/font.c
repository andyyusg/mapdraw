#include <GL/glew.h>

#include "font.h"
#include "logger.h"


void font_init(font_t* me)
{
	me->error = FT_Init_FreeType(&me->ft);
	if (me->error)
	{
		LOG4("[font] init error %d", me->error);
		return;
	}
}



void font_clean(font_t* me)
{
	//unload
	if (me->face)
		font_unload(me);

	LOG0("[font] clean");
	FT_Done_FreeType(me->ft);
}



void font_atlas_get_dimensions(font_t* me)
{
	FT_GlyphSlot g = me->face->glyph;

	for (int i = 32; i < 128; i++) 
	{
		if (FT_Load_Char(me->face, i, FT_LOAD_RENDER))
		{
			LOG4("[font] create texture atlas, Loading character %c failed!\n", i);
			continue;
		}

		me->atlas_w += g->bitmap.width;
		me->atlas_h = (uint)(me->atlas_h) > g->bitmap.rows? me->atlas_h : g->bitmap.rows;
	}
}





static void font_load_char_info(font_t* me)
{
	int i;

	for (i = 0; i < 95; i++)
	{
		font_render_char(me, YMGE_FONT_SIZE, 32 + i);
		
		me->char_info[i].left = me->char_left;
		me->char_info[i].top = me->char_top;
		me->char_info[i].width = me->char_width;
		me->char_info[i].height = me->char_height;
		me->char_info[i].adv_x = me->char_adv_x;
		memcpy(me->char_info[i].img, me->face->glyph->bitmap.buffer, me->char_width * me->char_height);
	}
}


void font_load(font_t* me, const char* path)
{
	me->error = FT_New_Face(me->ft, path, 0, &me->face);
	if (me->error)
	{
		LOG4("[font] load error %d", me->error);
		return;
	}

	//set pixel size
	//FT_Set_Pixel_Sizes(me->face, 0, 48);

	//create texture atlas
	//NOT USING texture atlas at the moment
	//font_create_texture_atlas(me);

	font_load_char_info(me);
}

void font_render_char(font_t* me, uint char_height, wchar_t ch)
{
	FT_GlyphSlot g = me->face->glyph;
	int width, height;

	//set char size
	FT_Set_Pixel_Sizes(me->face, 0, char_height);

	//text
	//FT_ULong	ch = L'̩';
	//ch = L'̫';
	me->error = FT_Load_Char(me->face, ch, FT_LOAD_RENDER);

	//get glyph dimensions
	me->char_width = g->bitmap.width;
	me->char_height = g->bitmap.rows;
	me->char_adv_x = g->advance.x >> 6;
	me->char_left = g->bitmap_left;
	me->char_top = char_height - g->bitmap_top;

	width = me->char_width;
	height = me->char_height;

	//convert grayscale to RGBA image
	/*
	for (y = 0; y < me->char_height; y++)
	{
		for (x = 0; x < me->char_width; x++)
		{
			me->char_img[y*width + x] = g->bitmap.buffer[y*me->char_width + x];
		}
	}
	*/
}


void font_unload(font_t* me)
{
	//delete atlas texture
	//glDeleteTextures(1, &me->atlas_texture);

	//clean freetype library
	me->error = FT_Done_Face(me->face);
	me->face = 0;
	if (me->error)
	{
		LOG4("[font] unload error %d", me->error);
		return;
	}
}


