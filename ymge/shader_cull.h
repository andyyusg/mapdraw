#pragma once

#include "ymge_type.h"
#include "render_group.h"

typedef struct shader_cull_s
{
	uint			program;
	uint			ssbo;
}
shader_cull_t;




//function
void shader_cull_init(shader_cull_t* me, const char* shader_path);
void shader_cull_clean(shader_cull_t* me);

void shader_cull_exec(shader_cull_t* me, render_group_t* render_group);