#include <gl\glew.h>

#include "logger.h"
#include "ymge.h"
#include "msaa.h"

//variable
static msaa_t msaa = { 0 };


void msaa_init()
{
	int iSamples = 2;

	glEnable(GL_MULTISAMPLE);

	glGenFramebuffers(1, &msaa.fbo);
	glGenRenderbuffers(2, msaa.rbo);

	glBindRenderbuffer(GL_RENDERBUFFER, msaa.rbo[0]);
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, iSamples, GL_RGB8, ymge->window_width, ymge->window_height);

	glBindRenderbuffer(GL_RENDERBUFFER, msaa.rbo[1]);
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, iSamples, GL_DEPTH_COMPONENT, ymge->window_width, ymge->window_height);

	//attach
	glBindFramebuffer(GL_FRAMEBUFFER, msaa.fbo);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, msaa.rbo[0]);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, msaa.rbo[1]);

	//check
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		LOG4("[msaa] frame buffer check failed.");
	}


	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void msaa_clean()
{
	glDeleteFramebuffers(1, &msaa.fbo);
	glDeleteRenderbuffers(2, msaa.rbo);
}

void msaa_prepare()
{
	glBindFramebuffer(GL_FRAMEBUFFER, msaa.fbo);
}

void msaa_blit()
{
	glBindFramebuffer(GL_READ_FRAMEBUFFER, msaa.fbo);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBlitFramebuffer(0, 0, ymge->window_width, ymge->window_height, 0, 0, ymge->window_width, ymge->window_height, GL_COLOR_BUFFER_BIT, GL_LINEAR);
}
