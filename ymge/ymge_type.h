#pragma once


#include <ymtype.h>




//type
typedef unsigned int					uint;
typedef int								i32;
typedef long long						i64;


//type: aligned
typedef __declspec(align(64)) float		m44;
typedef __declspec(align(16)) float		v4;



//vbo info
typedef struct
{
	int					unit_bytes;

	int					unit_size;		//how many units per vertex
	int					unit_type;		//unit type: GL_FLOAT, GL_SHORT
	int					stride;			//interval
	i64					offset;			//offset
}
vbo_info_t;

typedef struct render_indirect_params_s
{
	uint			count;					//indices count
	uint			primCount;				//instances count
	uint			firstIndex;				//first index offset
	uint			baseVertex;				//first vertex offset
	uint			baseInstance;
}
render_indirect_params_t;


