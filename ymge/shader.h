#pragma once

#include "ymge_type.h"

typedef struct
{
	uint			type;
	const char*		path;
}
shader_source_t;

//function
uint shader_build(uint num_srcs, shader_source_t* srcs);