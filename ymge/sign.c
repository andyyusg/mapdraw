#include <GL/glew.h>

#include <logger.h>

#include "sign.h"
#include "ymge.h"


static void make_frame_model(sign_t* me)
{
	float scale = 1.5f;
	float sh = me->sh * scale;
	float sw_half = me->sw / 2.f;
	float a[] = {
		-(sw_half + sh/2.f),
		-(sw_half - .8115f * sh + sh/2.f),
		-(.4303f * sh),
		0.076f * sh,
		(sw_half - 1.1642f * sh) + sh / 2.f,
		sw_half + sh / 2.f
	};

	//create OpenGL buffer object
	//12 points
	float vertices[24] = {
		//left 4 points
		a[0], sh,
		a[0], 0.f,
		a[1], sh,
		a[1], 0.f,

		//mid 4 points
		a[2], sh,
		a[2], 0.f,
		a[3], sh,
		a[3], 0.f,

		//right 4 points
		a[4], sh,
		a[4], 0.f,
		a[5], sh,
		a[5], 0.f,
	};

	float uvs[24] = {
		0.f, 0.78f,
		0.f, 0.6357f,

		0.1171f, 0.78f,
		0.1171f, 0.6357f,

		0.1269f, 0.78f,
		0.1269f, 0.6357f,

		0.2f, 0.78f,
		0.2f, 0.6357f,

		0.21f, 0.78f,
		0.21f, 0.6357f,

		0.378f, 0.78f,
		0.368f, 0.6357f,
	};

	u16 indices[30] = {
		0, 1, 2,
		2, 1, 3,

		2, 3, 4,
		4, 3, 5,

		4, 5, 6,
		6, 5, 7,

		6, 7, 8,
		8, 7, 9,

		8, 9, 10,
		10, 9, 11
	};

	//vbo
	glGenBuffers(2, me->fvbo);
	glBindBuffer(GL_ARRAY_BUFFER, me->fvbo[0]);
	glBufferData(GL_ARRAY_BUFFER, 24 * sizeof(float), vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, me->fvbo[1]);
	glBufferData(GL_ARRAY_BUFFER, 24 * sizeof(float), uvs, GL_STATIC_DRAW);

	//ibo
	glGenBuffers(1, &me->fibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->fibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 30 * 2, indices, GL_STATIC_DRAW);

	//vao
	glGenVertexArrays(1, &me->fvao);
	glBindVertexArray(me->fvao);

	//link vbo to vao
	glBindBuffer(GL_ARRAY_BUFFER, me->fvbo[0]);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, me->fvbo[1]);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);
}


static void make_text_model(sign_t* me)
{
	float sh = me->sh = 0.05f;							//sign height on screen coordinates
	float bot = sh * 0.3f;								//text bottom height
	me->sw = me->sh / (float)me->th * (float)me->tw;	//sign width
	float sw_half = me->sw / 2.f;

	//create OpenGL buffer object
	float vertices[8] = {
		-sw_half, sh + bot,
		-sw_half, bot,
		sw_half, bot,
		sw_half, sh + bot,
	};

	float uvs[8] = {
		0.f, 0.f,
		0.f, 1.f,
		1.f, 1.f,
		1.f, 0.f,
	};

	u16 indices[6] = {
		0, 1, 2,
		2, 3, 0,
	};

	//vbo
	glGenBuffers(2, me->vbo);
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), uvs, GL_STATIC_DRAW);

	//ibo
	glGenBuffers(1, &me->ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 2 * 3 * 2, indices, GL_STATIC_DRAW);

	//vao
	glGenVertexArrays(1, &me->vao);
	glBindVertexArray(me->vao);

	//link vbo to vao
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[0]);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[1]);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);
}




static void text_calc_dimension(const char* text, int* tw, int* th)
{
	int w = 1;				//some character has left -1, init w to 1 to cater for this
	int h = 0;
	int chh;	//character height
	int i;
	char ch;
	font_t* font = ymge->font;
	char_info_t* ci;

	for (i = 0;; i++)
	{
		//get char until NULL
		ch = text[i];
		if (ch == ' ')
		{
			w += 10;
			continue;
		}
		else if (ch == 0)
			break;

		ci = &font->char_info[ch - 32];			//ascii char is rendered in table before hand

		w += ci->adv_x;
		chh = ci->top + ci->height;
		if (chh > h)
			h = chh;
	}

	*tw = w;
	*th = h + 1;		//extra space
}

static void text_draw(GLuint tid, const char* text)
{
	int i;
	char ch;
	int cur_x = 1;
	font_t* font = ymge->font;
	char_info_t* ci;
	u8 color = 0;

	//clear image
	glClearTexImage(tid, 0, GL_RED, GL_UNSIGNED_BYTE, &color);

	for (i = 0;; i++)
	{
		//get char until NULL
		ch = text[i];
		if (ch == ' ')
		{
			cur_x += 10;
			continue;
		}
		else if (ch == 0)
			break;

		ci = &font->char_info[ch - 32];			//ascii char is rendered in table before hand

												//draw char
		glBindTexture(GL_TEXTURE_2D, tid);
		glTexSubImage2D(GL_TEXTURE_2D, 0, cur_x + ci->left, ci->top, ci->width, ci->height, GL_RED, GL_UNSIGNED_BYTE, ci->img);

		//cursor advance
		cur_x += ci->adv_x;
	}
}

void make_texture(sign_t* me, const char* text)
{
	int tw, th;
	int error;

	//calculate texture dimension
	text_calc_dimension(text, &tw, &th);
	me->tw = tw;
	me->th = th;

	//create OpenGL texture
	//glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &me->tid);
	glBindTexture(GL_TEXTURE_2D, me->tid);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, tw, th, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
	error = glGetError();
	if (error)
	{
		LOG4("[sign] generate text, create texture failed: glTexImage2D, %d", error);
		return;
	}

	//these 2 must be set, or else the texture appeas black on screen
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//draw text
	text_draw(me->tid, text);
}


void sign_init(sign_t* me, uint fid, float* pos, const char* text)
{
	//pos
	me->t[0] = pos[0];
	me->t[1] = pos[1];
	me->t[2] = pos[2];
	me->t[3] = 1.f;

	//text
	make_texture(me, text);
	make_text_model(me);

	//frame
	me->fid = fid;
	make_frame_model(me);
}


void sign_clean(sign_t* me)
{
	glDeleteTextures(1, &me->tid);

	glDeleteBuffers(2, me->vbo);
	glDeleteBuffers(1, &me->ibo);
	glDeleteVertexArrays(1, &me->vao);

	glDeleteBuffers(2, me->fvbo);
	glDeleteBuffers(1, &me->fibo);
	glDeleteVertexArrays(1, &me->fvao);
}


static BOOL sign_cull(sign_t* me, m44* mat_vp, float* res)
{
	v4 r[4];

	//transform sign position
	v4_x_m44c(me->t, mat_vp, r);
	
	//divide w
	r[0] /= r[3];
	r[1] /= r[3];
	r[2] /= r[3];

	if (fabsf(r[0]) < 1.f && fabsf(r[1]) < 1.f && fabsf(r[2]) < 1.f)
	{
		res[0] = r[0];
		res[1] = r[1];
		return FALSE;
	}
	else
		return TRUE;
}

void sign_render(sign_t* me, m44* mat_vp)
{
	float r[2];

	//cull
	if (sign_cull(me, mat_vp, r))
		return;

	//render frame
	glUseProgram(ymge->shader_text_frame->program);
	glUniform2fv(0, 1, r);

	glBindTexture(GL_TEXTURE_2D, me->fid);

	glBindVertexArray(me->fvao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->fibo);
	glDrawElements(GL_TRIANGLES, 30, GL_UNSIGNED_SHORT, 0);

	//render text
	glUseProgram(ymge->shader_text->program);
	glUniform2fv(0, 1, r);

	glBindTexture(GL_TEXTURE_2D, me->tid);

	glBindVertexArray(me->vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->ibo);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
}

