#include <GL/glew.h>

#include "shader_constant_color.h"
#include "shader.h"

void shader_constant_color_init(shader_constant_color_t* me, const char* vertex_shader_path, const char* fragment_shader_path)
{
	shader_source_t		shaders[] = {
		{GL_VERTEX_SHADER, vertex_shader_path},
		{GL_FRAGMENT_SHADER, fragment_shader_path}
	};

	//build shader
	me->program = shader_build(2, shaders);
	if (!me->program) return;

	//get uniform locations
	me->mat_transform = glGetUniformLocation(me->program, "mat_transform");
	me->constant_color = glGetUniformLocation(me->program, "constant_color");
}


void shader_constant_color_clean(shader_constant_color_t* me)
{
	glDeleteProgram(me->program);
}

