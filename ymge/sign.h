#pragma once

#include <ymge_type.h>

typedef struct
{
	//sign position
	v4					t[4];
	

	//text texture
	uint				tid;	//texture id
	int					tw;		//texture width
	int					th;		//texture height

	//text model
	uint				vbo[2];
	uint				ibo;
	uint				vao;

	//frame texture
	uint				fid;

	//frame model
	uint				fvbo[2];
	uint				fibo;
	uint				fvao;

	//screen coordinate
	float				sh;
	float				sw;
}
sign_t;


void sign_init(sign_t* me, uint fid, float* pos, const char* text);
void sign_clean(sign_t* me);

void sign_render(sign_t* me, m44* mat_vp);