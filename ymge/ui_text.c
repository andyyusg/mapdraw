#include <GL/glew.h>

#include <logger.h>

#include "ui_text.h"
#include "matrix.h"
#include "timer.h"
#include "font.h"
#include "ymge.h"


void ui_text_init(ui_text_t* me, int x, int y, int w, int h)
{
	//create OpenGL buffer object
	float vertices[8] = {
		-1.f, -0.5f,
		-1.f, -1.f,
		1.f, -1.f,
		1.f, -0.5f,
	};

	float uvs[8] = {
		0.f, 0.f,
		0.f, 1.f,
		1.f, 1.f,
		1.f, 0.f,
	};

	u16 indices[6] = {
		0, 1, 2,
		2, 3, 0,
	};

	//char* clearColor = 0;

	//get texture dimensions
	me->tw = w;
	me->th = h;

	//left
	vertices[0] = vertices[2] = 2.f*((float)x) / ((float)ymge->window_width) - 1.f;

	//top
	vertices[1] = vertices[7] = 2.f*((float)y) / ((float)ymge->window_height) - 1.f;

	//right
	if (w == 0)
		vertices[4] = vertices[6] = 1.f;
	else
		vertices[4] = vertices[6] = 2.f*((float)x+w) / ((float)ymge->window_width) - 1.f;;
	me->tw = (int)((vertices[4] - vertices[0]) / 2.f * ((float)ymge->window_width));

	//bottom
	if (h == 0)
		vertices[3] = vertices[5] = -1.f;
	else
		vertices[3] = vertices[5] = 2.f*((float)y-h) / ((float)ymge->window_height) - 1.f;
	me->th = (int)((vertices[1] - vertices[3]) / 2.f * ((float)ymge->window_height));

	//no need
	//clearColor = malloc(tw*th);
	//memset(clearColor, 0, tw*th);

	//create OpenGL texture
	//glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &me->texture);
	glBindTexture(GL_TEXTURE_2D, me->texture);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, me->tw, me->th, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
	me->error = glGetError();
	if (me->error)
	{
		LOG4("[ui_text] init, create texture failed: glTexImage2D, %d", me->error);
		return;
	}

	//these 2 must be set, or else the texture appeas black on screen
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//free(clearColor);

	//some character has left -1
	//init cur_x to 1 to cater for this
	me->cur_x = 1;



	//vbo
	glGenBuffers(2, me->vbo);
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, 8 * 4, vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, 8 * 4, uvs, GL_STATIC_DRAW);

	//ibo
	glGenBuffers(1, &me->ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 2 * 3 * 2, indices, GL_STATIC_DRAW);

	//vao
	glGenVertexArrays(1, &me->vao);
	glBindVertexArray(me->vao);

	//link vbo to vao
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[0]);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[1]);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);
}




void ui_text_clean(ui_text_t* me)
{
	glDeleteTextures(1, &me->texture);
	glDeleteBuffers(2, me->vbo);
	glDeleteBuffers(1, &me->ibo);
	glDeleteVertexArrays(1, &me->vao);
}



void ui_text_draw(ui_text_t* me)
{
	float mat[16] = M44_IDENTITY;

	//set transform matrix
	//glUniformMatrix4fv(uniform_transform_matrix, 1, GL_FALSE, mat);

	//choose texture
	//glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, me->texture);

	glBindVertexArray(me->vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->ibo);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
}


void ui_text_add_char(ui_text_t* me, font_t* font, char ch)
{
	//ascii char is rendered in table before hand
	char_info_t* ci = &font->char_info[ch - 32];

	glBindTexture(GL_TEXTURE_2D, me->texture);
	glTexSubImage2D(GL_TEXTURE_2D, 0, me->cur_x + ci->left, me->cur_y + ci->top, ci->width, ci->height, GL_RED, GL_UNSIGNED_BYTE, ci->img);

	//cursor advance
	me->cur_x += ci->adv_x;
}

void ui_text_add_wchar(ui_text_t* me, font_t* font, uint char_height, u16 ch)
{
	if (ch > 31 && ch < 127)
	{
		ui_text_add_char(me, font, (char)ch);
	}
	else
	{
		font_render_char(font, char_height, ch);

		glBindTexture(GL_TEXTURE_2D, me->texture);
		glTexSubImage2D(GL_TEXTURE_2D, 0, me->cur_x + font->char_left, font->char_top, font->char_width, font->char_height, GL_RED, GL_UNSIGNED_BYTE, font->face->glyph->bitmap.buffer);

		//cursor advance
		me->cur_x += font->char_adv_x;
	}
}

void ui_text_add_wstring(ui_text_t* me, font_t* font, uint char_height, u16* str)
{
	u16 *p;

	for (p = str; *p != 0; p++)
	{
		ui_text_add_wchar(me, font, char_height, *p);
	}

}

void ui_text_add_string(ui_text_t* me, font_t* font, char* str)
{
	char *p;

	for (p = str; *p != 0; p++)
	{
		ui_text_add_char(me, font, *p);
	}
}

void ui_text_clear(ui_text_t* me)
{
	u8 color = 0;

	glClearTexImage(me->texture, 0, GL_RED, GL_UNSIGNED_BYTE, &color);
	me->cur_x = 1;
	me->cur_y = 0;
}


void ui_text_set_string(ui_text_t* me, font_t* font, char* str)
{
	char *p;
	char ch;
	u8 color = 0;

	//clear
	glClearTexImage(me->texture, 0, GL_RED, GL_UNSIGNED_BYTE, &color);
	me->cur_x = 1;
	me->cur_y = 0;

	for (p = str; *p != 0; p++)
	{
		ch = *p;

		if (ch == '\n')
		{
			me->cur_x = 1;
			me->cur_y += YMGE_FONT_SIZE;
		}
		else
			ui_text_add_char(me, font, ch);
	}
}



void ui_text_set_wstring(ui_text_t* me, font_t* font, uint char_height, wchar_t* str)
{
	wchar_t *p;
	wchar_t ch;
	u8 color = 0;

	//clear
	glClearTexImage(me->texture, 0, GL_RED, GL_UNSIGNED_BYTE, &color);
	me->cur_x = 1;
	me->cur_y = 0;

	for (p = str; *p != 0; p++)
	{
		ch = *p;

		if (ch == L'\n')
		{
			me->cur_x = 1;
			me->cur_y += YMGE_FONT_SIZE;
		}
		else
			ui_text_add_wchar(me, font, char_height, ch);
	}
}


