#pragma once


#include <SDL.h>





#ifdef __cplusplus
extern "C"
{
#endif

#include <timer.h>
#include <avl.h>

#include "ymge_type.h"
#include "ymge_error.h"
#include "font.h"
#include "model.h"

//shaders
#include "shader_model.h"
#include "shader_animated_model.h"
#include "shader_ui.h"
#include "shader_text.h"
#include "shader_text_frame.h"
#include "shader_constant_color.h"
#include "shader_cull.h"

#include "render_res.h"
#include "render_group.h"

//#include "terrain.h"
#include "texture.h"
#include "entity.h"
#include "camera.h"
#include "light.h"
#include "ray.h"
#include "ui.h"
#include "ui_text.h"
#include "ui_tpl.h"
#include "control.h"
#include "sign.h"



#define YMGE_CHUNK_DEPTH	3			//chunk visibility
#define YMGE_CHUNK_COUNT	YMGE_CHUNK_DEPTH * YMGE_CHUNK_DEPTH * YMGE_CHUNK_DEPTH

#define YMGE_NUM_EVENT_HANDLERS		10
#define YMGE_NUM_UPDATE_HANDLERS	10


//type
typedef int (*event_handler_t)(SDL_Event* event);
typedef void(*update_handler_t)(void);

//ymge config
typedef struct
{
	const char*			log_filename;
	uint				log_level;
	const char*			window_title;
	int					window_width;
	int					window_height;
	int					window_fullscreen;
	int					window_maximized;
	int					window_resizable;

	float				fov_half;
	float				near_plane;
	float				far_plane;
}
ymge_config_t;





//YMGE - engine
typedef struct ymge_s
{
	//config
	ymge_config_t		config;

	//window dimension
	int					window_width;
	int					window_height;

	//timer
	timer_t				time[1];			//game time
	timer_t				timer[2];

	//control
	u32					control;
	int					mouse_rel[2];



	//temp ray test result
	float				ray_test[6];

	//font
	font_t				font[1];

	//SDL
	SDL_Window*			wnd;
	SDL_GLContext		gl_ctx;

	//aspect radio
	float				ar;

	float				fov_half;
	float				near_plane;
	float				far_plane;

	//helper value
	float				fov_half_tan;
	float				fov_half_ctan;
	float				fov_half_tan_x_ar;

	//helper value for frustum culling
	float				fov_half_sin;
	float				fov_half_cos;


	//projection matrix
	m44					mat_proj[16];


	//GPU shaders
	shader_model_t					shader_model[1];
	shader_animated_model_t			shader_animated_model[1];
	shader_ui_t						shader_ui[1];
	shader_text_t					shader_text[1];
	shader_text_frame_t				shader_text_frame[1];
	shader_constant_color_t			shader_constant_color[1];

	shader_cull_t					shader_cull[1];


	//render resource
	render_res_t					render_res[1];

	//render group
	render_group_t					render_group[1];

	//camera
	camera_t			camera[1];

	//light
	light_t				light[1];

	//model
	struct avl_table*	models;

	//texture
	struct avl_table*	textures;





	//entity
	struct avl_table*		entities;
	struct avl_traverser	entities_traverser;

	//animated entity
	entity_t*				entities_animated[100];
	u16						num_entities_animated;


	//entity after culled
	entity_t*				entity_culled[100000];
	u16						num_entities_culled;

	//collided entity
	entity_t*				entity_collided[10000];
	u16						num_entities_collided;

	//selected entity
	entity_t*				entity_selected[1];

	//ui
	ui_text_t				ui_text[1];

	ui_t					ui[1];

	//sign
	sign_t					signs[1000];
	uint					num_signs;


	//event handlers
	event_handler_t			event_handlers[YMGE_NUM_EVENT_HANDLERS];
	u32						num_event_handlers;

	//update handlers
	update_handler_t		update_handlers[YMGE_NUM_UPDATE_HANDLERS];
	u32						num_update_handlers;


	//error
	int					error;
}
ymge_t;


//global variable
extern ymge_t* ymge;



//function
ymge_t* ymge_new(ymge_config_t* config);
void ymge_del(ymge_t* me);
void ymge_init(ymge_t* me);
void ymge_clean(ymge_t* me);

void ymge_run(ymge_t* me);

//load resource
int ymge_add_model(const char* uid, const char* path, render_res_t* render_res);
int ymge_add_model_from_mem(const char* id, int num_vertices, float* positions, float* uvs, float* normals, int num_indices, u16* indices, render_res_t* render_res);
int ymge_add_texture(const char* uid, const char* path, render_res_t* render_res);
int ymge_add_texture_text(const char* id, const char* text);

entity_t* ymge_add_entity(const char* uid, const char* model_uid, const char* texture_uid, render_group_t* render_group);

//update engine status
void ymge_update_proj_matrix(void);

//set event handler
//slot 0: reserved for YMGE: ymge_event()
//slot 1 ~ YMGE_NUM_EVENT_HANDLERS: for user
//set handler to 0 to remove it from specific slot
void ymge_set_event_handler(u32 slot, event_handler_t handler);


//set update handler
void ymge_set_update_handler(u32 slot, update_handler_t handler);


//enable/disable mouse exclusive
void ymge_enable_mouse_exclusive(int enable);


inline void ymge_update_mouse_ray(ymge_t* me)
{
	camera_t* camera = me->camera;

	//update mouse position
	camera->mouse_pos[0] += me->mouse_rel[0];
	camera->mouse_pos[1] += me->mouse_rel[1];

	//adjust mouse position within window space
	if (camera->mouse_pos[0] < 0)
		camera->mouse_pos[0] = 0;
	else if (camera->mouse_pos[0] > me->window_width)
		camera->mouse_pos[0] = me->window_width;

	if (camera->mouse_pos[1] < 0)
		camera->mouse_pos[1] = 0;
	else if (camera->mouse_pos[1] > me->window_height)
		camera->mouse_pos[1] = me->window_height;

	//calculate normalized mouse pos
	// nx = (x*2 - w)/w
	// ny = (h - y*2)/h
	camera->mouse_pos_n[0] = ((float)(camera->mouse_pos[0] * 2 - me->window_width)) / ((float)me->window_width);
	camera->mouse_pos_n[1] = ((float)(me->window_height - camera->mouse_pos[1] * 2)) / ((float)me->window_height);

	//get mouse position in eye space (actually direction vector - not normalized yet)
	// x = xn * tan * ar * abs(z)
	// y = yn * tan * abs(z)
	// z = - abs(z)
	//
	// xn, yn range (-1, 1) = camera->mouse_pos_n
	camera->mouse_pos_eye[0] = camera->mouse_pos_n[0] * me->fov_half_tan_x_ar;
	camera->mouse_pos_eye[1] = camera->mouse_pos_n[1] * me->fov_half_tan;
	camera->mouse_pos_eye[2] = -1.f;
	camera->mouse_pos_eye[3] = 0.f;


	// eye space to world space
	ray_eye_to_world(camera->mouse_pos_eye, camera->r, camera->mouse_ray);
}


//get model
model_t* ymge_get_model(const char* uid);

//get texture
texture_t* ymge_get_texture(const char* uid);

//get entity
entity_t* ymge_get_entity(const char* uid);






#ifdef __cplusplus
}
#endif
