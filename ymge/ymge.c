﻿#include <GL/glew.h>

#include <logger.h>
#include <file.h>
#include <timer.h>

#include "ymge.h"
#include "tri.h"
#include "matrix.h"
#include "vector.h"
#include "hwinfo.h"
#include "font.h"
#include "control.h"
#include "cull.h"
#include "ray.h"
#include "pois.h"
#include "msaa.h"


//global variable
ymge_t*			ymge = 0;


//get model
model_t* ymge_get_model(const char* uid)
{
	static model_t		model;

	model.uid_ptr = uid;
	return (model_t*)avl_find(ymge->models, &model);
}

//get texture
texture_t* ymge_get_texture(const char* uid)
{
	static texture_t	texture;

	texture.uid_ptr = uid;
	return (texture_t*)avl_find(ymge->textures, &texture);
}


//get entity
entity_t* ymge_get_entity(const char* uid)
{
	static entity_t		entity;

	entity.uid_ptr = uid;
	return (entity_t*)avl_find(ymge->entities, &entity);
}

static void ymge_print_sysinfo(ymge_t* me)
{
	int				value = 0;
	const GLubyte*	str = 0;

	hwinfo_print();

	SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &value);
	LOG0("[ymge] info, OpenGL version major: %d", value);

	SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &value);
	LOG0("[ymge] info, OpenGL version minor: %d", value);

	//vendor
	str = glGetString(GL_VENDOR);
	LOG0("[ymge] info, OpenGL vendor: %s", str);

	//version
	str = glGetString(GL_VERSION);
	LOG0("[ymge] info, OpenGL version: %s", str);

	//GLSL version
	str = glGetString(GL_SHADING_LANGUAGE_VERSION);
	LOG0("[ymge] info, OpenGL GLSL version: %s", str);

	//max uniform components
	glGetIntegerv(GL_MAX_VERTEX_UNIFORM_COMPONENTS, &value);
	LOG0("[ymge] info, OpenGL max uniform components: %d", value);


	//extensions
	if (0)
	{
		glGetIntegerv(GL_NUM_EXTENSIONS, &value);
		LOG0("[ymge] info, OpenGL extensions count: %d", value);
		for (int i = 0; i < value; i++)
		{
			str = glGetStringi(GL_EXTENSIONS, i);
			LOG0("[ymge] info, OpenGL extension: %s", str);
		}
	}
	
}


static void ymge_print_sysinfo_after_init_opengl(ymge_t* me)
{
	int value;

	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &value);
	LOG0("[ymge] info, GL_MAX_COMPUTE_WORK_GROUP_COUNT - x: %d", value);

	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &value);
	LOG0("[ymge] info, GL_MAX_COMPUTE_WORK_GROUP_COUNT - y: %d", value);

	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &value);
	LOG0("[ymge] info, GL_MAX_COMPUTE_WORK_GROUP_COUNT - z: %d", value);

	glGetIntegerv(GL_MAX_VERTEX_ATTRIB_STRIDE, &value);
	LOG0("[ymge] info, GL_MAX_VERTEX_ATTRIB_STRIDE: %d", value);
	
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &value);
	LOG0("[ymge] info, GL_MAX_VERTEX_ATTRIBS: %d", value);
}

static void ymge_init_setup_shaders(ymge_t* me)
{
	//model shader
	shader_model_init(me->shader_model, "shader\\model.vs", "shader\\model.fs");

	//animated model shader
	shader_animated_model_init(me->shader_animated_model, "shader\\animated_model.vs", "shader\\animated_model.fs");

	//ui shader
	shader_ui_init(me->shader_ui, "shader\\ui.vs", "shader\\ui.fs");

	//text shader
	shader_text_init(me->shader_text, "shader\\text.vs", "shader\\text.fs");
	shader_text_frame_init(me->shader_text_frame, "shader\\text_frame.vs", "shader\\text_frame.fs");

	//constant color shader - for lines etc.
	shader_constant_color_init(me->shader_constant_color, "shader\\constant.vs", "shader\\constant.fs");

	//cull compute shader
	shader_cull_init(me->shader_cull, "shader\\cull.cs");
}




static void ymge_on_window_resize()
{
	//get window size first, which ymge_update_proj needs
	SDL_GetWindowSize(ymge->wnd, &ymge->window_width, &ymge->window_height);
	ymge_update_proj_matrix();
	glViewport(0, 0, ymge->window_width, ymge->window_height);
}

static int ymge_on_event(SDL_Event* event)
{
	switch (event->type)
	{
	case SDL_QUIT:
		return 0;

	case SDL_WINDOWEVENT:
		if (event->window.event == SDL_WINDOWEVENT_RESIZED)
		{
			ymge_on_window_resize();
		}
		break;
	}

	//keep running
	//return 0 to quit
	return 1;
}


static void ymge_get_vp_matrix(m44* mat_vp)
{
	static m44 mat_identity[16] = M44_IDENTITY;
	m44 mat[16];

	//model-view-projection matrix
	m44r_x_m44c_r(mat_identity, ymge->camera->mat, mat);
	m44r_x_m44c_c(mat, ymge->mat_proj, mat_vp);
}

static void ymge_render_once(ymge_t* me)
{
	u32 i;
	static float constant_color[4] = { 1.f, 1.f, 1.f, 1.f };

	//msaa
	msaa_prepare();

	//clear screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);





	if (me->control & YMGE_CTRL_WIRE_RENDER)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


	//render features
	//glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glUseProgram(ymge->shader_animated_model->program);
	entity_render(me->entities_animated[0]);


	//disable transparency for 3D model to speed up rendering
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	//render resource - static
	glUseProgram(ymge->shader_model->program);
	glUniform3fv(ymge->shader_model->light_normal, 1, ymge->light[0].dir);			//light direction
	render_group_render(me->render_group);



	//restore polygon mode
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//enable transparency, without this, alpha channel doesn't have effects
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);

	//render buildings
	glUseProgram(ymge->shader_animated_model->program);
	entity_render(me->entities_animated[1]);


	//get view-projection matrix
	m44 mat_vp[16];
	ymge_get_vp_matrix(mat_vp);

	//render signs
	for (i = 0; i < me->num_signs; i++)
	{
		sign_render(me->signs + i, mat_vp);
	}

	//render poi
	pois_render(mat_vp);

	//use ui shader
	glUseProgram(me->shader_ui->program);
	ui_t* ui = me->ui;
	ui_wnd_group_t* wnd_groups = ui->wnd_groups;
	for (i = 0; i < ui->num_wnd_groups; i++)
	{
		ui_wnd_group_render(wnd_groups + i, me->shader_ui->mat_transform);
	}

	//render HUD text
	static float p[2] = { 0.f, 0.f };
	glUseProgram(me->shader_text->program);
	glUniform2fv(0, 1, p);
	ui_text_draw(me->ui_text);

	//msaa blit
	msaa_blit();
}





static void ymge_update_once(ymge_t* me)
{
	u32 i;
	int collide[4] = { 0 };
	entity_t* entity;
	update_handler_t update_handler = 0;


	//update game time
	timer_update(me->time);

	//TIMER FPS - start
	timer_start(me->timer);
	timer_start(me->timer + 1);



	//update handler
	for (i = 0; i < me->num_update_handlers; i++)
	{
		update_handler = me->update_handlers[i];
		if (update_handler)
		{
			update_handler();
		}
	}



	//shader cull
	shader_cull_exec(me->shader_cull, me->render_group);


	//reset entity culled
	me->num_entities_culled = 0;

	//frustum quarter cull
	//cull_all();
	//cull_quarter(me);
	cull_animated();




	//raytest
	if (0)
	{
		//reset collided entities collection
		me->num_entities_collided = 0;

		for (i = 0; i < me->num_entities_culled; i++)
		{
			entity = me->entity_culled[i];

			//test ray collision
			ray_test_aabb(me->camera->mouse_ray, me->camera->t, entity->aabb, me->ray_test, collide);
			entity->is_collided = collide[3];

			//add to collided collection
			if (entity->is_collided)
			{
				me->entity_collided[me->num_entities_collided++] = entity;
			}
		}
	}



/*
	if (1)
	{
		char str[1024];

		sprintf(str, 
			"Time elapsed: %f\n"
			"Entities: %u\n"
			"Entities(after culling): %u\n"
			"Camera pos: %f, %f, %f\n"
			"Camera Y rotation: %f\n"
			"Camera dir vector: %f, %f, %f\n"
			"Mouse: %d, %d\n"
			"Mouse(n): %f, %f\n"
			"Mouse(eye): %f, %f, %f, %f\n"
			"Light(eye): %f, %f, %f, %f\n",
			me->timer[0].elapsed,
			me->entities->avl_count,
			me->num_entities_culled,
			me->camera->t[0],
			me->camera->t[1],
			me->camera->t[2],
			me->camera->r[1],
			me->camera->n[0],
			me->camera->n[1],
			me->camera->n[2],
			me->camera->mouse_pos[0],
			me->camera->mouse_pos[1],
			me->camera->mouse_pos_n[0],
			me->camera->mouse_pos_n[1],
			me->camera->mouse_pos_eye[0],
			me->camera->mouse_pos_eye[1],
			me->camera->mouse_pos_eye[2],
			me->camera->mouse_pos_eye[3],
			me->light[0].dir[0],
			me->light[0].dir[1],
			me->light[0].dir[2],
			me->light[0].dir[3]
			);

		timer_start(me->timer + 1);
		ui_text_set_string(me->ui_text, me->font, str);
		timer_stop(me->timer + 1);
	}
	else
	{
		wchar_t str[256];
		swprintf(str, 256, L"Time elapsed: %f\nHello, how are you", me->timer[1].elapsed);

		timer_start(me->timer + 1);
		ui_text_set_wstring(me->ui_text, me->font, 30, str);
		timer_stop(me->timer + 1);
	}
*/
	

	//render
	ymge_render_once(me);


	//TIMER FPS - stop
	timer_stop(me->timer);

	//swap back buffer
	SDL_GL_SwapWindow(me->wnd);

	//TIMER FPS - stop
	timer_stop(me->timer + 1);
}



ymge_t* ymge_new(ymge_config_t* config)
{
	size_t size = sizeof(ymge_t);
	ymge_t* me = malloc(size);
	
	memset(me, 0, size);
	me->config = *config;
	ymge_init(me);

	return me;
}

void ymge_del(ymge_t* me)
{
	ymge_clean(me);
	free(me);
}

void ymge_enable_mouse_exclusive(int enable)
{
	SDL_SetRelativeMouseMode(enable);
}

void ymge_init_setup_opengl(ymge_t* me)
{
	//init glew
	glewExperimental = GL_TRUE;
	glewInit();

	//setup opengl clear color
	glClearColor(0.5f, 0.5f, 0.5f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	SDL_GL_SwapWindow(me->wnd);


	//setup OpenGL
	//glViewport(0, 0, me->config.window_width, me->config.window_height);
	glEnable(GL_CULL_FACE);			//remove face not facing you
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);									//set blend function
	glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_GREATER);

	//glShadeModel(GL_SMOOTH);

	//line width
	//glLineWidth(1.5f);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


	//texture
	glActiveTexture(GL_TEXTURE0);


	//setup shaders
	ymge_init_setup_shaders(me);


	//render resource
	vbo_info_t vbo_info[] = {
		{ 3 * sizeof(float), 3, GL_FLOAT, 0, 0 },				//0: pos
		{ 2 * sizeof(float), 2, GL_FLOAT, 0, 0 },				//1: uv
		{ 3 * sizeof(float), 3, GL_FLOAT, 0, 0 },				//2: normal
		{ 2 * 16 * sizeof(float), 4, GL_FLOAT, 128, 16 },		//3: mat_mvp, mat_model
		{ sizeof(float), 1, GL_FLOAT, 0, 0 }						//4: texture id
	};
	render_res_init(me->render_res, 3, 100000, vbo_info, 200000);

	//render group
	vbo_info_t vbo_info2[] = {
		{ 2 * 16 * sizeof(float), 4, GL_FLOAT, 128, 16 },		//0: mat_mvp, mat_model
		{ sizeof(float), 1, GL_FLOAT, 0, 0 }					//1: texture id
	};
	render_group_init(me->render_group, me->render_res, 2, vbo_info2, 2000000);


	me->error = glGetError();
}



void ymge_init(ymge_t* me)
{
	int err = 0;

	//assign to global variable
	ymge = me;


	//init log
	if (me->config.log_filename == 0)
	{
		me->config.log_filename = "game.log";	//default log filename
	}
	log_init(me->config.log_filename, me->config.log_level);
	LOG1("[ymge] init ********");

	//copy config
	me->window_width = me->config.window_width;
	me->window_height = me->config.window_height;

	me->fov_half = me->config.fov_half;
	me->near_plane = me->config.near_plane;
	me->far_plane = me->config.far_plane;

	//init timer
	timer_init(me->time);				//game time
	timer_init(me->timer);
	timer_init(me->timer + 1);

	//init math
	timer_start(me->timer + 1);
	tri_init();
	timer_stop(me->timer + 1);
	LOG1("[ymge] init trigonometry, time %f", me->timer[1].elapsed);

	//helper value
	tri_sin_cos(me->config.fov_half, &me->fov_half_sin, &me->fov_half_cos);		//sin, cos (theta) for frustum culling
	me->fov_half_tan = me->fov_half_sin / me->fov_half_cos;
	me->fov_half_ctan = me->fov_half_cos / me->fov_half_sin;


	//init SDL
	timer_start(me->timer + 1);
	err = SDL_Init(SDL_INIT_EVERYTHING);
	timer_stop(me->timer + 1);
	if (err)
	{
		LOG4("SDL init error: %d", err);
		me->error = YMGE_ERR_INIT_SDL;
		return;
	}
	LOG1("[ymge] init SDL OK, time %f", me->timer[1].elapsed);




	//setup OpenGL attributes
	//this should happen before window creation
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

/*	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
*/

	//create window
	{
		Uint32 flags = SDL_WINDOW_OPENGL;

		//fullscreen
		if (me->config.window_fullscreen)
			flags |= SDL_WINDOW_FULLSCREEN;
		else if (me->config.window_maximized)
			flags |= SDL_WINDOW_MAXIMIZED | SDL_WINDOW_RESIZABLE;
		else if (me->config.window_resizable)
			flags |= SDL_WINDOW_RESIZABLE;


		me->wnd = SDL_CreateWindow(me->config.window_title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, me->window_width, me->window_height, flags);
		if (me->wnd == 0)
		{
			LOG4("[ymge] init, create window error.");
			me->error = YMGE_ERR_INIT_WINDOW;
			return;
		}

		//window resize event
		ymge_on_window_resize(me);
	}

	//set SDL relative mouse mode, grab exclusive control of mouse, also mouse relative x, y works out of window
	//ymge_enable_mouse_exclusive(me, 1);

	//create OpenGL context
	me->gl_ctx = SDL_GL_CreateContext(me->wnd);
	if (me->gl_ctx == 0)
	{		
		LOG4("[ymge] init, create OpenGL context error: %s.", SDL_GetError());
		me->error = YMGE_ERR_INIT_OPENGL_CONTEXT;
		return;
	}



	//init font
	font_init(me->font);
	//font_load(me->font, "KaushanScript-Regular.otf");
	font_load(me->font, "bebas.ttf");
	//font_load(me->font, "chinese.ttf");
	if (me->font[0].error)
	{
		LOG4("[ymge] init font error: %d", me->font[0].error);
		me->error = YMGE_ERR_INIT_FONT;
		return;
	}



	//init models
	me->models = avl_create(model_compare, 0, 0);

	//init textures
	me->textures = avl_create(texture_compare, 0, 0);

	//init entities
	me->entities = avl_create(entity_compare, 0, 0);
	avl_t_init(&me->entities_traverser, me->entities);





	//init camera
	//camera_update_direction(me->camera);
	//camera_move_y(me->camera, 1.6f);

	//init light
	light_init(me->light);

	//print system info
	ymge_print_sysinfo(me);

	//setup OpenGL
	ymge_init_setup_opengl(me);	
	if (me->error)
	{
		LOG4("[ymge] init opengl error: %d", me->error);
		me->error = YMGE_ERR_INIT;
		return;
	}

	msaa_init();


	ymge_print_sysinfo_after_init_opengl(me);

	//load resources
	//ymge_init_load_resources(me);

	//set ymge internal event handler, always at slot 0
	ymge_set_event_handler(0, ymge_on_event);

	//status OK
	me->error = YMGE_ERR_OK;
}


// update projection matrix
void ymge_update_proj_matrix(void)
{
	ymge->ar = ((float)ymge->window_width) / ((float)ymge->window_height);

	//helper value
	ymge->fov_half_tan_x_ar = ymge->fov_half_tan * ymge->ar;

	m44c_set_proj(ymge->mat_proj, ymge->fov_half_ctan, ymge->ar, ymge->near_plane, ymge->far_plane);
}




void ymge_clean(ymge_t* me)
{
	//global variable
	ymge = 0;

	//msaa
	msaa_clean();

	//models
	avl_destroy(me->models, model_del);

	//textures
	avl_destroy(me->textures, texture_del);

	//entities
	avl_destroy(me->entities, entity_del);

	//ui
	ui_clean(me->ui);

	
	//terrain
	//terrain_clean(me->terrain);


	//ui text
	ui_text_clean(me->ui_text);

	//clean shaders
	shader_model_clean(me->shader_model);
	shader_animated_model_clean(me->shader_animated_model);
	shader_ui_clean(me->shader_ui);
	shader_text_clean(me->shader_text);
	shader_text_frame_clean(me->shader_text_frame);
	shader_constant_color_clean(me->shader_constant_color);
	shader_cull_clean(me->shader_cull);


	//render resource
	render_res_clean(me->render_res);

	//render group
	render_group_clean(me->render_group);

	//OpenGL context
	SDL_GL_DeleteContext(me->gl_ctx);

	//window
	SDL_DestroyWindow(me->wnd);

	//SDL
	SDL_Quit();

	//font
	font_clean(me->font);

	LOG0("[ymge] clean ========");
	log_clean();
}



void ymge_run(ymge_t* me)
{
	SDL_Event	event;
	int			isRunning = 1;
	event_handler_t handler = 0;

	//start game time
	timer_start(me->time);

	while (isRunning)
	{
		//reset mouse relative
		me->mouse_rel[0] = 0;
		me->mouse_rel[1] = 0;

		//handle SDL events
		while (SDL_PollEvent(&event))
		{
			for (u32 i = 0; i < me->num_event_handlers; i++)
			{
				handler = me->event_handlers[i];
				if (handler)
				{
					isRunning = handler(&event);
					if (!isRunning)
						break;
				}
			}
			if (!isRunning)
				break;
		}

		//engine run
		ymge_update_once(me);
	}
}


void ymge_set_event_handler(u32 slot, event_handler_t handler)
{
	u32 num_event_handlers = YMGE_NUM_EVENT_HANDLERS;
	u32 num_handlers_new = 0;
	u32 i;
	
	//slot range check
	if (slot >= YMGE_NUM_EVENT_HANDLERS)
		return;

	//set event handler
	ymge->event_handlers[slot] = handler;

	//reset num event handler
	if (slot >= ymge->num_event_handlers)
		num_event_handlers = slot + 1;
	else
		num_event_handlers = ymge->num_event_handlers;

	for (i = 0; i < num_event_handlers; i++)
	{
		if (ymge->event_handlers[i])
			num_handlers_new = i;
	}
	ymge->num_event_handlers = num_handlers_new + 1;
}


void ymge_set_update_handler(u32 slot, update_handler_t handler)
{
	u32 num_handlers = YMGE_NUM_UPDATE_HANDLERS;
	u32 num_handlers_new = 0;
	u32 i;

	//slot range check
	if (slot >= YMGE_NUM_UPDATE_HANDLERS)
		return;

	//set event handler
	ymge->update_handlers[slot] = handler;

	//reset num event handler
	if (slot >= ymge->num_update_handlers)
		num_handlers = slot + 1;
	else
		num_handlers = ymge->num_update_handlers;

	for (i = 0; i < num_handlers; i++)
	{
		if (ymge->update_handlers[i])
			num_handlers_new = i;
	}
	ymge->num_update_handlers = num_handlers_new + 1;
}



int ymge_add_model(const char* id, const char* path, render_res_t* render_res)
{
	model_t** res = 0;
	model_t* model = model_new(id);

	//insert to avl tree
	res = (model_t**)avl_probe(ymge->models, model);

	if (res == 0)
	{
		LOG4("[ymge] add model FAILED!! memory issue(%d)", YMGE_ERR_ADD_MODEL_MEMORY);
		free(model);
		return YMGE_ERR_ADD_MODEL_MEMORY;
	}
	else if (*res != model)
	{
		LOG4("[ymge] add model FAILED!! uid duplicate(%d)", YMGE_ERR_ADD_MODEL_DUPLICATE);
		free(model);
		return YMGE_ERR_ADD_MODEL_DUPLICATE;
	}

	//load model
	model_load_from_file(model, path, render_res);

	return YMGE_ERR_OK;
}


int ymge_add_model_from_mem(const char* id, int num_vertices, float* positions, float* uvs, float* normals, int num_indices, u16* indices, render_res_t* render_res)
{
	model_t** res = 0;
	model_t* model = model_new(id);

	//insert to avl tree
	res = (model_t**)avl_probe(ymge->models, model);

	if (res == 0)
	{
		LOG4("[ymge] add model FAILED!! memory issue(%d)", YMGE_ERR_ADD_MODEL_MEMORY);
		free(model);
		return YMGE_ERR_ADD_MODEL_MEMORY;
	}
	else if (*res != model)
	{
		LOG4("[ymge] add model FAILED!! uid duplicate(%d)", YMGE_ERR_ADD_MODEL_DUPLICATE);
		free(model);
		return YMGE_ERR_ADD_MODEL_DUPLICATE;
	}

	//load model
	if(render_res)
	{
		model->render_res = render_res;

		//NEW: render resource
		render_indirect_params_t* param = &model->render_indirect_param;
		param->count = num_indices;
		param->primCount = 1;
		param->firstIndex = render_res->index_ptr;
		param->baseVertex = render_res->vertex_ptr;
		param->baseInstance = 0;

		render_res_add_model(render_res, num_vertices, positions, uvs, normals, num_indices, indices);
	}
	else
	{
		model_load(model, num_vertices, positions, uvs, normals, 0, 0, num_indices, indices);
	}

	return YMGE_ERR_OK;
}

int ymge_add_texture(const char* id, const char* path, render_res_t* render_res)
{
	texture_t** res = 0;
	texture_t* texture = texture_new(id);

	//insert to avl tree
	res = (texture_t**)avl_probe(ymge->textures, texture);

	if (res == 0)
	{
		LOG4("[ymge] add texture FAILED!! memory issue(%d)", YMGE_ERR_ADD_TEXTURE_MEMORY);
		free(texture);
		return YMGE_ERR_ADD_TEXTURE_MEMORY;
	}
	else if (*res != texture)
	{
		LOG4("[ymge] add texture FAILED!! uid duplicate(%d)", YMGE_ERR_ADD_TEXTURE_DUPLICATE);
		free(texture);
		return YMGE_ERR_ADD_TEXTURE_DUPLICATE;
	}

	//load texture
	texture_load(texture, path, render_res);

	return YMGE_ERR_OK;
}


int ymge_add_texture_text(const char* id, const char* text)
{
	texture_t** res = 0;
	texture_t* texture = texture_new(id);

	//insert to avl tree
	res = (texture_t**)avl_probe(ymge->textures, texture);

	if (res == 0)
	{
		LOG4("[ymge] add texture FAILED!! memory issue(%d)", YMGE_ERR_ADD_TEXTURE_MEMORY);
		free(texture);
		return YMGE_ERR_ADD_TEXTURE_MEMORY;
	}
	else if (*res != texture)
	{
		LOG4("[ymge] add texture FAILED!! uid duplicate(%d)", YMGE_ERR_ADD_TEXTURE_DUPLICATE);
		free(texture);
		return YMGE_ERR_ADD_TEXTURE_DUPLICATE;
	}

	//load texture
	texture_gen_text(texture, text);

	return YMGE_ERR_OK;
}


entity_t* ymge_add_entity(const char* uid, const char* model_uid, const char* texture_uid, render_group_t* render_group)
{
	entity_t**			res = 0;
	entity_t*			entity = 0;
	model_t*			model = 0;
	texture_t*			texture = 0;

	//get model
	model = ymge_get_model(model_uid);
	if (model == 0)
	{
		LOG4("[ymge] add entity FAILED!! fetch model");
		return 0;
	}

	//get texture
	texture = ymge_get_texture(texture_uid);
	if (texture == 0)
	{
		LOG4("[ymge] add entity FAILED!! fetch texture");
		return 0;
	}

	//check render resource
	if (model->render_res != texture->render_res)
	{
		LOG4("[ymge] add entity FAILED!! model and texture render_res doesn't match");
		return 0;
	}

	//check render group and render resource
	if(render_group && render_group->render_res != model->render_res)
	{
		LOG4("[ymge] add entity FAILED!! render_group and render_res don't match");
		return 0;
	}




	//NEW: Render Resource
	if (render_group)
	{
		//render_group_add_entity(render_group, &model->render_indirect_param, &texture->render_res_texture_slot);
		entity = render_group_add_entity(render_group, uid, model, texture);
	}
	else
	{
		//new entity
		entity = entity_new(uid);

		//insert to avl tree
		res = (entity_t**)avl_probe(ymge->entities, entity);
		if (res == 0)
		{
			LOG4("[ymge] add entity FAILED!! memory issue(%d)", YMGE_ERR_ADD_ENTITY_MEMORY);
			free(entity);
			return 0;
		}
		else if (*res != entity)
		{
			LOG4("[ymge] add entity FAILED!! uid duplicate(%d)", YMGE_ERR_ADD_ENTITY_DUPLICATE);
			free(entity);
			return 0;
		}


		//no render group
		entity->render_group = 0;
		entity->render_group_instance_id = -1;

		//add to animated entity
		//if (model->animations)
		{
			ymge->entities_animated[ymge->num_entities_animated++] = entity;
		}

		//init entity
		entity_init(entity, model, texture);
	}



	return entity;
}







