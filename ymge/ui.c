#include <stdlib.h>

#include "ui.h"




void ui_init_templates(ui_t* me, u32 count)
{
	me->templates = calloc(count, sizeof(ui_tpl_t));
	me->num_templates = count;
}

void ui_clean_templates(ui_t* me)
{
	ui_tpl_t* tpl = 0;

	for (u32 i = 0; i < me->num_templates; i++)
	{
		tpl = me->templates + i;
		if (tpl->initialized)
			ui_tpl_clean(tpl);
	}

	if (me->num_templates)
		free(me->templates);
}



void ui_init_wnd_groups(ui_t* me, u32 count)
{
	me->wnd_groups = calloc(count, sizeof(ui_wnd_group_t));
	me->num_wnd_groups = count;
}

void ui_clean_wnd_groups(ui_t* me)
{
	ui_wnd_group_t* wnd_group = 0;

	for (u32 i = 0; i < me->num_wnd_groups; i++)
	{
		wnd_group = me->wnd_groups + i;
		if (wnd_group->initialized)
			ui_wnd_group_clean(wnd_group);
	}

	if (me->num_wnd_groups)
		free(me->wnd_groups);
}


void ui_init(ui_t* me, u32 num_templates, u32 num_wnd_groups)
{
	ui_init_templates(me, num_templates);
	ui_init_wnd_groups(me, num_wnd_groups);
}


void ui_clean(ui_t* me)
{
	ui_clean_templates(me);
	ui_clean_wnd_groups(me);
}




