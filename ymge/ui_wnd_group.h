#pragma once

#include "ui_wnd.h"
#include "texture.h"

typedef struct ui_wnd_group_s
{
	int				initialized;
	ui_wnd_t		root[1];

	char			uid[100];

	int				is_active;

	uint			vbo[2];		//vertex buffer object
	uint			ibo;		//index buffer object
	uint			vao;		//vertex array object

	//float			vertices[16 * 3];
	float*			vertices;
	float*			uvs;
	u16*			indices;

	uint			num_vertices;
	uint			num_uvs;
	uint			num_indices;

	int				pos_vertices;
	int				pos_uvs;
	int				pos_indices;
	int				next_index;		//index


	texture_t*		texture;	//texture

	int				error;
}
ui_wnd_group_t;




//function
void ui_wnd_group_clean(ui_wnd_group_t* me);

void ui_wnd_group_set_texture(ui_wnd_group_t* me, texture_t* texture);
void ui_wnd_group_prepare(ui_wnd_group_t* me);
void ui_wnd_group_render(ui_wnd_group_t* me, int uniform_transform_matrix);