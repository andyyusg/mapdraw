#include <GL/glew.h>

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <file.h>
#include <buf.h>
#include <logger.h>
#include <avl.h>

#include "model.h"
#include "matrix.h"
#include "ymge.h"

typedef struct
{
	u16			file_type;

	//vertex
	u16			num_vertex;
	float*		vertex_pos;
	float*		vertex_uv;
	float*		vertex_normal;
	short*		vertex_joint;
	float*		vertex_weight;


	//index
	u16			num_index;
	u16*		index;


	//joint
	u16			num_joint;

}model_loader_t;

//declaration
void model_load_from_file_model(model_t* me, const char* model_path, render_res_t* render_res);
void model_load_from_file_obj(model_t* me, const char* model_path);


// load VBO: Vertex Buffer Object
static void model_load_vbo(model_t* me, int slot, i64 size, void* data, int unit_size, int unit_type)
{
	vbo_t* vbo = &me->vbo[slot];

	//create
	glGenBuffers(1, &vbo->id);

	//bind
	glBindBuffer(GL_ARRAY_BUFFER, vbo->id);

	//load data
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);

	//set unit info
	vbo->unit_size = unit_size;
	vbo->unit_type = unit_type;
}


// load VIO: Vertex Indices
static void model_load_vio(model_t* me, uint size, void* data)
{
	//create
	glGenBuffers(1, &me->vio);

	//bind
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->vio);

	//load data
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
}


//load VAO: Vertex Array Object
static void model_load_vao(model_t* me)
{
	uint i;

	//create VAO
	glGenVertexArrays(1, &me->vao);

	//bind VAO
	glBindVertexArray(me->vao);

	//loop - link VBO to VAO
	for (i = 0; i < me->num_vbo; i++)
	{
		//get VBO
		vbo_t*	vbo = &me->vbo[i];


		//bind VBO
		glBindBuffer(GL_ARRAY_BUFFER, vbo->id);
		if(vbo->unit_type == GL_SHORT)
			glVertexAttribIPointer(i, vbo->unit_size, vbo->unit_type, 0, 0);
		else
			glVertexAttribPointer(i, vbo->unit_size, vbo->unit_type, GL_FALSE, 0, 0);

		//link to VAO
		glEnableVertexAttribArray(i);
	}
}

static void model_calculate_aabb(model_t* me, i64 num_vertices, const float* vertices)
{
	i64 i, j;
	float x, y, z;
	float dist_max = 0.f;

	//init aabb
	me->aabb[0] = me->aabb[1] = vertices[0];
	me->aabb[2] = me->aabb[3] = vertices[1];
	me->aabb[4] = me->aabb[5] = vertices[2];

	for (i = 1; i < num_vertices; i++)
	{
		j = i * 3;
		x = vertices[j];
		y = vertices[j + 1];
		z = vertices[j + 2];

		//get x bounding
		if (x < me->aabb[0])
			me->aabb[0] = x;
		else if (x > me->aabb[1])
			me->aabb[1] = x;

		//get y bounding
		if (y < me->aabb[2])
			me->aabb[2] = y;
		else if (y > me->aabb[3])
			me->aabb[3] = y;

		//get x bounding
		if (z < me->aabb[4])
			me->aabb[4] = z;
		else if (z > me->aabb[5])
			me->aabb[5] = z;
	}
}

void model_load(model_t* me, uint num_vertices, const float* vertices, const float* uvs, const float* normals, const short* joints, const float* weights, uint num_indices, const u16* indices)
{
	uint		vertices_size = num_vertices * 3 * sizeof(float);
	uint		uvs_size = num_vertices * 2 * sizeof(float);
	uint		normals_size = num_vertices * 3 * sizeof(float);
	uint		joints_size = num_vertices * 3 * sizeof(short);
	uint		weights_size = num_vertices * 3 * sizeof(float);
	uint		indices_size = num_indices * sizeof(u16);

	//load model data to OpenGL
	me->num_vbo = 3;
	model_load_vbo(me, 0, vertices_size, (void*)vertices, 3, GL_FLOAT);
	model_load_vbo(me, 1, uvs_size, (void*)uvs, 2, GL_FLOAT);
	model_load_vbo(me, 2, normals_size, (void*)normals, 3, GL_FLOAT);
	if (me->type)
	{
		me->num_vbo = 5;
		model_load_vbo(me, 3, joints_size, (void*)joints, 3, GL_SHORT);
		model_load_vbo(me, 4, weights_size, (void*)weights, 3, GL_FLOAT);
	}
	model_load_vio(me, indices_size, (void*)indices);
	model_load_vao(me);

	//set vertex index count
	me->num_index = num_indices;

}





static int avl_compare(const void* a, const void* b, void* params)
{
	u16* ia = (u16*)a;
	u16* ib = (u16*)b;

	int res = memcmp(a, b, sizeof(u16) * 3);
	return res;
}



void model_load_from_file(model_t* me, const char* model_path, render_res_t* render_res)
{
	size_t len = strlen(model_path);

	//invoke corresponding model loader
	if (len > 6 && strcmp(model_path + len - 6, ".model") == 0)
	{
		model_load_from_file_model(me, model_path, render_res);
	}
	else
	{
		model_load_from_file_obj(me, model_path);
	}
}


static void loader_read(void* to, char** ptr, size_t size)
{
	memcpy(to, *ptr, size);
	*ptr += size;
}

//*.model
//my own format
void model_load_from_file_model(model_t* me, const char* model_path, render_res_t* render_res)
{
	int					i;
	u16					nFileType = 1;
	size_t				nRead = 0;
	file_t				file = { 0 };
	buf_t*				file_content = 0;
	char*				p = 0;
	model_loader_t		loader = { 0 };

	//read model file
	if (file_open(&file, model_path, "rb"))
	{
		file_content = file_read_all(&file);
		file_close(&file);
	}
	else
	{
		LOG4("[model] load model file(%s) error.", model_path);
		return;
	}

	//parse start
	p = file_content->data;

	//file type
	//0: just model
	//1: plus joint weight
	//2: plus animation
	loader_read(&loader.file_type, &p, sizeof(u16));

	//-------------------------------------------------
	//vertex

	//vertex count
	loader_read(&loader.num_vertex, &p, sizeof(u16));

	//vertex pos
	nRead = loader.num_vertex * 3 * sizeof(float);
	loader.vertex_pos = malloc(nRead);
	loader_read(loader.vertex_pos, &p, nRead);

	//vertex uv
	nRead = loader.num_vertex * 2 * sizeof(float);
	loader.vertex_uv = malloc(nRead);
	loader_read(loader.vertex_uv, &p, nRead);

	//vertex normal
	nRead = loader.num_vertex * 3 * sizeof(float);
	loader.vertex_normal = malloc(nRead);
	loader_read(loader.vertex_normal, &p, nRead);

	if (loader.file_type)
	{
		//vertex weight
		nRead = loader.num_vertex * 3 * sizeof(float);
		loader.vertex_weight = malloc(nRead);
		loader_read(loader.vertex_weight, &p, nRead);

		//vertex joint
		nRead = loader.num_vertex * 3 * sizeof(short);
		loader.vertex_joint = malloc(nRead);
		loader_read(loader.vertex_joint, &p, nRead);
	}

	//-------------------------------------------------
	//index

	//index count
	loader_read(&loader.num_index, &p, sizeof(u16));

	//indices
	nRead = loader.num_index * sizeof(u16);
	loader.index = malloc(nRead);
	loader_read(loader.index, &p, nRead);


	//-------------------------------------------------
	//joint
	if (loader.file_type)
	{
		//joint count
		loader_read(&me->num_joint, &p, sizeof(u16));
		me->joints = malloc(sizeof(joint_t)*me->num_joint);
		me->joint_transform = malloc(sizeof(float) * 16 * me->num_joint);

		//read each joint
		for (i = 0; i < me->num_joint; i++)
		{
			u8 len;
			joint_t* joint = me->joints + i;

			//name
			loader_read(&len, &p, sizeof(u8));
			loader_read(joint->name, &p, len);
			joint->name[len] = 0;

			//remember root id
			if (strcmp(joint->name, "Root") == 0)
			{
				me->joint_root_id = i;
			}


			//Obsolete
			/*
			//4 x 4 inverse transform matrix
			loader_read(joint->inverse_transform, &p, sizeof(float)*16);

			//matrix read from file is column major, converted to row major to use in engine
			m44_transpose(joint->inverse_transform, joint->inverse_transform);
			*/


			//location
			loader_read(joint->location_relative, &p, sizeof(float) * 3);

			//flip location y and
			if (0)
			{
				float y = joint->location_relative[1];
				float z = joint->location_relative[2];
				joint->location_relative[1] = z;
				joint->location_relative[2] = y;
			}

			//child
			loader_read(&joint->num_child, &p, 1);
			if (joint->num_child)
			{
				loader_read(joint->child_id, &p, joint->num_child);
			}
		}

		//calculate absolute location relative to model origin
		//location_inverse too
		{
			float location_root[3] = { 0.f, 0.f, 0.f };
			joint_calculate_location(me->joints + me->joint_root_id, me->joints, location_root);
		}
	}



	//get aabb
	model_calculate_aabb(me, loader.num_vertex, loader.vertex_pos);


	//is animatable
	me->type = loader.file_type?1:0;

	if (render_res)
	{
		me->render_res = render_res;

		//NEW: render resource
		render_indirect_params_t* param = &me->render_indirect_param;
		param->count = loader.num_index;
		param->primCount = 1;
		param->firstIndex = render_res->index_ptr;
		param->baseVertex = render_res->vertex_ptr;
		param->baseInstance = 0;

		render_res_add_model(render_res, loader.num_vertex, loader.vertex_pos, loader.vertex_uv, loader.vertex_normal, loader.num_index, loader.index);
	}
	else
	{
		//load model
		model_load(me, loader.num_vertex, loader.vertex_pos, loader.vertex_uv, loader.vertex_normal, loader.vertex_joint, loader.vertex_weight, loader.num_index, loader.index);
	}


	//clean
	free(file_content);
	free(loader.vertex_pos);
	free(loader.vertex_uv);
	free(loader.vertex_normal);
	if (loader.file_type)
	{
		free(loader.vertex_weight);
		free(loader.vertex_joint);
	}
	free(loader.index);
}

//*.obj
//wavefront obj
void model_load_from_file_obj(model_t* me, const char* model_path)
{
	file_t		file = { 0 };
	buf_t*		buf = 0;
	size_t		len_left;
	char*		p = 0;
	char*		p2 = 0;
	char*		p3 = 0;
	float		x, y, z;
	u16			idx[3];
	int			i,j,k,l,m,n;
	int			base;
	u16			q, qq;
	u16			r;			//final pass indices count

	uint		num_vertices = 0;
	uint		num_uvs = 0;
	uint		num_normals = 0;
	uint		num_faces = 0;


	//set path
	strcpy_s(me->path, 255, model_path);

	//vertex data
	float*		vertex_pos = 0;
	float*		vertex_uv = 0;
	float*		vertex_normal = 0;
	u16*		vertex_index = 0;

	float*		pos = 0;
	float*		uvs = 0;
	float*		normals = 0;
	u16*		indices = 0;

	struct avl_table*	avl = avl_create(avl_compare, 0, 0);
	u16*				index = 0;
	u16**				p_index_add = 0;

	//read model
	if (file_open(&file, model_path, "rb"))
	{
		buf = file_read_all(&file);
		file_close(&file);
	}
	else
	{
		LOG4("[model] load model file(%s) error.", model_path);
		return;
	}

	//prepare parse
	p = buf->data;
	len_left = buf->len;

	//parse loop - get number of vertices
	for (;;)
	{
		if (array_compare_string(p, 2, "v ") == 0)
		{
			num_vertices++;
		}
		else if (array_compare_string(p, 3, "vt ") == 0)
		{
			num_uvs++;
		}
		else if (array_compare_string(p, 3, "vn ") == 0)
		{
			num_normals++;
		}
		else if (array_compare_string(p, 2, "f ") == 0)
		{
			num_faces++;
		}

		//next line
		p2 = array_search(p, len_left, "\n", 1);
		if (p2)
		{
			p2++;
			len_left -= p2 - p;
			if (len_left)
				p = p2;
			else
				break;
		}
		else
			break;
	}

	//alloc memory for vertex data
	pos = malloc(num_vertices * 3 * sizeof(float));
	uvs = malloc(num_uvs * 2 * sizeof(float));						//uvs and normals are 1st pass parse, vertex_uv and vertex_normal are final data to be passed to OpenGL
	normals = malloc(num_normals * 3 * sizeof(float));
	indices = malloc(num_faces * 3 * 4 * sizeof(u16));

	vertex_pos = malloc(num_faces * 3 * 3 * sizeof(float));
	vertex_uv = malloc(num_faces * 3 * 2 * sizeof(float));
	vertex_normal = malloc(num_faces * 3 * 3 * sizeof(float));
	vertex_index = malloc(num_faces * 3 * sizeof(u16));


	//prepare parse
	p = buf->data;
	len_left = buf->len;

	//parse loop
	for (i=0, j=0, k=0, l=0, q=0, r=0;;)
	{
		//get vertex position
		if (array_compare_string(p, 2, "v ") == 0)
		{
			//point p to vertex's 1st float value (x)
			p += 2;
			len_left -= 2;

			//get x
			p2 = array_search(p, len_left, " ", 1);
			*p2 = 0;
			x = (float)atof(p);

			//advance p
			len_left -= p2 - p + 1;
			p = p2 + 1;

			//get y
			p2 = array_search(p, len_left, " ", 1);
			*p2 = 0;
			y = (float)atof(p);

			//advance p
			len_left -= p2 - p + 1;
			p = p2 + 1;

			//get z
			p2 = array_search(p, len_left, "\n", 1);
			*p2 = 0;
			z = (float)atof(p);

			//advance p
			len_left -= p2 - p + 1;
			p = p2 + 1;

			//store vertex position
			pos[3 * i] = x;
			pos[3 * i + 1] = y;
			pos[3 * i + 2] = z;

			//advance i
			i++;

			continue;
		}

		//get vertex uv
		if (array_compare_string(p, 3, "vt ") == 0)
		{
			//point p to vertex's 1st float value (u)
			p += 3;
			len_left -= 3;

			//get u
			p2 = array_search(p, len_left, " ", 1);
			*p2 = 0;
			x = (float)atof(p);

			//advance p
			len_left -= p2 - p + 1;
			p = p2 + 1;

			//get v
			p2 = array_search(p, len_left, "\n", 1);
			*p2 = 0;
			y = (float)atof(p);

			//advance p
			len_left -= p2 - p + 1;
			p = p2 + 1;

			//store vertex uv
			uvs[2 * j] = x;
			uvs[2 * j + 1] = y;

			//advance j
			j++;

			continue;
		}

		//get vertex normal
		if (array_compare_string(p, 3, "vn ") == 0)
		{
			//point p to vertex's 1st float value (x)
			p += 3;
			len_left -= 3;

			//get x
			p2 = array_search(p, len_left, " ", 1);
			*p2 = 0;
			x = (float)atof(p);

			//advance p
			len_left -= p2 - p + 1;
			p = p2 + 1;

			//get y
			p2 = array_search(p, len_left, " ", 1);
			*p2 = 0;
			y = (float)atof(p);

			//advance p
			len_left -= p2 - p + 1;
			p = p2 + 1;

			//get z
			p2 = array_search(p, len_left, "\n", 1);
			*p2 = 0;
			z = (float)atof(p);

			//advance p
			len_left -= p2 - p + 1;
			p = p2 + 1;

			//store vertex position
			normals[3 * k] = x;
			normals[3 * k + 1] = y;
			normals[3 * k + 2] = z;

			//advance k
			k++;

			continue;
		}

		//get face
		//NOTE: face's vertex index starts from 1 not 0
		if (array_compare_string(p, 2, "f ") == 0)
		{
			//point p to vertex's 1st float value (x)
			p += 2;
			len_left -= 2;

			for (n = 0; n < 3; n++)
			{
				//get face index: pos/uv/normal
				for (m = 0; m < 3; m++)
				{
					if (m < 2)
						p2 = array_search(p, len_left, "/", 1);
					else
					{
						if(n == 2)
							p2 = array_search(p, len_left, "\n", 1);
						else
							p2 = array_search(p, len_left, " ", 1);
					}

					*p2 = 0;

					base = 12 * l + 4 * n + m;
					indices[base] = idx[m] = (u16)strtoul(p, &p3, 10) - 1;			//NOTE: OBJ file face's vertex index starts from 1 not 0, so convert it to 0 based

					//advance p
					len_left -= p2 - p + 1;
					p = p2 + 1;
				}

				index = indices + 12 * l + 4 * n;
				p_index_add = (u16**)avl_probe(avl, index);
				if (p_index_add == 0)
					LOG4("[model] load from file fail, avl_probe memory.");
				else if(*p_index_add == index)
				{
					//insert OK

					//write q to indices's 4th position
					*(index + 3) = qq = q;

					//store vertex position
					vertex_pos[3 * q] = pos[idx[0] * 3];
					vertex_pos[3 * q + 1] = pos[idx[0] * 3 + 1];
					vertex_pos[3 * q + 2] = pos[idx[0] * 3 + 2];

					//store vertex uv
					vertex_uv[2 * q] = uvs[idx[1] * 2];
					vertex_uv[2 * q + 1] = uvs[idx[1] * 2 + 1];

					//store vertex normal
					vertex_normal[3 * q] = normals[idx[2] * 3];
					vertex_normal[3 * q + 1] = normals[idx[2] * 3 + 1];
					vertex_normal[3 * q + 2] = normals[idx[2] * 3 + 2];

					//advance q, q is index of final pass vertex index
					q++;
				}
				else
				{
					//insert FAIL, duplicate
					
					//get index from table
					index = *p_index_add;

					qq = *(index + 3);
				}

				//store vertex index
				vertex_index[r] = qq;

				//advance r, final pass indices count
				r++;
			}


			//advance l
			l++;

			continue;
		}

		//next line
		p2 = array_search(p, len_left, "\n", 1);
		if (p2)
		{
			p2++;
			len_left -= p2 - p;
			if (len_left)
				p = p2;
			else
				break;
		}
		else
			break;
	}


	//get aabb
	model_calculate_aabb(me, q, vertex_pos);


	model_load(me, q, vertex_pos, vertex_uv, vertex_normal, 0, 0, r, vertex_index);


	//clean
	avl_destroy(avl, 0);
	buf_del(buf);
	free(pos);
	free(uvs);
	free(normals);
	free(indices);
	free(vertex_pos);
	free(vertex_uv);
	free(vertex_normal);
	free(vertex_index);
}

void model_init_animation(model_t* me, u16 num_animation)
{
	me->num_animation = num_animation;

	me->animations = malloc(sizeof(animation_t) * num_animation);

	for (int i = 0; i < num_animation; i++)
	{
		animation_init(me->animations + i);
	}
}


void model_unload(model_t* me)
{
	uint i = 0;

	glDeleteVertexArrays(1, &me->vao);
	glDeleteBuffers(1, &me->vio);
	for (i = 0; i < me->num_vbo; i++)
		glDeleteBuffers(1, &me->vbo[i].id);

	if (me->joints)
		free(me->joints);

	if (me->joint_transform)
		free(me->joint_transform);

	//animation
	if (me->num_animation)
	{
		for (i = 0; i < me->num_animation; i++)
		{
			animation_clean(me->animations + i);
		}

		free(me->animations);
	}
}

model_t* model_new(const char* id)
{
	model_t* me = malloc(sizeof(model_t));
	memset(me, 0, sizeof(model_t));
	strcpy_s(me->uid, 100, id);
	me->uid_ptr = me->uid;
	return me;
}

void model_del(model_t* me, void* param)
{
	model_unload(me);
	free(me);
}


void model_update_animation(model_t* me, animation_t* anim, m44* mat_joint_transform, u16 curr_keyframe, float t, v4* root_pos)
{
	m44 mat_root[16] = M44_IDENTITY;

	m44c_set_t(mat_root, root_pos);

	animation_update_joint_transformation(anim, me->joints, (u8)(me->joint_root_id), curr_keyframe, t, mat_root, mat_joint_transform);
}

void model_render(model_t* me)
{
	glBindVertexArray(me->vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->vio);
	glDrawElements(GL_TRIANGLES, me->num_index, GL_UNSIGNED_SHORT, 0);
}



int model_compare(const model_t* a, const model_t* b, void* param)
{
	return strcmp(a->uid_ptr, b->uid_ptr);
}


void model_compare_anim(model_t* me, model_t* m2)
{
	int res = 0;

	for (int i = 0; i < me->num_joint; i++)
	{
		joint_t* j = me->joints + i;
		joint_t* j2 = m2->joints + i;

		res = strcmp(j->name, j2->name);
		if (res)
			LOG4("[model_compare_anim] name mismatch!");

	
		if (j->num_child != j2->num_child)
			LOG4("[model_compare_anim] num_child mismatch!");

		for (int k = 0; k < j->num_child; k++)
		{
			if(j->child_id[k] != j2->child_id[k])
				LOG4("[model_compare_anim] child_id mismatch!");
		}

		res = memcmp(j->inverse_transform, j2->inverse_transform, 64);
		if (res)
			LOG4("[model_compare_anim] inverse_transform mismatch!");

		res = memcmp(j->location, j2->location, 12);
		if (res)
			LOG4("[model_compare_anim] location mismatch!");

		res = memcmp(j->location_inverse, j2->location_inverse, 12);
		if (res)
			LOG4("[model_compare_anim] location_inverse mismatch!");

		res = memcmp(j->location_relative, j2->location_relative, 12);
		if (res)
			LOG4("[model_compare_anim] location_relative mismatch!");

	}
}



void model_loader_load(model_loader_t* loader, const char* model_path)
{
	u16					nFileType = 1;
	size_t				nRead = 0;
	file_t				file = { 0 };
	buf_t*				file_content = 0;
	char*				p = 0;

	//read model file
	if (file_open(&file, model_path, "rb"))
	{
		file_content = file_read_all(&file);
		file_close(&file);
	}
	else
	{
		LOG4("[model] load model file(%s) error.", model_path);
		return;
	}

	//parse start
	p = file_content->data;

	//file type
	//0: just model
	//1: plus joint weight
	//2: plus animation
	loader_read(&loader->file_type, &p, sizeof(u16));

	//-------------------------------------------------
	//vertex

	//vertex count
	loader_read(&loader->num_vertex, &p, sizeof(u16));

	//vertex pos
	nRead = loader->num_vertex * 3 * sizeof(float);
	loader->vertex_pos = malloc(nRead);
	loader_read(loader->vertex_pos, &p, nRead);

	//vertex uv
	nRead = loader->num_vertex * 2 * sizeof(float);
	loader->vertex_uv = malloc(nRead);
	loader_read(loader->vertex_uv, &p, nRead);

	//vertex normal
	nRead = loader->num_vertex * 3 * sizeof(float);
	loader->vertex_normal = malloc(nRead);
	loader_read(loader->vertex_normal, &p, nRead);

	if (loader->file_type)
	{
		//vertex weight
		nRead = loader->num_vertex * 3 * sizeof(float);
		loader->vertex_weight = malloc(nRead);
		loader_read(loader->vertex_weight, &p, nRead);

		//vertex joint
		nRead = loader->num_vertex * 3 * sizeof(short);
		loader->vertex_joint = malloc(nRead);
		loader_read(loader->vertex_joint, &p, nRead);
	}

	//-------------------------------------------------
	//index

	//index count
	loader_read(&loader->num_index, &p, sizeof(u16));

	//indices
	nRead = loader->num_index * sizeof(u16);
	loader->index = malloc(nRead);
	loader_read(loader->index, &p, nRead);





	//get aabb
	//model_calculate_aabb(me, loader->num_vertex, loader->vertex_pos);


	//is animatable
	//me->type = loader->file_type ? 1 : 0;

	//load model
	//model_load(me, loader->num_vertex, loader->vertex_pos, loader->vertex_uv, loader->vertex_normal, loader->vertex_joint, loader->vertex_weight, loader->num_index, loader->index);


	//clean
	free(file_content);
}



void model_loader_clean(model_loader_t* loader)
{
	//clean
	free(loader->vertex_pos);
	free(loader->vertex_uv);
	free(loader->vertex_normal);
	if (loader->file_type)
	{
		free(loader->vertex_weight);
		free(loader->vertex_joint);
	}
	free(loader->index);
}


//debug purpose only
void model_compare_file(const char* model_path, const char* model_path2)
{
	model_loader_t loader = { 0 };
	model_loader_t loader2 = { 0 };

	model_loader_load(&loader, model_path);
	model_loader_load(&loader2, model_path2);

	model_loader_clean(&loader);
	model_loader_clean(&loader2);
}

