#pragma once

#include "ymtype.h"

typedef struct
{
	uint			fbo;
	uint			rbo[2];
}
msaa_t;


//function
void msaa_init();
void msaa_clean();

void msaa_prepare();
void msaa_blit();