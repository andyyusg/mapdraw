#include <GL/glew.h>
#include <stdlib.h>


#include "ui.h"
#include "ui_wnd.h"
#include "ui_tpl.h"


void ui_wnd_init(ui_wnd_t* me, ui_tpl_wnd_t* tpl, int pos_style, int x, int y, int w, int h, u32 num_imgs, u32 num_wnds)
{
	me->initialized = 1;

	//template
	me->tpl = tpl;

	//layout
	me->pos_style = pos_style;
	me->x = x;
	me->y = y;
	me->w = w;
	me->h = h;

	//child images
	me->num_imgs = num_imgs;
	if (num_imgs)
	{
		me->imgs = calloc(me->num_imgs, sizeof(ui_img_t));
	}

	//child windows
	me->num_wnds = num_wnds;
	if (num_wnds)
	{
		me->wnds = calloc(me->num_wnds, sizeof(ui_wnd_t));
	}
}


void ui_wnd_clean(ui_wnd_t* me)
{
	if (me->initialized)
	{
		//clean child windows
		for (u32 i = 0; i < me->num_wnds; i++)
		{
			ui_wnd_clean(me->wnds + i);
		}

		if (me->imgs)
		{
			free(me->imgs);
			me->num_imgs = 0;
			me->imgs = 0;
		}

		if (me->wnds)
		{
			free(me->wnds);
			me->num_wnds = 0;
			me->wnds = 0;
		}
	}
}

void ui_wnd_set_img(ui_wnd_t* me, u32 slot, ui_tpl_img_t* tpl, ui_layout_t* layout)
{
	ui_img_t* img = me->imgs + slot;
	img->tpl = tpl;
	img->layout = *layout;
}



void ui_wnd_count_wnds(ui_wnd_t* me, int* num_wnds)
{
	ui_wnd_t* wnd = 0;

	(*num_wnds)++;

	for (u32 i = 0; i < me->num_wnds; i++)
	{
		wnd = me->wnds + i;
		ui_wnd_count_wnds(wnd, num_wnds);
	}
}
