#include <math.h>

#include "tri.h"



float	tri_table[YMGE_TRI_TABLE_UNITS + 1];
float*	tri_table_sin;
float*	tri_table_cos;

float	tri_table_degrees;
float	tri_table_precision;
int		tri_table_units;
int		tri_table_4q_units;


void tri_init()
{
	int i;
	float radian_max = YMGE_TRI_PI * ((float)YMGE_TRI_TABLE_QUARTERS) / 2.0f;
	float radian;

	//init tri values
	tri_table_degrees = YMGE_TRI_TABLE_DEGREES;
	tri_table_precision = YMGE_TRI_TABLE_PRECISION;
	tri_table_units = YMGE_TRI_TABLE_UNITS;
	tri_table_4q_units = 360 * YMGE_TRI_TABLE_PRECISION;

	for (i = 0; i <= tri_table_units; i++)
	{
		radian = ((float)i) / ((float)tri_table_units);
		radian *= radian_max;
		tri_table[i] = sinf(radian);
	}

	//init tri tables
	tri_table_sin = tri_table;
	tri_table_cos = tri_table + 90 * YMGE_TRI_TABLE_PRECISION;
}




