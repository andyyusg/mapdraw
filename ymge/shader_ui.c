#include <GL/glew.h>

#include "shader_ui.h"
#include "shader.h"

void shader_ui_init(shader_ui_t* me, const char* vertex_shader_path, const char* fragment_shader_path)
{
	shader_source_t		shaders[] = {
		{GL_VERTEX_SHADER, vertex_shader_path},
		{GL_FRAGMENT_SHADER, fragment_shader_path}
	};

	//build shader
	me->program = shader_build(2, shaders);
	if (!me->program) return;

	//get uniform locations
	me->mat_transform = glGetUniformLocation(me->program, "mat_transform");
}


void shader_ui_clean(shader_ui_t* me)
{
	glDeleteProgram(me->program);
}

