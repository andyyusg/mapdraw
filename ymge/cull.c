#include "ymge.h"
//#include "GL/glew.h"


static void cull_add_entity(entity_t* entity)
{
	ymge->entity_culled[ymge->num_entities_culled] = entity;
	ymge->num_entities_culled++;
}

static void cull_frustum(entity_t* entity)
{
	v4 pos_entity_in_frustum[4];

	//frustum cull
	v4_x_m44c(entity->aabb_center, ymge->camera->mat, pos_entity_in_frustum);		//convert entity location to camera coordinate

	//hornor aspect ratio on x-axis
	pos_entity_in_frustum[0] /= ymge->ar;

	//distance = -abs(x or y) * cos - z * sin, distance > 0 if inside frustum, distance < 0 if outside
	// or
	//distance = abs(x or y) * cos + z * sin, distance < 0 if inside frustum, distance > 0 if outside
	//negative z is where the camera is facing
	float z_times_sin = pos_entity_in_frustum[2] * ymge->fov_half_sin;
	float distance_xz = (pos_entity_in_frustum[0]<0 ? -pos_entity_in_frustum[0] : pos_entity_in_frustum[0])* ymge->fov_half_cos + z_times_sin;
	float distance_yz = (pos_entity_in_frustum[1]<0 ? -pos_entity_in_frustum[1] : pos_entity_in_frustum[1])* ymge->fov_half_cos + z_times_sin;

	if (distance_xz < entity->bounding_radius[3] && distance_yz < entity->bounding_radius[3])
	{
		cull_add_entity(entity);
	}

	// check result
	/*
	if (entity->render_res_instance_id >= 0)
	{
		float aabb[5];
		float dist_xz = 0;
		float dist_yz = 0;
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, gYmge->render_res->ssbo_test);
		glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, entity->render_res_instance_id * 8, 4, &dist_xz);
		glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, entity->render_res_instance_id * 8 + 4, 4, &dist_yz);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, gYmge->render_res->ssbo_aabb);
		glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, entity->render_res_instance_id * 20, 20, aabb);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	}
	*/
	
}

void cull_all()
{
	entity_t* en;

	en = avl_t_first(&ymge->entities_traverser, ymge->entities);

	do
	{
		cull_frustum(en);
	} 
	while (en = avl_t_next(&ymge->entities_traverser));
}


void cull_animated()
{
	int count = ymge->num_entities_animated;
	entity_t** entities = ymge->entities_animated;
	entity_t* entity = 0;

	for (int i = 0; i < count; i++)
	{
		entity = entities[i];
		cull_frustum(entity);
	}
}


void cull_quarter(ymge_t* ymge)
{
	entity_t* en;

	float cx = ymge->camera->t[0];
	float cz = ymge->camera->t[2];
	float cr = ymge->camera->r[1];

	en = avl_t_first(&ymge->entities_traverser, ymge->entities);

	//get quarter
	if (cr < 45.f)
	{
		//Q1 Q4
		do
		{
			if (en->aabb[4] < cz)
			{
				cull_frustum(en);
			}
		} while (en = avl_t_next(&ymge->entities_traverser));
	}
	else if (cr < 135.f)
	{
		//Q1 Q2
		do
		{
			if (en->aabb[0] < cx)
			{
				cull_frustum(en);
			}
		} while (en = avl_t_next(&ymge->entities_traverser));
	}
	else if (cr < 225.f)
	{
		//Q2 Q3
		do
		{
			if (en->aabb[5] > cz)
			{
				cull_frustum(en);
			}
		} while (en = avl_t_next(&ymge->entities_traverser));
	}
	else if (cr < 315.f)
	{
		//Q3 Q4
		do
		{
			if (en->aabb[1] > cx)
			{
				cull_frustum(en);
			}
		} while (en = avl_t_next(&ymge->entities_traverser));
	}
	else
	{
		//Q1 Q4
		do
		{
			if (en->aabb[4] < cz)
			{
				cull_frustum(en);
			}
		} while (en = avl_t_next(&ymge->entities_traverser));
	}
}










/* obsolete
void cull_quarter_chunk(ymge_t* ymge)
{
	int i;
	chunk_piece_t* piece;
	entity_t* en;

	float cx = ymge->camera->t[0];
	float cz = ymge->camera->t[2];
	float cr = ymge->camera->r[1];

	//chunk loop
	chunk_t* chunk;
	for (int c = 0; c < YMGE_CHUNK_COUNT; c++)
	{
		chunk = ymge->chunks[c];

		//point to first piece
		piece = chunk->first;
		if (piece == 0)
			return;
		en = piece->entity;

		//get quarter
		if (cr < 45.f)
		{
			//Q1 Q4
			for (;;)
			{
				if (en->aabb[4] < cz)
				{
					cull_frustum(en);
				}

				//advance
				if (piece->next_index == -1)
					break;
				piece = chunk->pieces + piece->next_index;
				en = piece->entity;
			};
		}
		else if (cr < 135.f)
		{
			//Q1 Q2
			for (;;)
			{
				if (en->aabb[0] < cx)
				{
					cull_frustum(en);
				}

				//advance
				if (piece->next_index == -1)
					break;
				piece = chunk->pieces + piece->next_index;
				en = piece->entity;
			};
		}
		else if (cr < 225.f)
		{
			//Q2 Q3
			for (;;)
			{
				if (en->aabb[5] > cz)
				{
					cull_frustum(en);
				}

				//advance
				if (piece->next_index == -1)
					break;
				piece = chunk->pieces + piece->next_index;
				en = piece->entity;
			};
		}
		else if (cr < 315.f)
		{
			//Q3 Q4
			for (;;)
			{
				if (en->aabb[1] > cx)
				{
					cull_frustum(en);
				}

				//advance
				if (piece->next_index == -1)
					break;
				piece = chunk->pieces + piece->next_index;
				en = piece->entity;
			};
		}
		else
		{
			//Q1 Q4
			for (;;)
			{
				if (en->aabb[4] < cz)
				{
					cull_frustum(en);
				}

				//advance
				if (piece->next_index == -1)
					break;
				piece = chunk->pieces + piece->next_index;
				en = piece->entity;
			};
		}
	}
}
*/
