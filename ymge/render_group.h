#pragma once

#include <avl.h>

#include "entity.h"
#include "ymge_type.h"
#include "render_res.h"

#define YMGE_RENDER_GROUP_NUM_VBO			3




typedef struct render_group_s
{
	render_res_t*	render_res;

	uint			vbo[YMGE_RENDER_GROUP_NUM_VBO];
	i64				vbo_size[YMGE_RENDER_GROUP_NUM_VBO];
	int				vbo_ptr[YMGE_RENDER_GROUP_NUM_VBO];
	vbo_info_t		vbo_info[YMGE_RENDER_GROUP_NUM_VBO];
	int				num_vbo;

	uint			vao;		//vertex array

	uint			ssbo_aabb;	//aabb shader storage buffer for culling
	uint			ssbo_test;

	uint			dib;		//draw indirect buffer

	//instance
	uint			size_instances;	//total instances capacity
	uint			num_instances;	//count of actual instances stored

	//entities
	struct avl_table*		entities_avl;
	entity_t*				entities;
}
render_group_t;




//function
void render_group_init(render_group_t* me, render_res_t* render_res, int num_vbo, vbo_info_t* vbo_info, uint size_instances);
void render_group_clean(render_group_t* me);



entity_t*  render_group_add_entity(render_group_t* me, const char* uid, model_t* model, texture_t* texture);
entity_t* render_group_get_entity(render_group_t* me, const char* uid);

void render_group_update_model_matrix(render_group_t* me, int instance_id, float* mat);
void render_group_update_aabb(render_group_t* me, int instance_id, float* aabb_center, float* bounding_radius);

void render_group_render(render_group_t* me);