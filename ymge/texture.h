#pragma once

#include "ymge_type.h"
#include "render_res.h"

typedef struct texture_s
{
	char				uid[100];				//unique id
	char				path[255];				//path to model

	//uid pointer - for comaprison
	const char*			uid_ptr;


	uint				id;		//OpenGL texture id

	u32					w;
	u32					h;

	int					bit_depth;
	int					color_type;


	//render resource
	float				render_res_texture_slot;
	render_res_t*		render_res;

	int					error;
}
texture_t;


texture_t* texture_new(const char* uid);
void texture_del(texture_t* me, void* param);


//function
void texture_load(texture_t* me, const char* path, render_res_t* render_res);
void texture_gen_text(texture_t* me, const char* text);
void texture_unload(texture_t* me);


int texture_compare(const texture_t* a, const texture_t* b, void* param);