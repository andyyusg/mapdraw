#pragma once

#include "ymge_type.h"


typedef struct
{
	uint			program;
	
	//uniform
	int				mat_transform;
	int				constant_color;
}
shader_constant_color_t;



//function
void shader_constant_color_init(shader_constant_color_t* me, const char* vertex_shader_path, const char* fragment_shader_path);
void shader_constant_color_clean(shader_constant_color_t* me);