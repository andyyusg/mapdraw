#include <stdlib.h>
#include "ui_tpl.h"

void ui_tpl_init(ui_tpl_t* me, float width, float height, int num_imgs, int num_wnds)
{
	me->initialized = 1;

	me->width = width;
	me->height = height;

	//imgs
	me->num_imgs = num_imgs;
	me->imgs = calloc(num_imgs, sizeof(ui_tpl_img_t));

	//wnds
	me->num_wnds = num_wnds;
	me->wnds = calloc(num_wnds, sizeof(ui_tpl_wnd_t));
}


void ui_tpl_clean(ui_tpl_t* me)
{
	free(me->imgs);
}

//input unit is pixel
//output ui_tpl_img_t unit is normalized uv
void ui_tpl_set_img(ui_tpl_t* me, int slot, float x, float y, float w, float h)
{
	ui_tpl_img_t* img = me->imgs + slot;

	img->x = x;
	img->y = y;
	img->w = w;
	img->h = h;

	img->u1 = x / me->width;
	img->v1 = y / me->height;
	img->u2 = img->u1 + w / me->width;
	img->v2 = img->v1 + h / me->height;
}

ui_tpl_wnd_t* ui_tpl_set_wnd(ui_tpl_t* me, int slot, float x0, float x1, float x2, float x3, float y0, float y1, float y2, float y3)
{
	ui_tpl_wnd_t* wnd = me->wnds + slot;
	int i;

	wnd->x[0] = (int)x0;
	wnd->x[1] = (int)x1;
	wnd->x[2] = (int)x2;
	wnd->x[3] = (int)x3;

	wnd->y[0] = (int)y0;
	wnd->y[1] = (int)y1;
	wnd->y[2] = (int)y2;
	wnd->y[3] = (int)y3;

	for (i = 0; i < 4; i++)
	{
		wnd->u[i] = (float)wnd->x[i] / (float)me->width;
		wnd->v[i] = (float)wnd->y[i] / (float)me->height;
	}

	return wnd;
}

