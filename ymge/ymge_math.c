#include <math.h>

#include <timer.h>
#include <logger.h>

#include "ymge_math.h"
#include "tri.h"
#include "matrix.h"







void math_test(float* a, float* r)
{
	timer_t			tmr;
	float			t[16] = M44_IDENTITY;
	float			s[16] = M44_IDENTITY;
	float			txs[16];

	//trigonometry
	float			th = 45.1f;
	float			sin, cos;
	float			v;

	timer_init(&tmr);

	m44_set_t(t, 3, 4, 5);
	m44_set_s(s, 2, 2, 2);

	int max = 1000000;

	for (int j = 0; j < 2; j++)
	{
		timer_start(&tmr);
		for (int i = 0; i < max; i++)
		{
			th = 45.14f + (float)i / max;
			tri_sin_cos(th, &sin, &cos);
		}
		timer_stop(&tmr);
		LOG0("[math] test trigonometry MINE result: %f %f, time used %f", sin, cos, tmr.elapsed);

		timer_start(&tmr);
		for (int i = 0; i < max; i++)
		{
			th = 45.14f + (float)i / max;
			th *= (3.1415926f / 180.f);
			sin = sinf(th);
			cos = cosf(th);
		}
		timer_stop(&tmr);
		LOG0("[math] test trigonometry C Library result: %f %f, time used %f", sin, cos, tmr.elapsed);
	}

	for (int j = 0; j < 2; j++)
	{
		timer_start(&tmr);
		for (int i = 0; i < max; i++)
		{
			th = 0.f + (float)i/(float)max;
			v = tri_atan(th);
		}
		timer_stop(&tmr);
		LOG0("[math] test trigonometry arc-tan MINE result: %f, time used %f", v * YMGE_TRI_180_DIV_PI, tmr.elapsed);

		timer_start(&tmr);
		for (int i = 0; i < max; i++)
		{
			th = 0.f + (float)i/(float)max;
			v = atanf(th);
		}
		timer_stop(&tmr);
		LOG0("[math] test trigonometry arc-tan C Library result: %f, time used %f", v * YMGE_TRI_180_DIV_PI, tmr.elapsed);
	}

	//matrix test
	timer_start(&tmr);
	for (int i = 0; i < max; i++)
	{
		m44_x_m44t(t, s, txs);
	}
	timer_stop(&tmr);
	LOG0("[math] test matrix result: %f %f %f %f, time used %f", txs[0], txs[1], txs[2], txs[3], tmr.elapsed);

}
