#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>

#include <logger.h>

#include "ymge_error.h"
#include "render_group.h"





void render_group_init(render_group_t* me, render_res_t* render_res, int num_vbo, vbo_info_t* vbo_info, uint size_instances)
{
	//assign render res
	me->render_res = render_res;

	//copy settings
	me->num_vbo = num_vbo;
	me->size_instances = size_instances;
	memcpy(me->vbo_info, vbo_info, sizeof(vbo_info_t)*num_vbo);

	//entities
	me->entities = calloc(size_instances, sizeof(entity_t));
	me->entities_avl = avl_create(entity_compare, 0, 0);

	//generate vertex array
	glGenVertexArrays(1, &me->vao);


	//bind vertex array
	glBindVertexArray(me->vao);

	//setup render_res vertex attribute
	int va_id = 0;
	for (int i = 0; i < render_res->num_vbo; i++)
	{
		//allocate vbo buffer
		glBindBuffer(GL_ARRAY_BUFFER, render_res->vbo[i]);
		int error = glGetError();

		//setup vertex attribute
		int stride = render_res->vbo_info[i].stride;

		if (stride == 0)
		{
			glVertexAttribPointer(va_id, render_res->vbo_info[i].unit_size, render_res->vbo_info[i].unit_type, GL_FALSE, stride, 0);
			glEnableVertexAttribArray(va_id);

			//increase va_id
			va_id++;
		}
		else
		{
			i64 times = stride / render_res->vbo_info[i].offset;
			i64 start_offset = 0;
			for (i64 j = 0; j < times; j++)
			{
				glVertexAttribPointer(va_id, render_res->vbo_info[i].unit_size, render_res->vbo_info[i].unit_type, GL_FALSE, stride, (const void*)start_offset);
				glEnableVertexAttribArray(va_id);
				error = glGetError();

				//increase va_id
				va_id++;

				//increase start offset
				start_offset += render_res->vbo_info[i].offset;
			}
		}
	}



	//init vbo
	//vertex buffer
	glGenBuffers(num_vbo, me->vbo);

	for (int i = 0; i < num_vbo; i++)
	{
		//per instance vbo
		me->vbo_size[i] = size_instances * vbo_info[i].unit_bytes;
		me->vbo_ptr[i] = 0;

		//allocate vbo buffer
		glBindBuffer(GL_ARRAY_BUFFER, me->vbo[i]);
		glBufferStorage(GL_ARRAY_BUFFER, me->vbo_size[i], 0, GL_DYNAMIC_STORAGE_BIT);
		//glBufferData(GL_ARRAY_BUFFER, me->vbo_size[i], 0, GL_STATIC_DRAW);


		//setup vertex attribute
		int stride = vbo_info[i].stride;

		if (stride == 0)
		{
			glVertexAttribPointer(va_id, vbo_info[i].unit_size, vbo_info[i].unit_type, GL_FALSE, stride, 0);
			glVertexAttribDivisor(va_id, 1);
			glEnableVertexAttribArray(va_id);

			//increase va_id
			va_id++;
		}
		else
		{
			i64 times = stride / vbo_info[i].offset;
			i64 start_offset = 0;
			for (i64 j = 0; j < times; j++)
			{
				glVertexAttribPointer(va_id, vbo_info[i].unit_size, vbo_info[i].unit_type, GL_FALSE, stride, (const void*)start_offset);
				glVertexAttribDivisor(va_id, 1);
				glEnableVertexAttribArray(va_id);

				//increase va_id
				va_id++;

				//increase start offset
				start_offset += vbo_info[i].offset;
			}
		}
	}

	//unbind vertex array
	glBindVertexArray(0);



	//ssbo aabb for culling
	glGenBuffers(1, &me->ssbo_aabb);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, me->ssbo_aabb);
	glBufferStorage(GL_SHADER_STORAGE_BUFFER, size_instances * 20, 0, GL_DYNAMIC_STORAGE_BIT);			//storage for vec4 aabb_center and 1 float bouding_radius, total 20 bytes

																										//ssbo test, should be removed
	glGenBuffers(1, &me->ssbo_test);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, me->ssbo_test);
	glBufferStorage(GL_SHADER_STORAGE_BUFFER, size_instances * 8, 0, GL_DYNAMIC_STORAGE_BIT);


	//draw indirect buffer
	glGenBuffers(1, &me->dib);
	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, me->dib);
	glBufferStorage(GL_DRAW_INDIRECT_BUFFER, size_instances * sizeof(render_indirect_params_t), 0, GL_DYNAMIC_STORAGE_BIT);
	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, 0);
}



void render_group_clean(render_group_t* me)
{
	//entities
	avl_destroy(me->entities_avl, entity_clean);
	free(me->entities);

	glDeleteBuffers(me->num_vbo, me->vbo);
	glDeleteBuffers(1, &me->ssbo_aabb);
	glDeleteBuffers(1, &me->ssbo_test);
	glDeleteBuffers(1, &me->dib);

	glDeleteVertexArrays(1, &me->vao);
}



entity_t* render_group_add_entity(render_group_t* me, const char* uid, model_t* model, texture_t* texture)
{
	render_indirect_params_t* render_indirect_param = &model->render_indirect_param;
	float* texture_id = &texture->render_res_texture_slot;
	entity_t* entity = me->entities + me->num_instances;

	//set uid: for insertion into avl tree
	entity_set_uid(entity, uid);

	//insert into entities avl
	//insert to avl tree
	entity_t** res = (entity_t**)avl_probe(me->entities_avl, entity);
	if (res == 0)
	{
		LOG4("[render_group] add entity FAILED!! memory issue(%d)", YMGE_ERR_ADD_ENTITY_MEMORY);
		return 0;
	}
	else if (*res != entity)
	{
		LOG4("[render_group] add entity FAILED!! uid duplicate(%d)", YMGE_ERR_ADD_ENTITY_DUPLICATE);
		return 0;
	}

	//setup render group info
	entity->render_group = me;
	entity->render_group_instance_id = me->num_instances;

	//render resource add entity
	model->render_indirect_param.baseInstance = entity->render_group_instance_id;

	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, me->dib);
	glBufferSubData(GL_DRAW_INDIRECT_BUFFER, render_indirect_param->baseInstance * sizeof(render_indirect_params_t), sizeof(render_indirect_params_t), render_indirect_param);

	//upload texture slot to GPU
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[1]);
	glBufferSubData(GL_ARRAY_BUFFER, render_indirect_param->baseInstance * 4, 4, texture_id);

	//increase instance count
	me->num_instances++;

	//init entity
	entity_init(entity, model, texture);

	return entity;
}


//get entity
entity_t* render_group_get_entity(render_group_t* me, const char* uid)
{
	static entity_t		entity;

	entity.uid_ptr = uid;
	return (entity_t*)avl_find(me->entities_avl, &entity);
}



void render_group_update_model_matrix(render_group_t* me, int instance_id, float* mat)
{
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[0]);
	glBufferSubData(GL_ARRAY_BUFFER, instance_id * 128 + 64, 64, mat);
}

void render_group_update_aabb(render_group_t* me, int instance_id, float* aabb_center, float* bounding_radius)
{
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, me->ssbo_aabb);
	glBufferSubData(GL_SHADER_STORAGE_BUFFER, instance_id * 20, 16, aabb_center);
	glBufferSubData(GL_SHADER_STORAGE_BUFFER, instance_id * 20 + 16, 4, bounding_radius);
}



void render_group_render(render_group_t* me)
{
	render_res_t* render_res = me->render_res;

	//check result
	/*	if (0)
	{
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, gYmge->render_res->vbo[3]);
	glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, entity->render_res_instance_id * 128, 64, mat3);
	glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, entity->render_res_instance_id * 128 + 64, 64, mat4);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	}
	*/

	//use texture 
	glBindTexture(GL_TEXTURE_2D_ARRAY, render_res->texture_array->id);

	glBindVertexArray(me->vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, render_res->vio);
	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, me->dib);
	glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_SHORT, 0, me->num_instances, 0);
}

