#pragma once

#include "ymge_type.h"
#include "texture_array.h"

#define YMGE_RENDER_RES_NUM_VBO			3








typedef struct render_res_s
{
	int				error;

	uint			vbo[YMGE_RENDER_RES_NUM_VBO];
	i64				vbo_size[YMGE_RENDER_RES_NUM_VBO];
	int				vbo_ptr[YMGE_RENDER_RES_NUM_VBO];
	vbo_info_t		vbo_info[YMGE_RENDER_RES_NUM_VBO];
	int				num_vbo;

	int				num_va;

	int				size_vertices;
	i64				size_indices;


	uint			vio;		//index buffer
	int				vio_ptr;
	i64				vio_size;

	int				vertex_ptr;	//counts of vertices
	int				index_ptr;	//counts of indices


	//texture array
	texture_array_t	texture_array[1];
	uint			num_texture;
}
render_res_t;



//function
void render_res_init(render_res_t* me, int num_vbo, int size_vertices, vbo_info_t* vbo_info, i64 size_indices);
void render_res_clean(render_res_t* me);

void render_res_add_model(render_res_t* me, int num_vertices, const float* vertices_ptr, const float* uvs_ptr, const float* normals_ptr, int num_indices, const u16* indices_ptr);
void render_res_add_texture(render_res_t* me, const char* texture_file_path);


