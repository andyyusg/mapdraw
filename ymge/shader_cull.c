#include <GL/glew.h>
#include "shader.h"
#include "shader_cull.h"

#include "ymge.h"


void shader_cull_init(shader_cull_t* me, const char* shader_path)
{
	shader_source_t		shaders[] = {
		{ GL_COMPUTE_SHADER, shader_path }
	};

	//build shader
	me->program = shader_build(1, shaders);
	if (!me->program) return;

	//shader storage buffer
	glGenBuffers(1, &me->ssbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, me->ssbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, 2 * 64 + 12, 0, GL_STATIC_DRAW);					//storage for 2 mat4 and 1 float aspect ratio, 1 float fov_half_sin, 1 float fov_half_cos
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, me->ssbo);								//bind to  "binding=0" buffer object in shader file
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}



void shader_cull_clean(shader_cull_t* me)
{
	glDeleteProgram(me->program);
	glDeleteBuffers(1, &me->ssbo);
}



void shader_cull_exec(shader_cull_t* me, render_group_t* render_group)
{

	//prepare buf_sys
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, me->ssbo);
	glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, 64, ymge->camera->mat);
	glBufferSubData(GL_SHADER_STORAGE_BUFFER, 64, 64, ymge->mat_proj);
	glBufferSubData(GL_SHADER_STORAGE_BUFFER, 128, 4, &ymge->ar);
	glBufferSubData(GL_SHADER_STORAGE_BUFFER, 132, 4, &ymge->fov_half_sin);
	glBufferSubData(GL_SHADER_STORAGE_BUFFER, 136, 4, &ymge->fov_half_cos);

	//prepare buf_aabb
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, render_group->ssbo_aabb);			//bind to  "binding=1" buffer object in shader file
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, render_group->vbo[0]);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, render_group->dib);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, render_group->ssbo_test);

	//use shader
	glUseProgram(me->program);
	glDispatchCompute(render_group->num_instances, 1, 1);



	//wait result
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	//check result
	if (0)
	{
		render_indirect_params_t param;
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, render_group->dib);
		glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, sizeof(param) * 1, sizeof(param), &param);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	}
}
