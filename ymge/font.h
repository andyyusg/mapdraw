#pragma once

#include <ft2build.h>
#include FT_FREETYPE_H

#include "ymge_type.h"


#define YMGE_FONT_SIZE		20


typedef struct
{
	int			left;
	int			top;
	int			width;
	int			height;
	int			adv_x;

	char		img[YMGE_FONT_SIZE * YMGE_FONT_SIZE];
}
char_info_t;


typedef struct
{
	FT_Library		ft;		//freetype library
	FT_Face			face;	//freetype face

	//char			char_img[150 * 150 * 4];	//character image - 100 x 100 RGBA
	int				char_width;
	int				char_height;
	int				char_adv_x;					//character advance x
	int				char_left;					//character left offset
	int				char_top;					//character top offset;

	/* NOT USED */
	int				atlas_w;
	int				atlas_h;
	uint			atlas_texture;

	char_info_t		char_info[95];

	int				error;
}
font_t;



//function
void font_init(font_t* me);
void font_clean(font_t* me);

void font_load(font_t* me, const char* path);
void font_unload(font_t* me);

void font_render_char(font_t* me, uint char_height, wchar_t ch);