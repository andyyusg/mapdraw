#pragma once

#include "ymge_type.h"


typedef struct
{
	uint			program;
	
	//uniform
	int				mat_transform;
}
shader_ui_t;



//function
void shader_ui_init(shader_ui_t* me, const char* vertex_shader_path, const char* fragment_shader_path);
void shader_ui_clean(shader_ui_t* me);