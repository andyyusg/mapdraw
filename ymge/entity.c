#include <GL/glew.h>

#include "ymge.h"
#include "entity.h"
#include "matrix.h"



static void entity_update_aabb_local(entity_t* me, float* model_aabb)
{
	float* bounding_radius = me->bounding_radius;
	float* aabb_local = me->aabb_local;

	//init AABB local
	memcpy(aabb_local, model_aabb, 24);

	//calculate bounding radius
	// 0 ~ 2 for each axis x,y,z
	// bounding_radius[3] is final radius
	bounding_radius[0] = aabb_local[1] - aabb_local[0];
	bounding_radius[1] = aabb_local[3] - aabb_local[2];
	bounding_radius[2] = aabb_local[5] - aabb_local[4];
	bounding_radius[3] = 0.f;
	__m128 m = _mm_mul_ps(_mm_load_ps(bounding_radius), _mm_set1_ps(0.5f));
	_mm_store_ps(bounding_radius, m);

	//calculate rsqrt
	m = _mm_mul_ps(m, m);
	m = _mm_hadd_ps(m, m);
	m = _mm_hadd_ps(m, m);
	_mm_store_ss(bounding_radius + 3, _mm_rsqrt_ps(m));

	//rsqrt to sqrt
	//bounding_radius[3] now is actual bounding radius
	bounding_radius[3] = 1.f / bounding_radius[3];
}




void entity_init(entity_t* me, model_t* model, texture_t* texture)
{
	//model, texture
	me->model = model;
	me->texture = texture;

	//init AABB local
	entity_update_aabb_local(me, model->aabb);


	//update transformation
	entity_update_transformation(me);

	//prepare AABB rendering
	float aabb_vertex[24];
	u16 aabb_index[24] = {
		//front
		0, 1,
		1, 2,
		2, 3,
		3, 0,

		//back
		4, 5,
		5, 6,
		6, 7,
		7, 4,


		//bottom
		1, 5,
		2, 6,

		//top
		0, 4,
		3, 7
	};

	//0
	aabb_vertex[0] = me->aabb_local[0];
	aabb_vertex[1] = me->aabb_local[3];
	aabb_vertex[2] = me->aabb_local[5];

	//1
	aabb_vertex[3] = me->aabb_local[0];
	aabb_vertex[4] = me->aabb_local[2];
	aabb_vertex[5] = me->aabb_local[5];

	//2
	aabb_vertex[6] = me->aabb_local[1];
	aabb_vertex[7] = me->aabb_local[2];
	aabb_vertex[8] = me->aabb_local[5];

	//3
	aabb_vertex[9] = me->aabb_local[1];
	aabb_vertex[10] = me->aabb_local[3];
	aabb_vertex[11] = me->aabb_local[5];

	//4
	aabb_vertex[12] = me->aabb_local[0];
	aabb_vertex[13] = me->aabb_local[3];
	aabb_vertex[14] = me->aabb_local[4];

	//5
	aabb_vertex[15] = me->aabb_local[0];
	aabb_vertex[16] = me->aabb_local[2];
	aabb_vertex[17] = me->aabb_local[4];

	//6
	aabb_vertex[18] = me->aabb_local[1];
	aabb_vertex[19] = me->aabb_local[2];
	aabb_vertex[20] = me->aabb_local[4];

	//7
	aabb_vertex[21] = me->aabb_local[1];
	aabb_vertex[22] = me->aabb_local[3];
	aabb_vertex[23] = me->aabb_local[4];


	glGenBuffers(2, me->vbo_aabb);
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo_aabb[0]);
	glBufferData(GL_ARRAY_BUFFER, 24 * sizeof(float), aabb_vertex, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->vbo_aabb[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 24 * sizeof(u16), aabb_index, GL_STATIC_DRAW);

	glGenVertexArrays(1, &me->vao_aabb);
	glBindVertexArray(me->vao_aabb);
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo_aabb[0]);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);
}

void entity_clean(entity_t* me, void* param)
{
	glDeleteVertexArrays(1, &me->vao_aabb);
	glDeleteBuffers(2, me->vbo_aabb);
}

entity_t* entity_new(const char* uid)
{
	entity_t* me = malloc(sizeof(entity_t));
	memset(me, 0, sizeof(entity_t));

	//set uid
	entity_set_uid(me, uid);

	return me;
}

void entity_set_uid(entity_t* me, const char* uid)
{
	strcpy_s(me->uid, 255, uid);
	me->uid_ptr = me->uid;
}

void entity_del(entity_t* me, void* param)
{
	entity_clean(me, 0);
	free(me);
}




void entity_update_transformation(entity_t* me)
{
	//update bounding radious
	entity_update_bounding(me);

	//update transform matrix
	//m44r_set_rt(me->mat, me->t, me->r);
	m44r_set_RzxyT(me->mat, me->t, me->r);

	//NEW: render resource
	if (me->render_group)
	{
		render_group_update_model_matrix(me->render_group, me->render_group_instance_id, me->mat);
	}
}



void entity_update_bounding(entity_t* me)
{
	v4* aabb_center = me->aabb_center;
	float* aabb = me->aabb;
	float* aabb_local = me->aabb_local;
	v4* bounding_radius = me->bounding_radius;
	float* t = me->t;

	//set AABB
	aabb[0] = aabb_local[0] + t[0];
	aabb[1] = aabb_local[1] + t[0];

	aabb[2] = aabb_local[2] + t[1];
	aabb[3] = aabb_local[3] + t[1];

	aabb[4] = aabb_local[4] + t[2];
	aabb[5] = aabb_local[5] + t[2];


	//get center of entity's bouding radius
	aabb_center[0] = aabb[0] + bounding_radius[0];
	aabb_center[1] = aabb[2] + bounding_radius[1];
	aabb_center[2] = aabb[4] + bounding_radius[2];
	aabb_center[3] = 1.f;


	if (me->render_group)
	{
		render_group_update_aabb(me->render_group, me->render_group_instance_id, aabb_center, bounding_radius + 3);
	}
}

void entity_update(entity_t* me)
{
	//me->r[1] += .9f;
	//me->r[2] = 45.f;
	//if (me->r[1] > 360.f)
	//	me->r[1] -= 360.f;

	//me->t[2] = -2.f;

	//update transform matrix
	//m44_set_tr(me->mat, me->t, me->r);
	//m44_set_rt(me->mat, me->t, me->r);
}


// 100 4x4 joint transformation matrix
// quaternion rotation x translation
m44			mat_joint_transform[1600];


void entity_render(entity_t* me)
{
	float constant_color[4] = { 1.f, 1.f, 1.f, 1.f };
	m44 mat[16];
	m44 mat2[16];
	model_t* model;
	animation_t* anim;

	//shader
	//shader_model_t* shader_model = gYmge->shader_model;



	//model-view-projection matrix
	m44r_x_m44c_r(me->mat, ymge->camera->mat, mat);
	m44r_x_m44c_c(mat, ymge->mat_proj, mat2);

	//render models

	//model
	model = me->model;
	anim = me->animation;

	//glUseProgram(shader_animated_model->program);
	shader_animated_model_t* shader_animated_model = ymge->shader_animated_model;
	glUniformMatrix4fv(shader_animated_model->mat_transform, 1, GL_FALSE, mat2);										//model-view-projection matrix
	glUniformMatrix4fv(shader_animated_model->mat_model, 1, GL_FALSE, me->mat);											//model matrix
	glUniform3fv(shader_animated_model->light_normal, 1, ymge->light[0].dir);											//light direction
	//glUniformMatrix4fv(shader_animated_model->mat_joint_transform, model->num_joint, GL_FALSE, mat_joint_transform);	//joint transformation

																														//use texture
	glBindTexture(GL_TEXTURE_2D, me->texture->id);

	//render model
	model_render(model);

	
}





void entity_render_aabb(entity_t* me)
{
	m44 mat[16];
	m44 mat2[16];

	m44r_x_m44c_r(me->mat, ymge->camera->mat, mat);
	m44r_x_m44c_c(mat, ymge->mat_proj, mat2);

	//upload uniform trasform matrix
	glUniformMatrix4fv(ymge->shader_constant_color->mat_transform, 1, GL_FALSE, mat2);		//model shader

	/*
	{
		glBindVertexArray(me->vao_aabb);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->vbo_aabb[1]);
		glDrawElements(GL_LINES, 24, GL_UNSIGNED_SHORT, 0);
	}
	*/
}



int entity_compare(const entity_t* a, const entity_t* b, void* param)
{
	return strcmp(a->uid_ptr, b->uid_ptr);
}



void entity_change_animation(entity_t* me, int anim_slot)
{
	me->animation = me->model->animations + anim_slot;
	me->model_animation_time = 0.f;
	me->model_animation_keyframe = 0;

	v4* root_pos_curr = me->model_animation_root_pos_curr;
	root_pos_curr[0] = 0.f;
	root_pos_curr[1] = 0.f;
	root_pos_curr[2] = 0.f;
	root_pos_curr[3] = 1.f;
}




void entity_set_animation_sequence(entity_t* me, int anim_seq_slot, int anim_slot_start, int anim_slot_loop, int anim_slot_stop_0, int anim_slot_stop_1)
{
	//get anim seq
	animation_sequence_t* anim_seq = me->anim_seq + anim_seq_slot;

	//reset switch
	entity_set_animation_sequence_loop_stop_switch(me, anim_seq_slot, 0, -1, -1);
	entity_set_animation_sequence_loop_stop_switch(me, anim_seq_slot, 1, -1, -1);

	//update slot
	anim_seq->start.slot = anim_slot_start;
	anim_seq->loop.slot = anim_slot_loop;
	anim_seq->stop[0].slot = anim_slot_stop_0;
	anim_seq->stop[1].slot = anim_slot_stop_1;

	//update animation pointer
	animation_t* anims = me->model->animations;
	anim_seq->start.anim = anims + anim_slot_start;
	anim_seq->loop.anim = anims + anim_slot_loop;
	anim_seq->stop[0].anim = anims + anim_slot_stop_0;
	anim_seq->stop[1].anim = anims + anim_slot_stop_1;
}

//loop to stop switch
//pose at keyframe should be one keyframe before stop animation start keyframe 0
void entity_set_animation_sequence_loop_stop_switch(entity_t* me, int anim_seq_slot, int switch_slot, u16 keyframe, int stop_piece_slot)
{
	//get
	animation_sequence_t* anim_seq = me->anim_seq + anim_seq_slot;
	animation_sequence_switch_t* switcher = anim_seq->loop.switcher + switch_slot;

	switcher->keyframe = keyframe;
	switcher->piece_slot = stop_piece_slot;
}



void entity_play_animation_sequence(entity_t* me, int anim_seq_slot)
{
	//get anim seq
	animation_sequence_t* anim_seq = me->anim_seq + anim_seq_slot;

	//reset state
	anim_seq->is_looping = 0;
	anim_seq->is_stopping = 0;
	anim_seq->is_stopped = 0;

	//set to current
	me->anim_seq_slot_curr = anim_seq_slot;
	me->anim_seq_curr = anim_seq;

	//get anim slots
	int anim_slot_start = anim_seq->start.slot;
	int anim_slot_loop = anim_seq->loop.slot;

	//start animation
	if (anim_slot_start >= 0)
		entity_change_animation(me, anim_slot_start);
	else if (anim_slot_loop >= 0)
		entity_change_animation(me, anim_slot_loop);
}

void entity_stop_animation_sequence(entity_t* me)
{
	me->anim_seq_curr->is_stopping = 1;
}

int entity_get_current_animation_sequence_slot(entity_t* me)
{
	return me->anim_seq_slot_curr;
}

