#include <stdlib.h>
#include <string.h>

#include <file.h>
#include <buf.h>
#include <logger.h>

#include "animation.h"
#include "matrix.h"
#include "model.h"


void animation_init(animation_t* me)
{
	memset(me, 0, sizeof(animation_t));
}



void animation_clean(animation_t* me)
{
	if (me->time)
	{
		free(me->time);
		me->time = 0;
	}

	if (me->joint_rotation)
	{
		free(me->joint_rotation);
		me->joint_rotation = 0;
	}
}


static void loader_read(void* to, char** ptr, size_t size)
{
	memcpy(to, *ptr, size);
	*ptr += size;
}


void animation_load_from_file(animation_t* me, model_t* model, const char* path)
{
	int					i, j, k;
	u16					nFileType;
	size_t				nRead = 0;
	file_t				file = { 0 };
	buf_t*				file_content = 0;
	char*				p = 0;

	//read animation file
	if (file_open(&file, path, "rb"))
	{
		file_content = file_read_all(&file);
		file_close(&file);
	}
	else
	{
		LOG4("[animation] load animation file(%s) error.", path);
		return;
	}

	//parse start
	p = file_content->data;

	//file type
	//0: just model
	//1: plus joint weight
	//2: just animation
	loader_read(&nFileType, &p, sizeof(u16));


	//keyframes count
	loader_read(&me->num_keyframes, &p, sizeof(u16));

	//joints count
	loader_read(&me->num_joints, &p, sizeof(u16));

	//time array
	nRead = me->num_keyframes * sizeof(float);
	me->time = malloc(nRead);
	loader_read(me->time, &p, nRead);

	//joint rotation array
	nRead = me->num_joints * me->num_keyframes * 4 * sizeof(float);
	float* joint_rotation = malloc(nRead);
	me->joint_rotation = malloc(nRead);
	loader_read(joint_rotation, &p, nRead);


	//joint position array
	nRead = me->num_joints * me->num_keyframes * 3 * sizeof(float);
	float* joint_position = malloc(nRead);
	me->joint_position = malloc(me->num_joints * me->num_keyframes * 4 * sizeof(float));
	loader_read(joint_position, &p, nRead);



	//joint rotation array's bone name
	char* joint_rotation_bone = malloc(me->num_joints * 50);
	for (i = 0; i < me->num_joints; i++)
	{
		u8 nBoneLen;
		char* bone_name = joint_rotation_bone + 50 * i;
		loader_read(&nBoneLen, &p, 1);
		loader_read(bone_name, &p, nBoneLen);
		*(bone_name + nBoneLen) = 0;
	}

	//copy to animation joint rotation and position
	for (i = 0; i < model->num_joint; i++)
	{
		size_t size_block_4 = 4 * me->num_keyframes;
		size_t size_block_3 = 3 * me->num_keyframes;
		v4* to_rotation = me->joint_rotation + size_block_4 * i;
		v4* to_position = me->joint_position + size_block_4 * i;

		for (j = 0; j < me->num_joints; j++)
		{
			float* from_rotation = joint_rotation + size_block_4 * j;
			float* from_position = joint_position + size_block_3 * j;

			if (strcmp(joint_rotation_bone + 50 * j, model->joints[i].name) == 0)
			{
				memcpy(to_rotation, from_rotation, size_block_4 * sizeof(float));
				//memcpy(to_position, from_position, size_block_3 * sizeof(float));
				for (k = 0; k < me->num_keyframes; k++)
				{
					to_position[4 * k + 0] = from_position[3 * k + 0];
					to_position[4 * k + 1] = from_position[3 * k + 1];
					to_position[4 * k + 2] = from_position[3 * k + 2];
					to_position[4 * k + 3] = 1.f;
				}
			}
		}
	}

	//clean rotation related temp mem
	free(joint_rotation);
	free(joint_position);
	free(joint_rotation_bone);

	//flip y z
	if(0)
	for (int i = 0; i < nRead; i+=4)
	{
		v4* q = me->joint_rotation + i;
		float y = q[2];
		float z = q[3];
		q[2] = z;
		q[3] = y;
	}

	//clean
	free(file_content);
}


//write to joint_transformation
void animation_update_joint_transformation(animation_t* me, joint_t* joints, u8 joint_id, u16 curr_keyframe, float t, m44* parent_rt_transform, m44* joint_transformation)
{
}

