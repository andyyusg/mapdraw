#pragma once

#include "ymge.h"

void cull_quarter(ymge_t* ymge);
void cull_all();
void cull_animated();