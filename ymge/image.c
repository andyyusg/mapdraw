#include <stdlib.h>
#include <png.h>
#include <GL/glew.h>
#include <logger.h>
#include "image.h"



void image_load(image_t* me, const char* path)
{
	png_byte		header[8];
	png_structp		png = 0;
	png_infop		png_info = 0;
	png_infop		png_end_info = 0;

	FILE*			file = 0;
	size_t			read_size = 0;
	png_size_t		rowbytes = 0;
	size_t			size = 0;
	png_bytepp		rows = 0;
	u32				i = 0;
	int				error = 0;


	//init
	me->error = 0;

	LOG0("[ymge] texture load: %s", path);

	//open file
	fopen_s(&file, path, "rb");
	if (file == 0)
	{
		me->error = YMGE_ERR_IMAGE_LOAD_OPEN_FILE;
		return;
	}

	//read header
	fread(header, 8, 1, file);

	//check header
	error = png_sig_cmp(header, 0, 8);
	if (error)
	{
		me->error = YMGE_ERR_IMAGE_LOAD_PNG_SIG_CHECK;
		fclose(file);
		return;
	}

	//create read struct
	png = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
	if (!png)
	{
		me->error = YMGE_ERR_IMAGE_LOAD;
		fclose(file);
		return;
	}

	//create info struct
	png_info = png_create_info_struct(png);
	if (!png_info)
	{
		me->error = YMGE_ERR_IMAGE_LOAD;
		png_destroy_read_struct(&png, 0, 0);
		fclose(file);
		return;
	}

	png_end_info = png_create_info_struct(png);
	if (!png_end_info)
	{
		me->error = YMGE_ERR_IMAGE_LOAD;
		png_destroy_read_struct(&png, &png_info, 0);
		fclose(file);
		return;
	}

	//if error, below code is called
	if (setjmp(png_jmpbuf(png)))
	{
		me->error = YMGE_ERR_IMAGE_LOAD;
		png_destroy_read_struct(&png, &png_info, &png_end_info);
		fclose(file);
		return;
	}

	//init png
	png_init_io(png, file);
	png_set_sig_bytes(png, 8);

	//read png info upto image data
	png_read_info(png, png_info);

	//get png info
	png_get_IHDR(png, png_info, &me->w, &me->h, &me->bit_depth, &me->color_type, 0, 0, 0);
	LOG0("[ymge] texture load: width %u, height %u, bit depth %d", me->w, me->h, me->bit_depth);
	switch (me->color_type)
	{
	case PNG_COLOR_TYPE_RGB:
		me->format = GL_RGB;
		LOG0("[ymge] texture load: color type: RGB");
		break;
	case PNG_COLOR_TYPE_RGBA:
		me->format = GL_RGBA;
		LOG0("[ymge] texture load: color type: RGBA");
		break;
	default:
		LOG4("[ymge] texture load: invalid color type: %d", me->color_type);
		me->error = YMGE_ERR_IMAGE_LOAD_PNG_COLOR_FORMAT;
		png_destroy_read_struct(&png, &png_info, &png_end_info);
		fclose(file);
		return;
	}

	//read row bytes
	//png_read_update_info(png, png_info);
	rowbytes = png_get_rowbytes(png, png_info);

	//buf new - image data
	me->buf_img = buf_new(rowbytes * me->h);
	if (!me->buf_img)
	{
		me->error = YMGE_ERR_IMAGE_LOAD;
		buf_del(me->buf_img);
		me->buf_img = 0;
		png_destroy_read_struct(&png, &png_info, &png_end_info);
		fclose(file);
		return;
	}

	//prepare row pointers - for png read
	size = me->h * sizeof(png_byte*);
	rows = (png_bytepp)malloc(size);
	for (i = 0; i < me->h; i++)
	{
		rows[me->h - 1 - i] = (png_bytep)me->buf_img->data + i*rowbytes;
	}

	//png read
	png_read_image(png, rows);

	//clean
	free(rows);
	png_destroy_read_struct(&png, &png_info, &png_end_info);
	fclose(file);
}



void image_unload(image_t* me)
{
	buf_del(me->buf_img);
}