#pragma once

#include "ymge_type.h"


typedef struct
{
	uint			program;
}
shader_text_frame_t;



//function
void shader_text_frame_init(shader_text_frame_t* me, const char* vertex_shader_path, const char* fragment_shader_path);
void shader_text_frame_clean(shader_text_frame_t* me);