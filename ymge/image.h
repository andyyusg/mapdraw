#pragma once

#include <buf.h>
#include "ymge_type.h"

#define YMGE_ERR_IMAGE_LOAD_OPEN_FILE			1
#define YMGE_ERR_IMAGE_LOAD_PNG_SIG_CHECK		2
#define YMGE_ERR_IMAGE_LOAD						3
#define YMGE_ERR_IMAGE_LOAD_PNG_COLOR_FORMAT	4


typedef struct image_s
{
	//image info
	u32			w;
	u32			h;

	int			bit_depth;
	int			color_type;			//2:	RGB			6:		RGBA
	int			format;				//6407:	GL_RGB		6408:	GL_RGBA


	//image data
	buf_t*			buf_img;


	//error
	int				error;
}
image_t;


// Usage:
// 1. call image_load()
// 2. use image info and data to load OpenGL
// 3. call image_unload() to cleanup buf_img


//function
void image_load(image_t* me, const char* path);
void image_unload(image_t* me);
