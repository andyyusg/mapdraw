#pragma once

#include "joint.h"
#include "animation.h"
#include "texture.h"
#include "render_res.h"

//VAO - size of VBO slots
#define YMGE_MODEL_VBO_COUNT				5			//vertex: position, normal, uv, joint id, joint weight



//vertex data list
typedef struct vertex_data_s
{
	struct vertex_data_s*		next;
	float						data[3];
}
vertex_data_t;



//VBO - Vertex Buffer
typedef struct
{
	uint				id;

	int					unit_size;
	int					unit_type;
}
vbo_t;



typedef struct model_s
{
	char				uid[100];				//unique id
	char				path[255];				//path to model

	//uid pointer - for comparison
	const char*			uid_ptr;

	//0: static
	//1: skeleton animatable
	int					type;					

	uint				vao;						//vertex array object
	vbo_t				vbo[YMGE_MODEL_VBO_COUNT];	//vertex buffer object
	uint				vio;						//vertex index

	uint				num_vbo;					//3 for static model, 5 for animated model

	uint				num_index;					//vertex index count

	//joint
	u16					num_joint;
	joint_t*			joints;
	float*				joint_transform;		// pose inverse matrix multiplied by keyframe's transformation matrix
	u16					joint_root_id;

	//animation
	u16					num_animation;
	animation_t*		animations;

	//bounding info
	float				aabb[6];				//axis aligned bounding box: Xmin, Xmax, Ymin, Ymax, Zmin, Zmax

	//NEW: render resource
	render_indirect_params_t	render_indirect_param;
	render_res_t*				render_res;
}
model_t;



model_t* model_new(const char* id);
void model_del(model_t* me, void* param);

//function
void model_load(model_t* me, uint num_vertices, const float* vertices, const float* uvs, const float* normals, const short* joints, const float* weights, uint num_indices, const u16* indices);
void model_load_from_file(model_t* me, const char* model_path, render_res_t* render_res);
void model_unload(model_t* me);
void model_render(model_t* me);
void model_init_animation(model_t* me, u16 num_animation);
void model_update_animation(model_t* me, animation_t* anim, m44* mat_joint_transform, u16 curr_keyframe, float t, v4* root_pos);


//compare function for avl
int model_compare(const model_t* a, const model_t* b, void* param);

void model_compare_anim(model_t* me, model_t* m2);



//temp
//debug purpose only
void model_compare_file(const char* model_path, const char* model_path2);
