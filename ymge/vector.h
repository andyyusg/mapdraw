#pragma once

#include <intrin.h>
#include <math.h>

#include "tri.h"


//set v[3] = 0 before normalize
inline void v4_normalize(float* v, float* n)
{
	v4 b[4];
	__m128 a = _mm_load_ps(v);
	
	//multiply
	_mm_store_ps(b, _mm_hadd_ps(_mm_mul_ps(a, a), _mm_setzero_ps()));

	//distance
	float d = sqrtf(b[0] + b[1]);
	
	//divide components
	_mm_store_ps(n, _mm_div_ps(a, _mm_set1_ps(d)));
}


inline void v3_cross_v3(float* a, float* b, float* r)
{
	/*
	v4		a1[4];
	v4		b1[4];
	v4		a2[4];
	v4		b2[4];

	//batch 1
	a1[0] = a1[1] = a[0];
	a1[2] = a1[3] = a[1];

	b1[0] = b[1];
	b1[1] = b[2];
	b1[2] = b[0];
	b1[3] = b[2];


	//batch 2
	a2[0] = a2[1] = a[2];
	a2[2] = a2[3] = 0.f;

	b2[0] = b[0];
	b2[1] = b[1];
	b2[2] = 0.f;
	b2[3] = 0.f;

	__m128 A = _mm_load_ps(a1);
	__m128 B = _mm_load_ps(b1);

	__m128 R = _mm_mul_ps(A, B);
	*/


	// Vector Cross Product Formula
	r[0] = a[1] * b[2] - a[2] * b[1];
	r[1] = a[2] * b[0] - a[0] * b[2];
	r[2] = a[0] * b[1] - a[1] * b[0];
}


//get 2D vector radian
inline float v2_radian(float* a)
{
	float r;

	r = atanf(a[1] / a[0]);

	//atan result range -pi to pi
	if (a[0] < 0)
		return r + YMGE_TRI_PI;
	else
		return r;
}


inline float v2_v2_radian(float* a, float* b)
{
	float dot = a[0] * b[0] + a[1] * b[1];
	float ab = sqrtf(a[0] * a[0] + a[1] * a[1]) * sqrtf(b[0] * b[0] + b[1] * b[1]);
	return acosf(dot / ab);
}


inline float v2_cross_v2(float* a, float* b)
{
	return a[0] * b[1] - a[1] * b[0];
}


inline float v2_dot_v2(float* a, float* b)
{
	return a[0] * b[0] + a[1] * b[1];
}

