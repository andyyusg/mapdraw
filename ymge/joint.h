#pragma once

#include "ymge_type.h"


typedef struct
{
	char		name[50];
	float		location_relative[3];		//location relative to parent, read from model file
	float		location[3];				//location absolute from origin
	float		location_inverse[3];		//location inverse from origin
	
	//child
	u8			num_child;
	u8			child_id[30];

	m44			inverse_transform[16];
}joint_t;



//function
void joint_calculate_location(joint_t* me, joint_t* joints, float* location_parent);