#pragma once

#include "ymge_type.h"
#include "matrix.h"

typedef struct camera_s
{
	//matrix (transposed)
	m44								mat[16];

	//transformation (reversed) - for easy matrix calculation
	v4								tr[4];			//keep t[3] 0, for camera matrix calculation
	float							rr[3];

	//transformation
	v4								t[4];			//keep t[3] 0, for camera matrix calculation
	float							r[3];

	//direction normal vector
	v4								n[4];


	//mouse info
	int								mouse_pos[2];
	float							mouse_pos_n[2];
	v4								mouse_pos_eye[4];
	v4								mouse_ray[4];

	//obsollete
	float							mouse_angle[2];
}
camera_t;


extern v4 camera_r0[4];


//update matrix
void camera_update_position(camera_t* me, float* t);
void camera_update_direction(camera_t* me, float* r);



inline void camera_update_transformation(camera_t* me)
{
	//for model, reversed
	m44c_set_tr(me->mat, me->tr, me->rr);

	//for light: rotation only
	//m44_set_r_transposed(me->mat_light, me->r);
}


inline void camera_update_mouse_ray_old(camera_t* me)
{
	float								r[3];									//mouse ray rotation
	m44									m[16];									//rotation matrix

	//get mouse ray rotation
	r[0] = me->r[0] + me->mouse_angle[0];
	r[1] = me->r[1] + me->mouse_angle[1];
	r[2] = me->r[2];

	//adjust r, make it less than 360.f
	r[0] -= r[0] > 360.f ? 360.f : 0.f;
	r[1] -= r[1] > 360.f ? 360.f : 0.f;

	//transform with matrix, get final normal vector
	m44c_set_r(m, r);
	v4_x_m44c(camera_r0, m, me->mouse_ray);
}



void camera_init(camera_t* me);