#include "camera.h"
#include "matrix.h"
#include "ymge.h"
#include "control.h"

#define YMGE_CAMERA_MOVE_SPEED			0.1f
#define YMGE_CAMERA_ROTATE_SPEED		1.f

//default rotation - facing Z-
v4 camera_r0[4] = { 0.f, 0.f, -1.f, 0.f };

void camera_init(camera_t* me)
{
	me->mouse_pos_n[0] = 0.5f;
	me->mouse_pos_n[1] = 0.5f;
}



void camera_update_position(camera_t* me, float* t)
{
	//update rotation
	me->t[0] = t[0];
	me->t[1] = t[1];
	me->t[2] = t[2];
	me->tr[0] = -t[0];
	me->tr[1] = -t[1];
	me->tr[2] = -t[2];
}


void camera_update_direction(camera_t* me, float* r)
{
	//update rotation
	me->r[0] = r[0];
	me->r[1] = r[1];
	me->r[2] = r[2];
	me->rr[0] = -r[0];
	me->rr[1] = -r[1];
	me->rr[2] = -r[2];
	
	//update mouse ray normal vector
	//ray_eye_to_world(me->mouse_pos_eye, me->r, me->mouse_ray);
}




// IN, camera rotation, 3
// IN, mouse rotate angel on screen, 2
// OUT, mouse ray normal vector, 4
// DEBUG version
void camera_update_mouse_ray_old2(float* camera_r, float* mouse_r, float* out_mouse_ray)
{
	float		r[3];									//mouse ray rotation
	m44			m[16];									//rotation matrix
	static v4	n0[4] = { 0.f, 0.f, -1.f, 0.f };		//mouse ray initial normal vector, facing Z-

	//get mouse ray rotation
	r[0] = camera_r[0] + mouse_r[0];
	r[1] = camera_r[1] + mouse_r[1];
	r[2] = camera_r[2];

	//transform with matrix, get final normal vector
	m44c_set_r(m, r);
	v4_x_m44c(n0, m, out_mouse_ray);
}





