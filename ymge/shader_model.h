#pragma once

#include "ymge_type.h"


typedef struct
{
	uint			program;
	
	//uniform
	int				light_normal;
}
shader_model_t;



//function
void shader_model_init(shader_model_t* me, const char* vertex_shader_path, const char* fragment_shader_path);
void shader_model_clean(shader_model_t* me);