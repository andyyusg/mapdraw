#pragma once

#include "ymge_type.h"

typedef struct
{
	//data
	v4			pos[4];

	//render objects
	uint		vbo[2];
	uint		ibo;
	uint		vao;
}
poi_t;


typedef struct
{
	poi_t		pois[1000];
	int			num_pois;

	//render object
	uint		texture;
}
pois_t;


void pois_init(void);
void pois_clean(void);

void pois_render(m44* mat_vp);
