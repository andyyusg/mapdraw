#include <GL/glew.h>

#include "shader_animated_model.h"
#include "shader.h"

void shader_animated_model_init(shader_animated_model_t* me, const char* vertex_shader_path, const char* fragment_shader_path)
{
	shader_source_t		shaders[] = {
		{GL_VERTEX_SHADER, vertex_shader_path},
		{GL_FRAGMENT_SHADER, fragment_shader_path}
	};

	//build shader
	me->program = shader_build(2, shaders);
	if (!me->program) return;

	//get uniform locations
	me->mat_transform = glGetUniformLocation(me->program, "mat_transform");
	me->mat_model = glGetUniformLocation(me->program, "mat_model");
	me->light_normal = glGetUniformLocation(me->program, "light_normal");
	me->mat_joint_transform = glGetUniformLocation(me->program, "mat_joint_transform");
}


void shader_animated_model_clean(shader_animated_model_t* me)
{
	glDeleteProgram(me->program);
}

