#pragma once

#include "ymge_type.h"
#include "font.h"




typedef struct
{
	int			tw;			//texture width
	int			th;			//texture height

	int			cur_x;		//cursor x, texture writing pointer
	int			cur_y;

	uint		texture;	//texture id
	uint		vbo[2];		//vertex buffer object
	uint		ibo;		//index buffer object
	uint		vao;		//vertex array object

	int			error;
}
ui_text_t;


typedef struct ymge_s ymge_t;


//function
void ui_text_init(ui_text_t* me, int x, int y, int w, int h);
void ui_text_clean(ui_text_t* me);

void ui_text_draw(ui_text_t* me);

void ui_text_clear(ui_text_t* me);

void ui_text_set_string(ui_text_t* me, font_t* font, char* str);
void ui_text_add_char(ui_text_t* me, font_t* font, char ch);
void ui_text_add_string(ui_text_t* me, font_t* font, char* str);

void ui_text_set_wstring(ui_text_t* me, font_t* font, uint char_height, wchar_t* str);
void ui_text_add_wchar(ui_text_t* me, font_t* font, uint char_height, u16 ch);
void ui_text_add_wstring(ui_text_t* me, font_t* font, uint char_height, u16* str);

