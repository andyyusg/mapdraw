#include <GL/glew.h>

#include "ymge.h"
#include "ui_wnd_group.h"





void ui_wnd_group_set_texture(ui_wnd_group_t* me, texture_t* texture)
{
	me->texture = texture;
}


void ui_wnd_group_clean(ui_wnd_group_t* me)
{
	if (me->vertices)
		free(me->vertices);

	if (me->uvs)
		free(me->uvs);

	if (me->root)
	{
		ui_wnd_clean(me->root);
	}

	glDeleteBuffers(2, me->vbo);
	glDeleteBuffers(1, &me->ibo);
	glDeleteVertexArrays(1, &me->vao);
}

static void ui_wnd_group_prepare_wnd(ui_wnd_group_t* me, ui_wnd_t* wnd, float parent_nw, float parent_nh, float parent_ndx, float parent_ndy)
{
	u32 i;
	int corner_btm_w, corner_top_w;
	float nw, nh;		//normalized w, h   -1 ~ 1
	float nw_inner_top, nh_inner_top;
	float nw_inner_btm, nh_inner_btm;

	float nx, ny;
	float ndx, ndy;		//delta x, y

	float f = 0.f;		//temp

	//template
	ui_tpl_wnd_t* tpl = wnd->tpl;

	int corner_w = tpl->x[1] - tpl->x[0];
	int corner_btm_h = tpl->y[1] - tpl->y[0];
	int corner_top_h = tpl->y[3] - tpl->y[2];
	//corner top and btm have same width
	corner_btm_w = corner_top_w = corner_w;

	float window_width = (float)ymge->window_width;
	float window_height = (float)ymge->window_height;

	int pos_style = wnd->pos_style;
	int x = wnd->x;
	int y = wnd->y;
	int w = wnd->w;
	int h = wnd->h;
	

	nw = (float)w / window_width;		//normalized width, actually half width, so window box is between X  -nw ~ nw
	nh = (float)h / window_height;		//normalized height, ditto
	nw_inner_top = (float)(w - 2 * corner_top_w) / window_width;
	nh_inner_top = (float)(h - 2 * corner_top_h) / window_height;
	nw_inner_btm = (float)(w - 2 * corner_btm_w) / window_width;
	nh_inner_btm = (float)(h - 2 * corner_btm_h) / window_height;


	//nx = -(1.f - (float)x / (window_width / 2.f));
	//ny = (1.f - (float)y / (window_height / 2.f));
	nx = (float)x / (window_width / 2.f);
	ny = (float)y / (window_height / 2.f);

	//reset x, y delta position
	ndx = parent_ndx + nx;
	ndy = parent_ndy + ny;

	//adjust x, y delta accordingly
	if (pos_style & YMGE_UI_WINDOW_POS_X_RIGHT)
	{
		ndx += parent_nw;
	}
	else if (pos_style & YMGE_UI_WINDOW_POS_X_LEFT)
	{
		ndx -= parent_nw;
	}

	if (pos_style & YMGE_UI_WINDOW_POS_Y_BOTTOM)
	{
		ndy -= parent_nh;
	}
	else if (pos_style & YMGE_UI_WINDOW_POS_Y_TOP)
	{
		ndy += parent_nh;
	}


	//creae OpenGL buffer object


	//pointers
	wnd->pos_vertices = me->pos_vertices;
	wnd->pos_uvs = me->pos_uvs;
	wnd->pos_indices = me->pos_indices;

	float* vertices = me->vertices + wnd->pos_vertices;
	float* uvs = me->uvs + wnd->pos_uvs;
	u16* indices = me->indices + wnd->pos_indices;


	///////////////////////
	// vertices

	//vertex x: left outter
	vertices[0] =
		vertices[3 * 4] =
		vertices[3 * 8] =
		vertices[3 * 12] = -nw + ndx;

	//vertex x: left inner
	vertices[3 * 1] = -nw_inner_top + ndx;
	vertices[3 * 5] = -nw_inner_top + ndx;
	vertices[3 * 9] = -nw_inner_btm + ndx;
	vertices[3 * 13] = -nw_inner_btm + ndx;

	//vertex x: right inner
	vertices[3 * 2] = nw_inner_top + ndx;
	vertices[3 * 6] = nw_inner_top + ndx;
	vertices[3 * 10] = nw_inner_btm + ndx;
	vertices[3 * 14] = nw_inner_btm + ndx;

	//vertex x: right outter
	vertices[3 * 3] =
		vertices[3 * 7] =
		vertices[3 * 11] =
		vertices[3 * 15] = nw + ndx;

	//vertex y: top outter
	vertices[3 * 0 + 1] =
		vertices[3 * 1 + 1] =
		vertices[3 * 2 + 1] =
		vertices[3 * 3 + 1] = nh + ndy;

	//vertex y: top inner
	f = nh_inner_top + ndy;
	vertices[3 * 4 + 1] =
		vertices[3 * 5 + 1] =
		vertices[3 * 6 + 1] =
		vertices[3 * 7 + 1] = f;

	//vertex y: bot inner
	f = -nh_inner_btm + ndy;
	vertices[3 * 8 + 1] =
		vertices[3 * 9 + 1] =
		vertices[3 * 10 + 1] =
		vertices[3 * 11 + 1] = f;

	//vertex y: top outter
	vertices[3 * 12 + 1] =
		vertices[3 * 13 + 1] =
		vertices[3 * 14 + 1] =
		vertices[3 * 15 + 1] = -nh + ndy;

	///////////////////////
	// uvs

	//top row - top edge
	uvs[0] = tpl->u[0];		uvs[1] = tpl->v[3];
	uvs[2] = tpl->u[1];		uvs[3] = tpl->v[3];
	uvs[4] = tpl->u[2];		uvs[5] = tpl->v[3];
	uvs[6] = tpl->u[3];		uvs[7] = tpl->v[3];

	//2nd row
	uvs[8] = tpl->u[0];		uvs[9] = tpl->v[2];
	uvs[10] = tpl->u[1];	uvs[11] = tpl->v[2];
	uvs[12] = tpl->u[2];	uvs[13] = tpl->v[2];
	uvs[14] = tpl->u[3];	uvs[15] = tpl->v[2];

	//3rd row
	uvs[16] = tpl->u[0];	uvs[17] = tpl->v[1];
	uvs[18] = tpl->u[1];	uvs[19] = tpl->v[1];
	uvs[20] = tpl->u[2];	uvs[21] = tpl->v[1];
	uvs[22] = tpl->u[3];	uvs[23] = tpl->v[1];

	//bottom row - bottom edge
	uvs[24] = tpl->u[0];	uvs[25] = tpl->v[0];
	uvs[26] = tpl->u[1];	uvs[27] = tpl->v[0];
	uvs[28] = tpl->u[2];	uvs[29] = tpl->v[0];
	uvs[30] = tpl->u[3];	uvs[31] = tpl->v[0];



	///////////////////////
	// indices
	u16 indices2[54] = {
		0, 4, 1,
		1, 4, 5,

		1, 5, 2,
		2, 5, 6,

		2, 6, 3,
		3, 6, 7,

		4, 8, 5,
		5, 8, 9,

		5, 9, 6,
		6, 9, 10,

		6, 10, 7,
		7, 10, 11,

		8, 12, 9,
		9, 12, 13,

		9, 13, 10,
		10, 13, 14,

		10, 14, 11,
		11, 14, 15,
	};
	//adjust index
	for (i = 0; i < 54; i++)
	{
		indices2[i] += me->next_index;
	}
	memcpy(indices, indices2, 54 * sizeof(u16));

	//increase pos
	me->next_index += 16;
	me->pos_vertices += 16 * 3;
	me->pos_uvs += 16 * 2;
	me->pos_indices += 54;



	//loop children windows
	ui_wnd_t* child = 0;
	for (i = 0; i < wnd->num_wnds; i++)
	{
		child = wnd->wnds + i;
		if (child->initialized)
		{
			ui_wnd_group_prepare_wnd(me, child, nw, nh, ndx, ndy);
		}
	}
}


void ui_wnd_group_prepare(ui_wnd_group_t* me)
{
	int num_total_wnds = 0;

	//initialized
	me->initialized = 1;

	//count total windows count
	ui_wnd_count_wnds(me->root, &num_total_wnds);

	//prepare buffer
	me->num_vertices = 16 * 3 * num_total_wnds;
	me->num_uvs = 16 * 2 * num_total_wnds;
	me->num_indices = 18 * 3 * num_total_wnds;		// 3x3=9 squares, 18 triangles, x3=54 indices

	me->vertices = calloc(me->num_vertices, sizeof(float));
	me->uvs = calloc(me->num_uvs, sizeof(float));
	me->indices = calloc(me->num_indices, sizeof(u16));


	//prepare each window's vertex buffer
	ui_wnd_group_prepare_wnd(me, me->root, 1, 1, 0.f, 0.f);





	//vbo
	glGenBuffers(2, me->vbo);
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, me->num_vertices * sizeof(float), me->vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, me->num_uvs * sizeof(float), me->uvs, GL_STATIC_DRAW);

	//ibo
	glGenBuffers(1, &me->ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, me->num_indices * sizeof(u16), me->indices, GL_STATIC_DRAW);

	//vao
	glGenVertexArrays(1, &me->vao);
	glBindVertexArray(me->vao);

	//link vbo to vao
	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[0]);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, me->vbo[1]);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);
}




void ui_wnd_group_render(ui_wnd_group_t* me, int uniform_transform_matrix)
{
	float mat[16] = M44_IDENTITY;

	//check active
	if (!me->is_active)
		return;

	//set transform matrix
	glUniformMatrix4fv(uniform_transform_matrix, 1, GL_FALSE, mat);

	//choose texture
	//glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, me->texture->id);

	glBindVertexArray(me->vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, me->ibo);
	glDrawElements(GL_TRIANGLES, me->num_indices, GL_UNSIGNED_SHORT, 0);
}

