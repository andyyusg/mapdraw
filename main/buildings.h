#pragma once



typedef struct
{
	char		name[50];
	int			index;
	int			num_nodes;
}
building_t;


typedef struct
{
	int			num_buildings;
	building_t	buildings[10];

	int			num_nodes;
	float		nodes[100][2];
}
buildings_t;


void buildings_init();
void buildings_clean();
