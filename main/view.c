#include "view.h"

//singleton
static view_t sView;


void view_init()
{
	camera_t* cam = ymge->camera;

	sView.rotate_speed = 0.05f;
	sView.move_speed = 1.f;
	sView.is_2d = FALSE;

	//camera init
	sView.zoom = 60.f;

	sView.t[0] = 0.f;
	sView.t[1] = 0.f;
	sView.t[2] = 0.f;

	sView.r[0] = 315.f;
	sView.r[1] = 0.f;
	sView.r[2] = 0.f;

	camera_update_position(cam, sView.t);
	camera_update_direction(cam, sView.r);
	camera_update_transformation(ymge->camera);
}

void view_toggle_2d()
{
	sView.is_2d = !sView.is_2d;
}

float* view_get_pos()
{
	return sView.t;
}

void view_update(void)
{
	u32 control = ymge->control;
	camera_t* camera = ymge->camera;
	float sin, cos;
	float move_speed = sView.move_speed;


	//moving state
	int is_panning_vertical = 0;
	int is_panning_horizontal = 0;
	int is_zooming = 0;


	//rotation horizontal - Y axis
	if (ymge->mouse_rel[0])
	{
		sView.r[1] -= sView.rotate_speed * ((float)ymge->mouse_rel[0]);
		if (sView.r[1] > 360.f)
			sView.r[1] -= 360.f;
		else if (sView.r[1] < 0.f)
			sView.r[1] += 360.f;
	}

	//rotation vertical - X axis
	if (ymge->mouse_rel[1])
	{
		sView.r[0] += sView.rotate_speed * ((float)ymge->mouse_rel[1]);
		if (sView.r[0] > 350.f)
			sView.r[0] = 350.f;
		else if (sView.r[0] < 270.f)
			sView.r[0] = 270.f;
	}

	//control
	//pan forward/backward
	if (control & YMGE_CTRL_MOVE_FORWARD)
	{
		is_panning_vertical = 1;
	}
	else if (control & YMGE_CTRL_MOVE_BACKWARD)
	{
		is_panning_vertical = -1;
	}
	//pan left/right
	if (control & YMGE_CTRL_MOVE_LEFT)
	{
		is_panning_horizontal = 1;
	}
	else if (control & YMGE_CTRL_MOVE_RIGHT)
	{
		is_panning_horizontal = -1;
	}
	//zoom
	if (control & YMGE_CTRL_MOVE_UP)
	{
		is_zooming = 1;
	}
	else if (control & YMGE_CTRL_MOVE_DOWN)
	{
		is_zooming = -1;
	}

	//position
	//panning vertical
	if (is_panning_vertical)
	{
		tri_sin_cos(sView.r[1], &sin, &cos);

		sView.t[0] -= is_panning_vertical * move_speed * sin;
		sView.t[2] -= is_panning_vertical * move_speed * cos;
	}
	//panning horizontal
	if (is_panning_horizontal)
	{
		float r = sView.r[1] + 90.f;
		if (r > 360.f)
			r -= 360.f;
		tri_sin_cos(r, &sin, &cos);

		sView.t[0] -= is_panning_horizontal * move_speed * sin;
		sView.t[2] -= is_panning_horizontal * move_speed * cos;
	}
	//zooming
	if (is_zooming)
	{
		sView.zoom += is_zooming * move_speed;

		//limit zoom
		if (sView.zoom < 10.f)
			sView.zoom = 10.f;
	}

	//2D mode
	if (sView.is_2d)
	{
		sView.r[0] = 270.f;
		sView.r[1] = 0.f;
	}

	//if (is_panning_vertical || is_panning_horizontal || is_zooming)
	{
		float t[3];
		float siny, cosy, sinx, cosx;

		tri_sin_cos(camera->r[0], &sinx, &cosx);
		tri_sin_cos(camera->r[1], &siny, &cosy);

		t[0] = sView.t[0] + sView.zoom * cosx * siny;
		t[1] = sView.t[1] - sView.zoom * sinx;
		t[2] = sView.t[2] + sView.zoom * cosx * cosy;

		camera_update_direction(camera, sView.r);
		camera_update_position(camera, t);
	}

	camera_update_transformation(ymge->camera);
}

