#pragma once


typedef struct
{
	char			name[50];
	int				index;
	int				num_nodes;
}
road_t;


typedef struct
{
	//roads
	road_t			roads[800];
	int				num_roads;

	//road nodes
	float			nodes[8000][2];
	int				num_nodes;

	//road connections
	int				conns[3000][2];
	int				num_conns;

	//display settings
	//height:
	float			y_kerb;
	float			y_road;
	//width:
	float			road_width;
	float			kerb_width;
}
roads_t;


void roads_init();
void roads_clean();

