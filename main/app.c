﻿#include <ymge.h>

#include "app.h"
#include "view.h"
#include "gui.h"
#include "roads.h"
#include "paths.h"
#include "pois.h"
#include "features.h"
#include "buildings.h"


void app_update(void)
{
	//double elapsed = ymge->time->elapsed;

	//update debug string
	char str[1024];

	float* pos = view_get_pos();

	sprintf_s(str, 1024,
		"Frame update time: %f\n"
		"Frame refresh time: %f\n"
		"MapDraw pos: %f, %f\n"
		"Camera pos: %f, %f, %f\n"
		"Camera X rotation: %f\n"
		"Camera Y rotation: %f\n"
		"Camera dir vector: %f, %f, %f\n"
		"Mouse: %d, %d\n"
		"Mouse(n): %f, %f\n"
		"Mouse(eye): %f, %f, %f, %f\n"
		"Light(eye): %f, %f, %f, %f\n",
		ymge->timer[0].elapsed,
		ymge->timer[1].elapsed,
		pos[0],
		-pos[2],
		ymge->camera->t[0],
		ymge->camera->t[1],
		ymge->camera->t[2],
		ymge->camera->r[0],
		ymge->camera->r[1],
		ymge->camera->n[0],
		ymge->camera->n[1],
		ymge->camera->n[2],
		ymge->camera->mouse_pos[0],
		ymge->camera->mouse_pos[1],
		ymge->camera->mouse_pos_n[0],
		ymge->camera->mouse_pos_n[1],
		ymge->camera->mouse_pos_eye[0],
		ymge->camera->mouse_pos_eye[1],
		ymge->camera->mouse_pos_eye[2],
		ymge->camera->mouse_pos_eye[3],
		ymge->light[0].dir[0],
		ymge->light[0].dir[1],
		ymge->light[0].dir[2],
		ymge->light[0].dir[3]
	);

	timer_start(ymge->timer + 1);
	ui_text_set_string(ymge->ui_text, ymge->font, str);
	timer_stop(ymge->timer + 1);

	//update view(camera)
	view_update();
}

int app_event(SDL_Event* event)
{
	switch (event->type)
	{
	case SDL_MOUSEMOTION:
	{
		ymge->mouse_rel[0] += event->motion.xrel;
		ymge->mouse_rel[1] += event->motion.yrel;

		//update mouse ray normal vector
		ymge_update_mouse_ray(ymge);

		break;
	}

	case SDL_MOUSEBUTTONDOWN:
		break;

	case SDL_KEYDOWN:
		switch (event->key.keysym.sym)
		{
		case SDLK_ESCAPE:
			return 0;

		case SDLK_v:
			view_toggle_2d();
			break;

		case SDLK_p:
			ymge->control ^= YMGE_CTRL_WIRE_RENDER;		//toggle wire render
			break;

		case SDLK_SPACE:
			ymge->ui->wnd_groups[0].is_active = !ymge->ui->wnd_groups[0].is_active;
			break;

		case SDLK_LEFT:
			ymge->control |= YMGE_CTRL_ROLL_LEFT;
			break;

		case SDLK_RIGHT:
			ymge->control |= YMGE_CTRL_ROLL_RIGHT;
			break;

		case SDLK_y:
		{
			entity_t* entity = ymge_get_entity("tank");
			if (!entity)
			{
				entity = render_group_get_entity(ymge->render_group, "tank");
			}
			if (entity)
			{
				entity->r[0] += 45.f;
				if (entity->r[0] > 360.f)
					entity->r[0] -= 360.f;
				entity_update_transformation(entity);
			}
			break;
		}

		case SDLK_m:
			//ymge_enable_mouse_exclusive(1);
			break;

		case SDLK_n:
			//ymge_enable_mouse_exclusive(0);
			break;

		case SDLK_w:
			ymge->control |= YMGE_CTRL_MOVE_FORWARD;
			break;

		case SDLK_s:
			ymge->control |= YMGE_CTRL_MOVE_BACKWARD;
			break;

		case SDLK_a:
			ymge->control |= YMGE_CTRL_MOVE_LEFT;
			break;

		case SDLK_d:
			ymge->control |= YMGE_CTRL_MOVE_RIGHT;
			break;

		case SDLK_LSHIFT:
			ymge->control |= YMGE_CTRL_RUN;
			break;

		case SDLK_KP_7:
		case SDLK_q:
			ymge->control |= YMGE_CTRL_MOVE_UP;
			break;

		case SDLK_KP_9:
		case SDLK_e:
			ymge->control |= YMGE_CTRL_MOVE_DOWN;
			break;

		case SDLK_KP_1:
			ymge->control |= YMGE_CTRL_YAW_LEFT;
			break;

		case SDLK_KP_3:
			ymge->control |= YMGE_CTRL_YAW_RIGHT;
			break;


		case SDLK_c:
			//glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
			break;
		}
		break;


	case SDL_KEYUP:
		switch (event->key.keysym.sym)
		{
		case SDLK_w:
			ymge->control &= ~YMGE_CTRL_MOVE_FORWARD;
			break;

		case SDLK_s:
			ymge->control &= ~YMGE_CTRL_MOVE_BACKWARD;
			break;

		case SDLK_a:
			ymge->control &= ~YMGE_CTRL_MOVE_LEFT;
			break;

		case SDLK_d:
			ymge->control &= ~YMGE_CTRL_MOVE_RIGHT;
			break;

		case SDLK_LSHIFT:
			ymge->control &= ~YMGE_CTRL_RUN;
			break;

		case SDLK_KP_7:
		case SDLK_q:
			ymge->control &= ~YMGE_CTRL_MOVE_UP;
			break;

		case SDLK_KP_9:
		case SDLK_e:
			ymge->control &= ~YMGE_CTRL_MOVE_DOWN;
			break;

		case SDLK_KP_1:
			ymge->control &= ~YMGE_CTRL_YAW_LEFT;
			break;

		case SDLK_KP_3:
			ymge->control &= ~YMGE_CTRL_YAW_RIGHT;
			break;

		case SDLK_LEFT:
			ymge->control &= ~YMGE_CTRL_ROLL_LEFT;
			break;

		case SDLK_RIGHT:
			ymge->control &= ~YMGE_CTRL_ROLL_RIGHT;
			break;

		}
		break;

	default:
		break;
	}

	//keep running
	//return 0 to quit
	return 1;
}






void app_init()
{
	int					err = 0;
	model_t*			model = 0;
	entity_t*			entity = 0;

	//mouse mode: exclusive
	ymge_enable_mouse_exclusive(1);

	//ui textures
	ymge_add_texture("ui.window", "texture/ui/window.png", 0);
	
	//feature texture
	ymge_add_texture("feature", "texture/feature.png", 0);

	//building texture
	ymge_add_texture("building", "texture/building.png", 0);

	//road texture
	ymge_add_texture("road", "texture/road.png", ymge->render_res);


	//MapDraw
	view_init();
	features_init();
	roads_init();
	paths_init();
	pois_init();
	buildings_init();


	//init HUD debug text
	ui_text_init(ymge->ui_text, ymge->window_width - 500, ymge->window_height, 0, 600);

	//gui
	gui_init();
	


	//set event handler
	ymge_set_event_handler(1, app_event);

	//set update handler
	ymge_set_update_handler(0, app_update);
}

void app_clean()
{
	//roads
	roads_clean();

	paths_clean();

	//signs
	for (uint i = 0; i < ymge->num_signs; i++)
	{
		sign_clean(ymge->signs + i);
	}

	//pois
	pois_clean();

	//features
	features_clean();

	//buildings
	buildings_clean();
}

