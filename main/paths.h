#pragma once


typedef struct
{
	char			name[50];
	int				index;
	int				num_nodes;
}
path_t;


typedef struct
{
	//paths
	path_t			paths[800];
	int				num_paths;

	//path nodes
	float			nodes[8000][2];
	int				num_nodes;

	//path connections
	int				conns[3000][2];
	int				num_conns;

	//display settings
	//height:
	float			y_kerb;
	float			y_path;
	//width:
	float			path_width;
	float			kerb_width;
}
paths_t;


void paths_init();
void paths_clean();

