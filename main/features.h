#pragma once


typedef struct
{
	char		name[50];
	int			index;
	int			num_nodes;

}
feature_t;


typedef struct
{
	int			num_features;
	feature_t	features[400];

	int			num_nodes;
	float		nodes[7000][2];
}
features_t;


void features_init();
void features_clean();
