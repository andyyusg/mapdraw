#include "ymge.h"
#include "gui.h"


//singleton
static gui_t me;


//function
void gui_init(void)
{
	texture_t*			texture = 0;

	//init ui
	ui_init(ymge->ui, 1, 2);

	//init ui templates
	ui_tpl_t* tpl = ymge->ui->templates;
	ui_tpl_init(tpl, 1024, 1024, 0, 3);

	ui_tpl_wnd_t* tpl_wnd = ui_tpl_set_wnd(tpl, 0,
		2, 123, 243, 365,
		805, 805 + 64, 805 + 128, 805 + 217);

	ui_tpl_wnd_t* tpl_title = ui_tpl_set_wnd(tpl, 1,
		367, 367, 941, 941,
		893, 893, 1022, 1022);

	ui_tpl_wnd_t* tpl_text = ui_tpl_set_wnd(tpl, 2,
		380, 300, 900, 925,
		304, 300, 42, 624);


	//init ui window groups
	ui_wnd_group_t* grp = ymge->ui->wnd_groups;
	ui_wnd_t* wnd = grp->root;

	//init ui root window
	ui_wnd_init(wnd, tpl_wnd, YMGE_UI_WINDOW_POS_X_LEFT,
		300, 0, 500, 400,
		0, 2);

	ui_wnd_init(wnd->wnds, tpl_title, YMGE_UI_WINDOW_POS_Y_TOP,
		0, 0, 400, 90,
		0, 0);

	ui_wnd_init(wnd->wnds + 1, tpl_text, YMGE_UI_WINDOW_POS_Y_TOP,
		0, -230, 545, 320,
		0, 0);

	//copy data to GPU Buffer
	ui_wnd_group_prepare(grp);

	//set texture
	texture = ymge_get_texture("ui.window");
	ui_wnd_group_set_texture(grp, texture);

	//activate
	grp->is_active = 1;
}