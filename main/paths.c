#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <file.h>

#include "paths.h"
#include "ymge.h"
#include "vector.h"

//singleton
static paths_t sPaths;

//declaration
void paths_load_from_file();
void paths_prepare_model();
void paths_init_signs();


void paths_init()
{
	//path display settings
	
	//height:
	sPaths.y_kerb = 0.03f;
	sPaths.y_path = 0.04f;

	//width:
	sPaths.path_width = 1.f;
	sPaths.kerb_width = 0.4f;

	//setup
	paths_load_from_file();
	paths_prepare_model();
	paths_init_signs();

	//paths entity
	ymge_add_entity("paths", "paths", "road", ymge->render_group);
}

void paths_clean()
{
}


void paths_load_from_file()
{
	file_t			file = { 0 };
	buf_t*			data = 0;
	char*			line = 0;
	char*			p = 0;
	char*			p2 = 0;
	float			f = 0.f;
	int				num_paths;
	int				num_nodes;
	int				index = 0;

	//read file
	if (file_open(&file, "data\\routed_path.txt", "r"))
	{
		data = file_read_all(&file);
		file_close(&file);

		line = data->data;

		//read num paths
		p = strchr(line, '\n'); *p = 0;
		p2 = strstr(line, " : ");
		num_paths = sPaths.num_paths = atoi(p2 + 3);
		line = p + 1;

		//read paths data
		for (int i = 0; i < num_paths; i++)
		{
			path_t* path = sPaths.paths + i;

			//read path name
			p = strchr(line, '\n'); *p = 0;
			strcpy_s(path->name, 50, line);
			line = p + 1;

			//read path num nodes
			p = strchr(line, '\n'); *p = 0;
			num_nodes = path->num_nodes = atoi(line);
			line = p + 1;

			//set path's index
			path->index = index;

			//read nodes
			for (int j = 0; j < num_nodes; j++)
			{
				//read node coordinates
				p = strchr(line, '\n'); *p = 0;
				p2 = strchr(line, ','); *p2 = 0;
				sPaths.nodes[index][0] = strtof(line, 0);
				sPaths.nodes[index][1] = strtof(p2 + 1, 0);
				line = p + 1;

				//increase index
				index++;
			}
		}

		//total nodes
		sPaths.num_nodes = index;

		//clean
		free(data);
	}
}



void paths_prepare_model()
{
	int			num_paths = sPaths.num_paths;
	int			num_nodes = sPaths.num_nodes;
	int			num_vertices = num_nodes * 6;
	int			num_indices = 0;

	int			index = 0;

	float		xp, zp, dist;

	//render buffers
	float*		vertices = malloc(num_vertices * 3 * sizeof(float));
	float*		uvs = malloc(num_vertices * 2 * sizeof(float));
	float*		normals = malloc(num_vertices * 3 * sizeof(float));
	u16*		indices = 0;


	//path settings
	//height:
	float y_kerb = sPaths.y_kerb;
	float y_path = sPaths.y_path;
	//width:
	float path_width = sPaths.path_width;
	float kerb_width = sPaths.kerb_width;

	float path_width_half = sPaths.path_width * .5f;
	float path_width_outter_half = path_width_half + sPaths.kerb_width;

	for (int i = 0; i < num_paths; i++)
	{
		path_t* path = sPaths.paths + i;
		int num_path_nodes = path->num_nodes;

		float dx = 0;
		float dz = 0;
		for (int i = 0; i < num_path_nodes; i++)
		{
			int node_index = path->index + i;
			float x = sPaths.nodes[node_index][0];
			float z = sPaths.nodes[node_index][1];
			float x1, z1;
			float radian;
			float v1[2], v2[2];

			//1st node
			if(i == 0)
			{
				//next node coordinates
				v1[0] = sPaths.nodes[node_index + 1][0] - x;
				v1[1] = sPaths.nodes[node_index + 1][1] - z;

				radian = v2_radian(v1);
				radian += YMGE_TRI_PI_DIV_2;

				dx = cosf(radian);
				dz = sinf(radian);
			}
			//last node
			else if (i == num_path_nodes - 1)
			{
				//next node coordinates
				v1[0] = x - sPaths.nodes[node_index - 1][0];
				v1[1] = z - sPaths.nodes[node_index - 1][1];

				radian = v2_radian(v1) + YMGE_TRI_PI_DIV_2;

				dx = cosf(radian);
				dz = sinf(radian);
			}
			//nodes in between
			else
			{
				//3 nodes' vectors
				v1[0] = x - sPaths.nodes[node_index - 1][0];
				v1[1] = z - sPaths.nodes[node_index - 1][1];
				v2[0] = sPaths.nodes[node_index + 1][0] - x;
				v2[1] = sPaths.nodes[node_index + 1][1] - z;
				radian = v2_v2_radian(v1, v2);

				radian = v2_radian(v1);
				float sin = sinf(radian);
				float cos = cosf(radian);
				dx = sinf(radian) + cosf(radian);
				dz = sinf(radian) - cosf(radian);


				//outter
				float radian1 = v2_radian(v1);
				float radian2 = v2_radian(v2);
				float t = 1.f / cosf(radian1);
				dx = (cosf(radian1) - cosf(radian2)) / sinf(radian1 - radian2);
				dz = (sinf(radian1) - sinf(radian2)) / sinf(radian1 - radian2);
			}

			//position
			// OpenGL(X, -Z) maps to MapDraw(X, Y)
			//0
			x1 = x + path_width_outter_half * dx;
			z1 = z + path_width_outter_half * dz;
			vertices[18 * node_index + 0] = x1;
			vertices[18 * node_index + 1] = y_kerb;
			vertices[18 * node_index + 2] = -z1;

			//1
			x1 = x + path_width_half * dx;
			z1 = z + path_width_half * dz;
			vertices[18 * node_index + 3] = x1;
			vertices[18 * node_index + 4] = y_kerb;
			vertices[18 * node_index + 5] = -z1;

			//2
			vertices[18 * node_index + 6] = x1;
			vertices[18 * node_index + 7] = y_path;
			vertices[18 * node_index + 8] = -z1;

			//3
			x1 = x - path_width_half * dx;
			z1 = z - path_width_half * dz;
			vertices[18 * node_index + 9] = x1;
			vertices[18 * node_index + 10] = y_path;
			vertices[18 * node_index + 11] = -z1;

			//4
			vertices[18 * node_index + 12] = x1;
			vertices[18 * node_index + 13] = y_kerb;
			vertices[18 * node_index + 14] = -z1;

			//5
			x1 = x - path_width_outter_half * dx;
			z1 = z - path_width_outter_half * dz;
			vertices[18 * node_index + 15] = x1;
			vertices[18 * node_index + 16] = y_kerb;
			vertices[18 * node_index + 17] = -z1;


			//uv
			uvs[12 * node_index + 0] = 0.5947f;
			uvs[12 * node_index + 1] = 0.06f;

			uvs[12 * node_index + 2] = 0.5947f;
			uvs[12 * node_index + 3] = 0.06f;

			uvs[12 * node_index + 4] = 0.0f;
			uvs[12 * node_index + 5] = 0.0f + (float)node_index * 1.f;

			uvs[12 * node_index + 6] = 0.4999f;
			uvs[12 * node_index + 7] = 0.0f + (float)node_index * 1.f;

			uvs[12 * node_index + 8] = 0.5947f;
			uvs[12 * node_index + 9] = 0.06f;

			uvs[12 * node_index + 10] = 0.5947f;
			uvs[12 * node_index + 11] = 0.06f;

			//adjust uv for red arrow
			if (node_index == 0)
			{
				uvs[12 * node_index + 5] = 0.0f;
				uvs[12 * node_index + 7] = 0.0f;
			}
			else
			{
				xp = sPaths.nodes[node_index - 1][0];
				zp = sPaths.nodes[node_index - 1][1];
				xp -= x;
				zp -= z;

				dist = sqrtf(xp * xp + zp * zp);

				uvs[12 * node_index + 5] = uvs[12 * (node_index - 1) + 5] + dist * 0.2f;
				uvs[12 * node_index + 7] = uvs[12 * (node_index - 1) + 7] + dist * 0.2f;
			}


			//normals
			normals[18 * node_index + 0] = 0.f;
			normals[18 * node_index + 1] = 1.f;
			normals[18 * node_index + 2] = 0.f;

			normals[18 * node_index + 3] = 0.f;
			normals[18 * node_index + 4] = 1.f;
			normals[18 * node_index + 5] = 0.f;

			normals[18 * node_index + 6] = 0.f;
			normals[18 * node_index + 7] = 1.f;
			normals[18 * node_index + 8] = 0.f;

			normals[18 * node_index + 9] = 0.f;
			normals[18 * node_index + 10] = 1.f;
			normals[18 * node_index + 11] = 0.f;

			normals[18 * node_index + 12] = 0.f;
			normals[18 * node_index + 13] = 1.f;
			normals[18 * node_index + 14] = 0.f;

			normals[18 * node_index + 15] = 0.f;
			normals[18 * node_index + 16] = 1.f;
			normals[18 * node_index + 17] = 0.f;
		}
	}

	//collect path connections
	int i, j;
	sPaths.num_conns = 0;
	for (i = 0; i < num_paths; i++)
	{
		path_t* path = sPaths.paths + i;
		int last = path->index + path->num_nodes - 1;
		float* p1 = sPaths.nodes[last];

		for (j = 0; j < num_paths; j++)
		{
			path_t* path2 = sPaths.paths + j;
			int start = path2->index;
			float* p2 = sPaths.nodes[start];
			if (fabsf(p2[0] - p1[0]) < 0.001f && fabsf(p2[1] - p1[1]) < 0.001f)
			{
				sPaths.conns[sPaths.num_conns][0] = last;
				sPaths.conns[sPaths.num_conns++][1] = start;
			}
		}
	}


	//get num indices
	for (int i = 0; i < num_paths; i++)
	{
		path_t* path = sPaths.paths + i;
		num_indices += (path->num_nodes - 1) * 18;
	}
	num_indices += sPaths.num_conns * 18;

	//populate indices
	indices = malloc(num_indices * sizeof(u16));
	index = 0;
	//paths indices
	for (int i = 0; i < num_paths; i++)
	{
		path_t* path = sPaths.paths + i;
		int num_quads = path->num_nodes - 1;
		for (int j = 0; j < num_quads; j++)
		{
			int base = path->index * 6 + 6 * j;

			//left kerb 
			indices[index++] = base + 0;
			indices[index++] = base + 1;
			indices[index++] = base + 6;
			
			indices[index++] = base + 1;
			indices[index++] = base + 7;
			indices[index++] = base + 6;

			//path
			indices[index++] = base + 2;
			indices[index++] = base + 3;
			indices[index++] = base + 8;

			indices[index++] = base + 3;
			indices[index++] = base + 9;
			indices[index++] = base + 8;

			//left kerb 
			indices[index++] = base + 4;
			indices[index++] = base + 5;
			indices[index++] = base + 10;

			indices[index++] = base + 5;
			indices[index++] = base + 11;
			indices[index++] = base + 10;
		}
	}
	//paths connections indices
	int num_conns = sPaths.num_conns;
	for (i = 0; i < num_conns; i++)
	{
		int base1 = sPaths.conns[i][0] * 6;
		int base2 = sPaths.conns[i][1] * 6;

		//left kerb 
		indices[index++] = base1 + 0;
		indices[index++] = base1 + 1;
		indices[index++] = base2 + 0;

		indices[index++] = base1 + 1;
		indices[index++] = base2 + 1;
		indices[index++] = base2 + 0;

		//path
		indices[index++] = base1 + 2;
		indices[index++] = base1 + 3;
		indices[index++] = base2 + 2;

		indices[index++] = base1 + 3;
		indices[index++] = base2 + 3;
		indices[index++] = base2 + 2;

		//left kerb 
		indices[index++] = base1 + 4;
		indices[index++] = base1 + 5;
		indices[index++] = base2 + 4;

		indices[index++] = base1 + 5;
		indices[index++] = base2 + 5;
		indices[index++] = base2 + 4;
	}

	//add path model
	ymge_add_model_from_mem("paths", num_vertices, vertices, uvs, normals, num_indices, indices, ymge->render_res);

	//clean
	free(vertices);
	free(uvs);
	free(normals);
 	free(indices);
}


void paths_init_signs()
{
	int i;

	//get ui texture
	texture_t* tui = ymge_get_texture("ui.window");

	for (i = 0; i < sPaths.num_paths; i++)
	{
		path_t* path = sPaths.paths + i;
		float* pos = sPaths.nodes[path->index + 1];
		float pos2[3] = { pos[0], 0.f, -pos[1] };

		//add sign
		ymge->num_signs ++;
		sign_init(ymge->signs + i, tui->id, pos2, path->name);
	}
}