#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <file.h>

#include "roads.h"
#include "ymge.h"
#include "vector.h"

//singleton
static roads_t me;

//declaration
void roads_load_from_file();
void roads_prepare_model();
void roads_init_signs();


void roads_init()
{
	//road display settings
	
	//height:
	me.y_kerb = 0.f;
	me.y_road = 0.01f;

	//width:
	me.road_width = 1.0f;
	me.kerb_width = 0.4f;

	//setup
	roads_load_from_file();
	roads_prepare_model();
	roads_init_signs();

	//roads entity
	ymge_add_entity("roads", "roads", "road", ymge->render_group);
}

void roads_clean()
{
}


void roads_load_from_file()
{
	file_t			file = { 0 };
	buf_t*			data = 0;
	char*			line = 0;
	char*			p = 0;
	char*			p2 = 0;
	float			f = 0.f;
	int				num_roads;
	int				num_nodes;
	int				index = 0;

	//read file
	if (file_open(&file, "data\\roads.txt", "r"))
	{
		data = file_read_all(&file);
		file_close(&file);

		line = data->data;

		//read num roads
		p = strchr(line, '\n'); *p = 0;
		p2 = strstr(line, " : ");
		num_roads = me.num_roads = atoi(p2 + 3);
		line = p + 1;

		//read roads data
		for (int i = 0; i < num_roads; i++)
		{
			road_t* road = me.roads + i;

			//read road name
			p = strchr(line, '\n'); *p = 0;
			strcpy_s(road->name, 50, line);
			line = p + 1;

			//read road num nodes
			p = strchr(line, '\n'); *p = 0;
			num_nodes = road->num_nodes = atoi(line);
			line = p + 1;

			//set road's index
			road->index = index;

			//read nodes
			for (int j = 0; j < num_nodes; j++)
			{
				//read node coordinates
				p = strchr(line, '\n'); *p = 0;
				p2 = strchr(line, ','); *p2 = 0;
				me.nodes[index][0] = strtof(line, 0);
				me.nodes[index][1] = strtof(p2 + 1, 0);
				line = p + 1;

				//increase index
				index++;
			}
		}

		//total nodes
		me.num_nodes = index;

		//clean
		free(data);
	}
}



void roads_prepare_model()
{
	int			num_roads = me.num_roads;
	int			num_nodes = me.num_nodes;
	int			num_vertices = num_nodes * 6;
	int			num_indices = 0;

	int			index = 0;

	//render buffers
	float*		vertices = malloc(num_vertices * 3 * sizeof(float));
	float*		uvs = malloc(num_vertices * 2 * sizeof(float));
	float*		normals = malloc(num_vertices * 3 * sizeof(float));
	u16*		indices = 0;


	//road settings
	//height:
	float y_kerb = me.y_kerb;
	float y_road = me.y_road;
	//width:
	float road_width = me.road_width;
	float kerb_width = me.kerb_width;

	float road_width_half = me.road_width * .5f;
	float road_width_outter_half = road_width_half + me.kerb_width;

	for (int i = 0; i < num_roads; i++)
	{
		road_t* road = me.roads + i;
		int num_road_nodes = road->num_nodes;

		float dx = 0;
		float dz = 0;
		for (int i = 0; i < num_road_nodes; i++)
		{
			int node_index = road->index + i;
			float x = me.nodes[node_index][0];
			float z = me.nodes[node_index][1];
			float x1, z1;
			float radian;
			float v1[2], v2[2];

			//1st node
			if(i == 0)
			{
				//next node coordinates
				v1[0] = me.nodes[node_index + 1][0] - x;
				v1[1] = me.nodes[node_index + 1][1] - z;

				radian = v2_radian(v1);
				radian += YMGE_TRI_PI_DIV_2;

				dx = cosf(radian);
				dz = sinf(radian);
			}
			//last node
			else if (i == num_road_nodes - 1)
			{
				//next node coordinates
				v1[0] = x - me.nodes[node_index - 1][0];
				v1[1] = z - me.nodes[node_index - 1][1];

				radian = v2_radian(v1) + YMGE_TRI_PI_DIV_2;

				dx = cosf(radian);
				dz = sinf(radian);
			}
			//nodes in between
			else
			{
				//3 nodes' vectors
				v1[0] = x - me.nodes[node_index - 1][0];
				v1[1] = z - me.nodes[node_index - 1][1];
				v2[0] = me.nodes[node_index + 1][0] - x;
				v2[1] = me.nodes[node_index + 1][1] - z;
				radian = v2_v2_radian(v1, v2);

				radian = v2_radian(v1);
				float sin = sinf(radian);
				float cos = cosf(radian);
				dx = sinf(radian) + cosf(radian);
				dz = sinf(radian) - cosf(radian);


				//outter
				float radian1 = v2_radian(v1);
				float radian2 = v2_radian(v2);
				float t = 1.f / cosf(radian1);
				dx = (cosf(radian1) - cosf(radian2)) / sinf(radian1 - radian2);
				dz = (sinf(radian1) - sinf(radian2)) / sinf(radian1 - radian2);
			}

			//position
			// OpenGL(X, -Z) maps to MapDraw(X, Y)
			//0
			x1 = x + road_width_outter_half * dx;
			z1 = z + road_width_outter_half * dz;
			vertices[18 * node_index + 0] = x1;
			vertices[18 * node_index + 1] = y_kerb;
			vertices[18 * node_index + 2] = -z1;

			//1
			x1 = x + road_width_half * dx;
			z1 = z + road_width_half * dz;
			vertices[18 * node_index + 3] = x1;
			vertices[18 * node_index + 4] = y_kerb;
			vertices[18 * node_index + 5] = -z1;

			//2
			vertices[18 * node_index + 6] = x1;
			vertices[18 * node_index + 7] = y_road;
			vertices[18 * node_index + 8] = -z1;

			//3
			x1 = x - road_width_half * dx;
			z1 = z - road_width_half * dz;
			vertices[18 * node_index + 9] = x1;
			vertices[18 * node_index + 10] = y_road;
			vertices[18 * node_index + 11] = -z1;

			//4
			vertices[18 * node_index + 12] = x1;
			vertices[18 * node_index + 13] = y_kerb;
			vertices[18 * node_index + 14] = -z1;

			//5
			x1 = x - road_width_outter_half * dx;
			z1 = z - road_width_outter_half * dz;
			vertices[18 * node_index + 15] = x1;
			vertices[18 * node_index + 16] = y_kerb;
			vertices[18 * node_index + 17] = -z1;


			//uv
			uvs[12 * node_index + 0] = 0.6f;
			uvs[12 * node_index + 1] = 0.6f;

			uvs[12 * node_index + 2] = 0.6f;
			uvs[12 * node_index + 3] = 0.6f;

			uvs[12 * node_index + 4] = 0.75f;
			uvs[12 * node_index + 5] = 0.25f;

			uvs[12 * node_index + 6] = 0.75f;
			uvs[12 * node_index + 7] = 0.25f;

			uvs[12 * node_index + 8] = 0.6f;
			uvs[12 * node_index + 9] = 0.6f;

			uvs[12 * node_index + 10] = 0.6f;
			uvs[12 * node_index + 11] = 0.6f;

			//normals
			normals[18 * node_index + 0] = 0.f;
			normals[18 * node_index + 1] = 1.f;
			normals[18 * node_index + 2] = 0.f;

			normals[18 * node_index + 3] = 0.f;
			normals[18 * node_index + 4] = 1.f;
			normals[18 * node_index + 5] = 0.f;

			normals[18 * node_index + 6] = 0.f;
			normals[18 * node_index + 7] = 1.f;
			normals[18 * node_index + 8] = 0.f;

			normals[18 * node_index + 9] = 0.f;
			normals[18 * node_index + 10] = 1.f;
			normals[18 * node_index + 11] = 0.f;

			normals[18 * node_index + 12] = 0.f;
			normals[18 * node_index + 13] = 1.f;
			normals[18 * node_index + 14] = 0.f;

			normals[18 * node_index + 15] = 0.f;
			normals[18 * node_index + 16] = 1.f;
			normals[18 * node_index + 17] = 0.f;
		}
	}

	//collect road connections
	int i, j;
	me.num_conns = 0;
	for (i = 0; i < num_roads; i++)
	{
		road_t* road = me.roads + i;
		int last = road->index + road->num_nodes - 1;
		float* p1 = me.nodes[last];

		for (j = 0; j < num_roads; j++)
		{
			road_t* road2 = me.roads + j;
			int start = road2->index;
			float* p2 = me.nodes[start];
			if (fabsf(p2[0] - p1[0]) < 0.001f && fabsf(p2[1] - p1[1]) < 0.001f)
			{
				me.conns[me.num_conns][0] = last;
				me.conns[me.num_conns++][1] = start;
			}
		}
	}


	//get num indices
	for (int i = 0; i < num_roads; i++)
	{
		road_t* road = me.roads + i;
		num_indices += (road->num_nodes - 1) * 18;
	}
	num_indices += me.num_conns * 18;

	//populate indices
	indices = malloc(num_indices * sizeof(u16));
	index = 0;
	//roads indices
	for (int i = 0; i < num_roads; i++)
	{
		road_t* road = me.roads + i;
		int num_quads = road->num_nodes - 1;
		for (int j = 0; j < num_quads; j++)
		{
			int base = road->index * 6 + 6 * j;

			//left kerb 
			indices[index++] = base + 0;
			indices[index++] = base + 1;
			indices[index++] = base + 6;
			
			indices[index++] = base + 1;
			indices[index++] = base + 7;
			indices[index++] = base + 6;

			//road
			indices[index++] = base + 2;
			indices[index++] = base + 3;
			indices[index++] = base + 8;

			indices[index++] = base + 3;
			indices[index++] = base + 9;
			indices[index++] = base + 8;

			//left kerb 
			indices[index++] = base + 4;
			indices[index++] = base + 5;
			indices[index++] = base + 10;

			indices[index++] = base + 5;
			indices[index++] = base + 11;
			indices[index++] = base + 10;
		}
	}
	//roads connections indices
	int num_conns = me.num_conns;
	for (i = 0; i < num_conns; i++)
	{
		int base1 = me.conns[i][0] * 6;
		int base2 = me.conns[i][1] * 6;

		//left kerb 
		indices[index++] = base1 + 0;
		indices[index++] = base1 + 1;
		indices[index++] = base2 + 0;

		indices[index++] = base1 + 1;
		indices[index++] = base2 + 1;
		indices[index++] = base2 + 0;

		//road
		indices[index++] = base1 + 2;
		indices[index++] = base1 + 3;
		indices[index++] = base2 + 2;

		indices[index++] = base1 + 3;
		indices[index++] = base2 + 3;
		indices[index++] = base2 + 2;

		//left kerb 
		indices[index++] = base1 + 4;
		indices[index++] = base1 + 5;
		indices[index++] = base2 + 4;

		indices[index++] = base1 + 5;
		indices[index++] = base2 + 5;
		indices[index++] = base2 + 4;
	}

	//add road model
	ymge_add_model_from_mem("roads", num_vertices, vertices, uvs, normals, num_indices, indices, ymge->render_res);

	//clean
	free(vertices);
	free(uvs);
	free(normals);
 	free(indices);
}


void roads_init_signs()
{
	int i;

	//get ui texture
	texture_t* tui = ymge_get_texture("ui.window");

	for (i = 0; i < me.num_roads; i++)
	{
		road_t* road = me.roads + i;
		float* pos = me.nodes[road->index + 1];
		float pos2[3] = { pos[0], 0.f, -pos[1] };

		//add sign
		ymge->num_signs ++;
		sign_init(ymge->signs + i, tui->id, pos2, road->name);
	}
}