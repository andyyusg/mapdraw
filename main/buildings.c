#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include <file.h>

#include "ymge.h"
#include "buildings.h"


//singleton
static buildings_t sBuildings;



static void load_from_file(void)
{
	file_t			file = { 0 };
	buf_t*			data = 0;
	char*			line = 0;
	char*			p = 0;
	char*			p2 = 0;
	float			f = 0.f;
	int				num_features;
	int				num_nodes;
	int				index = 0;

	//read file
	if (file_open(&file, "data\\buildings.txt", "r"))
	{
		data = file_read_all(&file);
		file_close(&file);

		line = data->data;

		//read num roads
		p = strchr(line, '\n'); *p = 0;
		p2 = strstr(line, " : ");
		num_features = sBuildings.num_buildings = atoi(p2 + 3);
		line = p + 1;

		//read roads data
		for (int i = 0; i < num_features; i++)
		{
			building_t* building = sBuildings.buildings + i;

			//read road name
			p = strchr(line, '\n'); *p = 0;
			strcpy_s(building->name, 50, line);
			line = p + 1;

			//read road num nodes
			p = strchr(line, '\n'); *p = 0;
			num_nodes = building->num_nodes = atoi(line);
			line = p + 1;

			//set road's index
			building->index = index;

			//read nodes
			for (int j = 0; j < num_nodes; j++)
			{
				//read node coordinates
				p = strchr(line, '\n'); *p = 0;
				p2 = strchr(line, ','); *p2 = 0;
				sBuildings.nodes[index][0] = strtof(line, 0);
				sBuildings.nodes[index][1] = strtof(p2 + 1, 0);
				line = p + 1;

				//increase index
				index++;
			}
		}

		//total nodes
		sBuildings.num_nodes = index;

		//clean
		free(data);
	}
}


static int sNumNodes;
static float sNodes[3000][2];
static int sNodeIDs[3000];

static int sNumTris;
static int sNumTrisTotal;
static float sTris[6000][3][2];
static u16 sTriIDs[6000][3];


static void building_tri()
{
	int i, j;
	int found;
	int left;
	int id0, id1, id2;
	float *p0, *p1, *p2;
	float a[2], d[2], t[2];
	float r;
	float cross_a, dot_d;

	//clear state
	sNumTris = 0;

	for (i = 0; i < sNumNodes;)
	{
		id0 = i;
		id1 = i + 1;
		id2 = i + 2;

		//adjust index
		if (id0 == sNumNodes - 2)
		{
			id2 = 0;
		}
		else if (id0 == sNumNodes - 1)
		{
			id1 = 0;
			id2 = 1;
		}

		p0 = sNodes[id0];
		p1 = sNodes[id1];
		p2 = sNodes[id2];

		a[0] = p1[0] - p0[0];
		a[1] = p1[1] - p0[1];

		d[0] = p2[0] - p0[0];
		d[1] = p2[1] - p0[1];

		//get a's dir
		cross_a = v2_cross_v2(d, a);
		cross_a /= fabsf(cross_a);

		//get d's dot
		dot_d = v2_dot_v2(d, d);

		//search all points for bad point that ruins diagonal
		found = FALSE;
		for (j = 0; j < sNumNodes; j++)
		{
			//ignore current 3 points
			if (j == i || j == i + 1 || j == i + 2)
				continue;

			t[0] = sNodes[j][0] - p0[0];
			t[1] = sNodes[j][1] - p0[1];

			r = v2_cross_v2(d, t);

			//this point is on the same side of a
			if (r*cross_a > 0)
			{
				r = v2_dot_v2(d, t);
				if (r > 0 && r < dot_d)
				{
					//found bad point
					found = TRUE;
					break;
				}
			}
		}


		if (found)
		{
			//bad point, continue, increase i
			i++;
		}
		else
		{
			//collect good tri p0-p1-p2
			int numTris = sNumTrisTotal + sNumTris;
			sTriIDs[numTris][0] = sNodeIDs[id0];

			sTris[numTris][0][0] = p0[0];
			sTris[numTris][0][1] = p0[1];

			if (cross_a > 0.f)
			{
				sTriIDs[numTris][1] = sNodeIDs[id2];
				sTriIDs[numTris][2] = sNodeIDs[id1];

				sTris[numTris][1][0] = p2[0];
				sTris[numTris][1][1] = p2[1];

				sTris[numTris][2][0] = p1[0];
				sTris[numTris][2][1] = p1[1];
			}
			else
			{
				sTriIDs[numTris][1] = sNodeIDs[id1];
				sTriIDs[numTris][2] = sNodeIDs[id2];

				sTris[numTris][1][0] = p1[0];
				sTris[numTris][1][1] = p1[1];

				sTris[numTris][2][0] = p2[0];
				sTris[numTris][2][1] = p2[1];
			}
			sNumTris++;

			//remove p1
			if (id2 == 0)
			{
			}
			else if (id2 == 1)
			{
				memmove(sNodes, sNodes[1], (sNumNodes - 1) * 2 * sizeof(float));
				memmove(sNodeIDs, sNodeIDs + 1, (sNumNodes - 1) * sizeof(int));
			}
			else
			{
				left = sNumNodes - id1 - 1;
				assert(left > 0);
				memmove(sNodes[i + 1], sNodes[i + 2], left * 2 * sizeof(float));
				memmove(sNodeIDs + i + 1, sNodeIDs + i + 2, left * sizeof(int));
			}

			//decrease num nodes
			sNumNodes--;
			if (sNumNodes < 3)
				break;

			//reset i
			i = 0;
		}
	}

	//increase total num tris
	sNumTrisTotal += sNumTris;
}

static void build_walls()
{
	int i, j;
	int num_buildings = sBuildings.num_buildings;
	int num_nodes;
	int last_node;
	int first_node;
	u16 *n;
	int top, bot;
	int top_next, bot_next;

	for (i = 0; i < num_buildings; i++)
	{
		building_t* building = sBuildings.buildings + i;
		first_node = building->index;
		num_nodes = building->num_nodes;
		last_node = first_node + building->num_nodes - 1;

		for (j = 0; j < num_nodes; j++)
		{
			top = first_node + j;
			bot = top + sBuildings.num_nodes;
			if (top == last_node)
			{
				top_next = first_node;
				bot_next = first_node + sBuildings.num_nodes;
			}
			else
			{
				top_next = top + 1;
				bot_next = bot + 1;
			}

			//1st tri
			n = sTriIDs[sNumTrisTotal++];
			n[0] = top;
			n[1] = top_next;
			n[2] = bot;
			//2nd tri
			n = sTriIDs[sNumTrisTotal++];
			n[0] = top_next;
			n[1] = bot_next;
			n[2] = bot;
		}
	}
}

static void triangulate()
{
	int i, j;
	int first_node;

	//reset state
	sNumTrisTotal = 0;

	for (i = 0; i < sBuildings.num_buildings; i++)
	{
		building_t* building = sBuildings.buildings + i;
		first_node = building->index;
		sNumNodes = building->num_nodes;

		//init nodes
		for (j = 0; j < sNumNodes; j++)
		{
			sNodeIDs[j] = first_node + j;

			sNodes[j][0] = sBuildings.nodes[first_node + j][0];
			sNodes[j][1] = sBuildings.nodes[first_node + j][1];
		}


		//triangulate
		building_tri();
	}
}


static void prepare_model(void)
{
	int i;
	int count;
	int num_indices = 0;
	int index = 0;
	float y_building = 15.f;

	//render objs
	float* pos = malloc(2 * sBuildings.num_nodes * 3 * sizeof(float));
	float* uv = malloc(2 * sBuildings.num_nodes * 2 * sizeof(float));
	float* normal = malloc(2 * sBuildings.num_nodes * 3 * sizeof(float));

	//building roof vertices
	for (i = 0; i < sBuildings.num_nodes; i++)
	{
		pos[3 * i + 0] = sBuildings.nodes[i][0];
		pos[3 * i + 1] = y_building;
		pos[3 * i + 2] = -sBuildings.nodes[i][1];

		uv[2 * i + 0] = 2.f * i;
		uv[2 * i + 1] = 1.0f;

		uv[2 * i + 0] = i % 2 ? 1.f : 0.0f;
		uv[2 * i + 1] = 1.0f;

		normal[3 * i + 0] = 0.f;
		normal[3 * i + 1] = 1.f;
		normal[3 * i + 2] = 0.f;
	}

	//building bottom vertices
	count = sBuildings.num_nodes * 2;
	for (i = sBuildings.num_nodes; i < count; i++)
	{
		pos[3 * i + 0] = sBuildings.nodes[i - sBuildings.num_nodes][0];
		pos[3 * i + 1] = 0;
		pos[3 * i + 2] = -sBuildings.nodes[i - sBuildings.num_nodes][1];

		uv[2 * i + 0] = 2.f * i;
		uv[2 * i + 1] = 0.f;

		uv[2 * i + 0] = (i- sBuildings.num_nodes) % 2 ? 1.f : 0.0f;
		uv[2 * i + 1] = 0.0f;

		normal[3 * i + 0] = 0.f;
		normal[3 * i + 1] = 1.f;
		normal[3 * i + 2] = 0.f;
	}


	//add buildings model
	ymge_add_model_from_mem("buildings", count, pos, uv, normal, sNumTrisTotal * 3, (u16*)sTriIDs, 0);

	//clean
	free(pos);
	free(uv);
	free(normal);
}

void buildings_init()
{
	//setup
	load_from_file();
	triangulate();
	build_walls();
	prepare_model();

	//buildings entity
	ymge_add_entity("buildings", "buildings", "building", 0);
}

void buildings_clean()
{
}
