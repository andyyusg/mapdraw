﻿#define WIN32_LEAN_AND_MEAN
#include <Windows.h>


#include <ymge.h>
#include <logger.h>

#include "app.h"


INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR strCmdLine, INT iCmdShow)
{
	ymge_t*			ymge = 0;
	ymge_config_t	config = { 0 };

	//config log
	config.log_filename = "MapDraw.log";
	config.log_level = 0;

	//config window
	config.window_title = "MapDraw";
	config.window_width = 1920;
	config.window_height = 1080;
	config.window_fullscreen = 0;
	config.window_maximized = 1;
	config.window_resizable = 0;

	// choose fov(up down y dir) 29.35 degrees
	// so that horizontally fov is just within 45 degrees if ar is (1920/1080 1.777777~)
	// tan(fov_h) = ar * tan(fov_v)
	// 1 = 1.777777777 * tan(39.35)
	// keep fov_h and fov_v within 45 degrees, meaning its tangent value within -1 to 1
	// so that my fast atan tri_atan can work
	//
	// Besides all above, 29.35 is a visually appealing fov
	config.fov_half = 29.35f;
	config.near_plane = 0.4f;
	config.far_plane = 1000.f;

	//init ymge
	ymge = ymge_new(&config);
	if (ymge->error)
		return -1;

	//custom app init
	app_init();

	//run
	ymge_run(ymge);

	//app clean
	app_clean();

	//clean
	ymge_del(ymge);

	return 0;
}