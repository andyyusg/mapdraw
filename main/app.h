#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

void app_init();
void app_clean();

int app_event(SDL_Event* event);


#ifdef __cplusplus
}
#endif