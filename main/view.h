#pragma once

#include "ymge.h"

typedef struct
{
	//settings
	float			rotate_speed;
	float			move_speed;

	//state
	float			t[3];				//translation
	float			r[3];				//rotation
	float			zoom;				//zoom distance
	int				is_2d;
}
view_t;


//function
void view_init(void);
void view_update(void);

void view_toggle_2d();
float* view_get_pos();
