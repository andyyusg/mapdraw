1. SDL2
https://www.libsdl.org/

2. GLEW
git clone https://github.com/nigels-com/glew.git glew

3. Freetype
https://download.savannah.gnu.org/releases/freetype/ft29.zip

4. libpng & zlib
http://www.libpng.org/pub/png/libpng.html
