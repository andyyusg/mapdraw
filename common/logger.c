#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#ifdef _WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#else
#include <sys/time.h>
#include <time.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <execinfo.h>
#include <signal.h>
#include <pthread.h>
#endif
#include <stdlib.h>

#include "logger.h"


#ifdef _WINDOWS
HANDLE sLogFile = 0;
#else
static FILE* sLogFile = 0;
#endif

unsigned int gLogLevel = 0;


//log filename without date affix
static char*				sLogFilename = 0;

//current log month day
static int					sLogDay = 0;

//mutex to check month day
#ifdef _WINDOWS
#else
static pthread_mutex_t		sMutex = PTHREAD_MUTEX_INITIALIZER;
#endif



void log_init(const char* strLogFilename, unsigned int nLogLevel)
{
	size_t			filename_len = 0;


	//log level
	gLogLevel = nLogLevel;

#ifdef _WINDOWS
	if(strLogFilename)
		sLogFile = CreateFileA( strLogFilename, GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
	SetFilePointer( sLogFile, 0, 0, FILE_END );
#else
	if(strLogFilename)
	{
		struct timeval		tv;
		struct tm*			t = 0;
		char				filename[256];

		//malloc log filename
		sLogFilename = malloc(256);

		//set log filename
		filename_len = strlen(strLogFilename);
		if(filename_len < 256)
			strcpy(sLogFilename, strLogFilename);
		else
			strcpy(sLogFilename, "log");
		
		//get time
		gettimeofday(&tv, 0);
		t = localtime(&(tv.tv_sec));
		
		//get month's day
		sLogDay = t->tm_mday;

		//set full log filename
		snprintf(filename, 256, "%s_%04d-%02d-%02d", sLogFilename, t->tm_year+1900, t->tm_mon+1, t->tm_mday);

		//open file
		sLogFile = fopen(filename, "a");
	}
#endif
}


void log_add(unsigned int iLevel, const char* strFormat, ...)
{
	va_list args;
	size_t	len = 0;

#ifdef _WINDOWS

	char strBuf[1028];
	SYSTEMTIME t;
	DWORD dwThreadID;
	
	//log thread id
	dwThreadID = GetCurrentThreadId();


	//log time
	GetLocalTime( &t );
	snprintf( strBuf, 1028, "%04d-%02d-%02d %02d:%02d:%02d-%03d [%1d] [%d]", t.wYear, t.wMonth, t.wDay, t.wHour, t.wMinute, t.wSecond, t.wMilliseconds, iLevel, dwThreadID );

	//get log header length
	len = strlen( strBuf );

	//log body
	va_start( args, strFormat );
	vsnprintf( strBuf + len, 1028 - len, strFormat, args );
	va_end( args );

	//line end
	strcat_s( strBuf, 1028, "\n" );

	//write to file
	if(sLogFile)
		WriteFile( sLogFile, strBuf, (DWORD)strlen( strBuf ), 0, 0 );
	else
		printf( strBuf );

#else
	struct timeval tv;
	struct tm t;
	char strBuf[1031];
	long tid = 0;


	//log thread id
	tid = syscall(SYS_gettid);

	//log time
	gettimeofday(&tv, 0);
	localtime_r(&(tv.tv_sec), &t);
	snprintf(strBuf, 1031, "%04d-%02d-%02d %02d:%02d:%02d-%06d [%1d] [%d]", t.tm_year+1900, t.tm_mon+1, t.tm_mday, t.tm_hour, t.tm_min, (int)t.tm_sec, (int)tv.tv_usec, iLevel, (int)tid);

	//get log header length
	len = strlen(strBuf);

	//log body
	va_start(args, strFormat);
	vsnprintf(strBuf + len, 1031 - len, strFormat, args);
	va_end(args);
	
	//line end
	if(strlen(strBuf) < 1030)
		strcat(strBuf, "\n");
	

	//check log month day
	if(sLogFilename)
	{
		pthread_mutex_lock(&sMutex);
		if(sLogDay != t.tm_mday)
		{
			char	filename[256];

			//day changed
			fclose(sLogFile);

			//set full log filename
			snprintf(filename, 256, "%s_%04d-%02d-%02d", sLogFilename, t.tm_year+1900, t.tm_mon+1, t.tm_mday);

			//open file
			sLogFile = fopen(filename, "a");
		}
		pthread_mutex_unlock(&sMutex);
	}


	//write to file
	if(sLogFile)
	{
		fprintf(sLogFile, strBuf);
		fflush(sLogFile);
	}
	else
	{
		printf(strBuf);
		fflush(stdout);
	}
#endif
}

#ifdef _WINDOWS
#else
void log_backtrace(int sig)
{
	void*		traces[20] = {0};
	char**		trace_names = 0;
	int		size = 0;
	int		i = 0;

	LOG3("[backtrace] SIGSEGV, segmentation fault caught!");

	size = backtrace(traces, 20);
	trace_names = backtrace_symbols(traces, size);
	
	if(trace_names)
	{
		
		for(i=0; i<size; i++)
		{
			char str[256];

			LOG3("[backtrace] %s", trace_names[i]);
			sprintf(str, "addr2line %p -e server", traces[i]);
			system(str);
			
		}

		//clean
		free(trace_names);
	}

	if(sig == SIGSEGV)
	{
		exit(1);
	}
}
#endif


void log_clean()
{
	//free log filename
	if(sLogFilename)
		free(sLogFilename);

	if(sLogFile)
#ifdef _WINDOWS
		CloseHandle( sLogFile );
#else
		fclose(sLogFile);
#endif
}
