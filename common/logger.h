//gLogLevel controls runtime log level
#define LOG(level, ...) do{if(level>=gLogLevel)log_add(level, __VA_ARGS__);}while(0)


//define LOG_LEVEL 1 and above will eliminate lower level log code entirely at runtime
//perfect for production code
#if LOG_LEVEL > 0 
	#define LOG0(...)
#else								
	#define LOG0(...) LOG(0, __VA_ARGS__)
#endif

#if LOG_LEVEL > 1 
	#define LOG1(...)
#else								
	#define LOG1(...) LOG(1, __VA_ARGS__)
#endif

#if LOG_LEVEL > 2 
	#define LOG2(...) 			
#else								
	#define LOG2(...) LOG(2, __VA_ARGS__)		
#endif

#if LOG_LEVEL > 3
	#define LOG3(...) 			
#else								
	#define LOG3(...) LOG(3, __VA_ARGS__)		
#endif

#if LOG_LEVEL > 4
	#define LOG4(...) 			
#else								
	#define LOG4(...) LOG(4, __VA_ARGS__)		
#endif




#ifdef __cplusplus
extern "C"
{
#endif

extern unsigned int gLogLevel;
	
void log_init(const char* strLogFilename, unsigned int nLogLevel);
void log_add(unsigned int iLevel, const char* strFormat, ...);
void log_backtrace(int sig);
void log_clean();



#ifdef __cplusplus
}
#endif
