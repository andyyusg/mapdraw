#pragma once

#include <stdio.h>

#include "buf.h"


typedef struct file_s
{
	FILE*		handle;
}
file_t;



//init
void file_init(file_t* this);
file_t* file_new();

//clean
void file_clean(file_t* this);
void file_del(file_t* this);


int file_open(file_t* this, const char* filename, char* mode);
void file_close(file_t* this);
long file_get_size(file_t* this);
buf_t* file_read_all(file_t* this);
size_t file_write(file_t* this, buf_t* buf);
size_t file_write_array(file_t* this, char* data, size_t len);
