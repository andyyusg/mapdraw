#include "timer.h"



#ifdef _WINDOWS

void timer_init(timer_t* me)
{
	/*
	DWORD_PTR	procMask;
	DWORD_PTR	sysMask;

	GetProcessAffinityMask(GetCurrentProcess(), &procMask, &sysMask);
	*/
	LARGE_INTEGER freq;
	HANDLE hThread = GetCurrentThread();
	DWORD_PTR dwThreadAffinity = SetThreadAffinityMask(hThread, 1);

	//query frequency, this need to be done only once, frequency doesn't change during system run
	QueryPerformanceFrequency(&freq);
	me->onetick = 1.0 / (double)freq.QuadPart;

	//restore thread affinity
	SetThreadAffinityMask(hThread, dwThreadAffinity);
}


void timer_start(timer_t* me)
{
	HANDLE hThread = GetCurrentThread();
	DWORD_PTR dwThreadAffinity = SetThreadAffinityMask(hThread, 1);

	//query counter - for start
	QueryPerformanceCounter(&me->start);
	SetThreadAffinityMask(hThread, dwThreadAffinity);
}


void timer_stop(timer_t* me)
{
	LARGE_INTEGER counter;
	HANDLE hThread = GetCurrentThread();
	DWORD_PTR dwThreadAffinity = SetThreadAffinityMask(hThread, 1);

	//query counter - for end
	QueryPerformanceCounter(&counter);
	SetThreadAffinityMask(hThread, dwThreadAffinity);

	//get elapsed ticks
	me->elapsed_double = (double)(counter.QuadPart - me->start.QuadPart);

	//calculate elapsed seconds
	me->elapsed_double *= me->onetick;

	//convert elapsed to float
	me->elapsed = (float)me->elapsed_double;
}

void timer_update(timer_t* me)
{
	LARGE_INTEGER counter;
	HANDLE hThread = GetCurrentThread();
	DWORD_PTR dwThreadAffinity = SetThreadAffinityMask(hThread, 1);

	//query counter - for end
	QueryPerformanceCounter(&counter);
	SetThreadAffinityMask(hThread, dwThreadAffinity);

	//get elapsed ticks
	me->elapsed_double = (double)(counter.QuadPart - me->start.QuadPart);

	//reset start
	me->start = counter;

	//calculate elapsed seconds
	me->elapsed_double *= me->onetick;

	//convert elapsed to float
	me->elapsed = (float)me->elapsed_double;
}


#else
void timer_start(struct timeval* tv)
{
	gettimeofday(tv, 0);
}


void timer_stop(struct timeval* tv)
{
	struct timeval		tv2;

	gettimeofday(&tv2, 0);

	//result
	tv->tv_sec = tv2.tv_sec - tv->tv_sec;
	tv->tv_usec = tv2.tv_usec - tv->tv_usec;

	//adjust
	if(tv->tv_usec < 0)
	{
		tv->tv_sec--;
		tv->tv_usec += 1000000;
	}
}

#endif