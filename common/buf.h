#pragma once

#include <stddef.h>
#include <ymtype.h>


//buf
typedef struct buf_s
{
	size_t	size;
	size_t	len;
	char	data[1];
}
buf_t;




//buf_ref
typedef struct buf_ref_s
{
	char*			data;
	size_t			len;
}
buf_ref_t;


//buf_ref_pair
typedef struct buf_ref_pair_s
{
	buf_ref_t					key;
	buf_ref_t					value;
}
buf_ref_pair_t;




//array split params
typedef struct array_split_s
{
	//split params
	char*			start;
	size_t			len;

	//find result
	char*			find;
	size_t			find_len;
}
array_split_t;



#ifdef __cplusplus
extern "C"
{
#endif

//new
buf_t* buf_new(size_t size);
buf_t* buf_new_from_array(const char* array, size_t len);
buf_t* buf_new_from_array_as_string(const char* array, size_t len);
buf_t* buf_new_from_string(const char* str);
buf_t* buf_new_from_string_as_string(const char* str);
buf_t* buf_new_from_buf(buf_t* from);

//del
void buf_del(buf_t* buf);

//function
char* buf_search(buf_t* buf_from, char* data, unsigned int len);
char* buf_search_offset(buf_t* buf_from, int offset, char* data, unsigned int len);
int buf_compare_string(buf_t* this, char* str);

void buf_append_buf_ref(buf_t* me, buf_ref_t* append);
void buf_append_u8(buf_t* me, u8 uc);
void buf_append_u16(buf_t* me, u16 ushort);
void buf_append_short_string(buf_t* me, char* str);



//array
char* array_search(char* from, size_t from_len, char* data, size_t len);
void array_split(array_split_t* split, char* sep, unsigned int sep_len);
int array_validate_string(char* data, size_t len);
int array_compare(char* array, unsigned int len, char* array2, unsigned int len2);
int array_compare_string(char* array, size_t len, const char* str);

int buf_ref_validate_string(buf_ref_t* ref);

#ifdef __cplusplus
}
#endif
