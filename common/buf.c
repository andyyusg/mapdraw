#include <stdlib.h>
#include <string.h>

#include "buf.h"
#include "logger.h"


buf_t* buf_new(size_t size)
{
	size_t size_total = sizeof(buf_t) - 1 + size;
	buf_t* this = malloc(size_total);
	if(this == 0)
	{
		LOG4("[o] buf_new failed, size %u", size_total);
		return 0;
	}
	memset(this, 0, size_total);

	this->size = size;
	return this;
}


buf_t* buf_new_from_array(const char* array, size_t len)
{
	size_t size_total = sizeof(buf_t) - 1 + len;
	buf_t* this = malloc(size_total);
	if(this == 0)
	{
		LOG4("[o] buf_new_from_array failed, size %u", size_total);
	}
	memcpy(this->data, array, len);

	this->size = len;
	this->len = len;

	return this;
}


buf_t* buf_new_from_buf(buf_t* from)
{
	return buf_new_from_array(from->data, from->len);
}


buf_t* buf_new_from_string(const char* str)
{
	size_t len = strlen(str);
	return buf_new_from_array(str, len);
}

//buf as a string
//len = string length
//size = len + 1, including the string end char '0'
buf_t* buf_new_from_array_as_string(const char* array, size_t len)
{
	size_t size_total = sizeof(buf_t) - 1 + len;
	buf_t* this = malloc(size_total + 1);
	if(this == 0)
	{
		LOG4("[o] buf_new_from_array_as_string failed, size %u", size_total);
	}
	memcpy(this->data, array, len);
	this->data[len] = 0;				//string end char '0'

	this->size = len + 1;
	this->len = len;

	return this;
}



buf_t* buf_new_from_string_as_string(const char* str)
{
	size_t len = strlen(str);
	return buf_new_from_array_as_string(str, len);
}



//buf can be safely destroyed by standrad library free()
//or just call this buf_del
void buf_del(buf_t* buf)
{
	if(buf)
		free(buf);
}


void buf_transfer(buf_t* this, buf_t* to)
{
	//check
	if((to->len + this->len) > to->size)
	{
		LOG4("[o] buf_transfer overflow");
		return;
	}

	memcpy(to->data + to->len, this->data, this->len);
	to->len += this->len;
	this->len = 0;
}

void buf_set_array(buf_t* this, char* array, size_t len)
{
	//check
	if(len > this->size)
	{
		LOG4("[o] buf_set_array OVERFLOW!!!");
		return;
	}

	memcpy(this->data, array, len);
	this->len = len;
}



void buf_set_buf_ref(buf_t* this, buf_ref_t* ref)
{
	buf_set_array(this, ref->data, ref->len);
}


void buf_set_string(buf_t* this, char* str)
{
	size_t len = strlen(str);
	buf_set_array(this, str, len);
}

void buf_append_array(buf_t* this, char* array, size_t len)
{
	memcpy(this->data + this->len, array, len);
	this->len += len;
}

//this function is similar with buf_transfer
//difference are:
// 1. append direction
// 2. buf_transfer resets len of its first buf (buf to append)
void buf_append(buf_t* this, buf_t* append)
{
	//check
	if((this->len + append->len) > this->size)
	{
		LOG4("[o] buf_append overflow");
		return;
	}

	memcpy(this->data + this->len, append->data, append->len);
	this->len += append->len;
}

void buf_append_buf_ref(buf_t* me, buf_ref_t* append)
{
	buf_append_array(me, append->data, append->len);
}


void buf_append_u8(buf_t* me, u8 uc)
{
	me->data[me->len++] = uc;
}

void buf_append_u16(buf_t* me, u16 ushort)
{
	*(u16*)(me->data + me->len) = ushort;
	me->len += 2;
}

void buf_append_short_string(buf_t* me, char* str)
{
	size_t str_len = strlen(str);
	size_t new_len = me->len + 1 + str_len;
	
	if(str_len < 256 && me->size >= new_len)
	{
		me->data[me->len] = (u8)str_len;
		memcpy(me->data + me->len + 1, str, str_len);
		me->len = new_len;
	}
}


void buf_move(buf_t* this, buf_t* to)
{
	memcpy(to->data, this->data, this->len);
	to->len = this->len;
	this->len = 0;
}


void buf_consume(buf_t* this, size_t len)
{
	size_t len_new = 0;

	if(len > this->len)
	{
		LOG2("[O] buf_consume overflow (%u/%u)", len, this->len);
	}
	else if(len == this->len)
	{
		this->len = 0;
	}
	else
	{
		len_new = this->len - len;
		memmove(this->data, this->data + len, len_new);
		this->len = len_new;
	}

	//LOG0("[o] buf_consume, consumed len: %u, new len: %u, %u", len, this->len, len_new);
}

char* array_search(char* from, size_t from_len, char* data, size_t len)
{
	char* p1 = from;
	char* p2 = data;
	char* to = from + from_len - len;
	size_t i;

	while(p1 <= to)
	{
		for(i=0; i<len; i++)
		{
			if(*(p1+i) != *(p2+i))
			{
				break;
			}
		}

		if(i == len)
		{
			//found
			return p1;
		}

		p1++;
	}

	//not found
	return 0;
}


//success:	split->find points to the array
//fail:		split->find = 0
void array_split(array_split_t* split, char* sep, unsigned int sep_len)
{
	char*		find;

	find = array_search(split->start, split->len, sep, sep_len);
	if(find)
	{
		//found
		split->find = split->start;
		split->find_len = find - split->start;

		//advance pointer
		split->start = find + sep_len;
		split->len -= split->find_len + sep_len;
	}
	else
	{
		//not found
		split->find = 0;
	}
}

int array_compare(char* array, unsigned int len, char* array2, unsigned int len2)
{
	if(len == len2)
		return memcmp(array, array2, len);
	else
		return len - len2;
}


int array_compare_string(char* array, size_t len, const char* str)
{
	size_t str_len = strlen(str);

	if(len == str_len)
		return memcmp(array, str, len);
	else
		return (int)(len - str_len);
}



//
int array_validate_string(char* data, size_t len)
{
	size_t			i;
	char			c;

	for(i=0; i<len; i++)
	{
		//char
		c = data[i];

		//check
		if((c > 96 && c < 123) ||		// a-z
		(c > 64 && c < 91) ||			// A-Z
		(c > 47 && c < 58) ||			// 0-9
		c == 95)						// _
		{
			//OK
		}
		else
		{
			//invalid character
			return 0;
		}
	}

	return 1;
}


int buf_ref_validate_string(buf_ref_t* ref)
{
	return array_validate_string(ref->data, ref->len);
}


int buf_compare_string(buf_t* this, char* str)
{
	return array_compare_string(this->data, this->len, str);
}


int buf_ref_compare_string(buf_ref_t* this, char* str)
{
	return array_compare_string(this->data, this->len, str);
}


char* array_search_string(char* from, size_t from_len, char* str)
{
	size_t len = strlen(str);
	
	if(from)
		return array_search(from, from_len, str, len);
	else
		return 0;
}

char* buf_ref_search(buf_ref_t* buf_from, char* data, unsigned int len)
{
	return array_search(buf_from->data, buf_from->len, data, len);
}


char* buf_search(buf_t* buf_from, char* data, unsigned int len)
{
	return array_search(buf_from->data, buf_from->len, data, len);
}


char* buf_search_offset(buf_t* buf_from, int offset, char* data, unsigned int len)
{
	return array_search(buf_from->data + offset, buf_from->len, data, len);
}

char* buf_ref_search_string(buf_ref_t* buf_from, char* str)
{
	return array_search_string(buf_from->data, buf_from->len, str);
}



