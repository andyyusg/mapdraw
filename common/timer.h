#pragma once

#ifdef _WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#else
#include <sys/time.h>
#endif

#ifdef _WINDOWS
typedef struct
{
	HANDLE			hThread;
	DWORD_PTR		dwThreadMask;

	double			onetick;	//one tick time - 1.0/frequency
	LARGE_INTEGER	start;
	double			elapsed_double;	//elapsed seconds
	float			elapsed;
}
timer_t;


void timer_init(timer_t* me);
void timer_start(timer_t* me);
void timer_stop(timer_t* me);
void timer_update(timer_t* me);

#else
void timer_start(struct timeval* tv);
void timer_stop(struct timeval* tv);
#endif