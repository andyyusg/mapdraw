#include <string.h>
#include <stdlib.h>

#include "file.h"



void file_init(file_t* this)
{
	//zero
	memset(this, 0, sizeof(file_t));


}



file_t* file_new()
{
	file_t* this = malloc(sizeof(file_t));
	
	file_init(this);
	return this;
}




void file_clean(file_t* this)
{
	if(this->handle)
		fclose(this->handle);
}


void file_del(file_t* this)
{
	//clean
	file_clean(this);

	free(this);
}





//////////////////////////////////////////////////
// function


//return 1 if success
//return 0 if fail
int file_open(file_t* this, const char* filename, char* mode)
{
	//check handle
	if(this->handle)
		return 0;

	//set file handle
#ifdef _WINDOWS
	fopen_s(&(this->handle), filename, mode);
#else
	this->handle = fopen(filename, mode);
#endif
	//return
	if(this->handle)
		return 1;
	else
		return 0;
}


void file_close(file_t* this)
{
	fclose(this->handle);
	this->handle = 0;
}


long file_get_size(file_t* this)
{
	fseek(this->handle, 0, SEEK_END);
	return ftell(this->handle);
}


//user don't forget to free buf
buf_t* file_read_all(file_t* this)
{
	buf_t*	buf = 0;
	size_t	read_len = 0;
	size_t	read_total_len = 0;
	size_t	left_len = 0;

	//file size check
	long size = file_get_size(this);
	if(size <= 0)
		return 0;

	//set len
	left_len = size;
	
	//prepare buf
	//add 1 more char for '0' end char, for convenience of text string file read
	buf = buf_new(size + 1);
	buf->data[size] = 0;
	
	//prepare file pointer
	fseek(this->handle, 0, SEEK_SET);

	//read loop
	while(left_len)
	{
		read_len = fread(buf->data + read_total_len, 1, left_len, this->handle);
		if(read_len > 0)
		{
			left_len -= read_len;
			read_total_len += read_len;
		}
		else
			break;
	}
	buf->len = read_total_len;

	//return
	return buf;
}



//return total write len
size_t file_write(file_t* this, buf_t* buf)
{
	size_t	write_len = 0;
	size_t	write_total_len = 0;
	size_t	left_len = 0;

	//set len
	left_len = buf->len;

	//write loop
	while(left_len)
	{
		write_len = fwrite(buf->data + write_total_len, 1, left_len, this->handle);
		if(write_len>0)
		{
			write_total_len += write_len;
			left_len -= write_len;
		}
		else
			break;
	}


	//return
	return write_total_len;
}



//return total write len
size_t file_write_array(file_t* this, char* data, size_t len)
{
	size_t	write_len = 0;
	size_t	write_total_len = 0;
	size_t	left_len = 0;

	//set len
	left_len = len;

	//write loop
	while(left_len)
	{
		write_len = fwrite(data + write_total_len, 1, left_len, this->handle);
		if(write_len>0)
		{
			write_total_len += write_len;
			left_len -= write_len;
		}
		else
			break;
	}


	//return
	return write_total_len;
}
