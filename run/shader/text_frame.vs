#version 430

//uniform
layout(location=0) uniform vec2 pos_relative;

//in
layout(location=0)
in vec2 pos;

layout(location=1)
in vec2 uv_in;


//out
out vec2 uv;



void main()
{
	gl_Position = vec4(pos + pos_relative, 0.f, 1.f);
	
	uv = uv_in;
}