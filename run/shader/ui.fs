#version 430


//in
in vec2 uv;



//out
out vec4 FragColor;


//uniform
uniform sampler2D texture_sampler;


void main()
{
	FragColor = texture(texture_sampler, uv);
}