#version 430


//uniform
layout(location=2) uniform vec3 light_normal;

//texture
uniform sampler2D texture_sampler;



//in
in vec2 uv;
in vec3 normal;


//out
out vec4 color;





void main()
{
/*
	//dot(v1, v2) = |v1| * |v2| * cos
	//change range from -1 ~ 1 to 0.4 * 0.7 ~ 2.4 * 0.7
	float factor = (dot(light_normal, normal) + 1.4) * 0.7;
	
	//color = vec4(1.f, 1.f, 1.f, 1.f) * factor;
	color = texture(texture_sampler, uv) * factor;
*/

	color = texture(texture_sampler, uv);
	color.a = 0.8;
}