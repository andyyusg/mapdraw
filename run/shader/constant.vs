#version 430


//in
layout(location=0)
in vec3 pos;



//out



//uniform
uniform mat4 mat_transform;


void main()
{
	gl_Position = vec4(pos, 1.f) * mat_transform;
}
