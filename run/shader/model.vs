#version 430


//uniform
//layout(location=0) uniform mat4 mat_transform;
//layout(location=1) uniform mat4 mat_model;


//in per vertex
layout(location=0) in vec3 pos_in;
layout(location=1) in vec2 uv_in;
layout(location=2) in vec3 normal_in;

//in per instance
layout(location=3) in mat4 mat_mvp;			// 3 ~ 6
layout(location=7) in mat4 mat_model;		// 7 ~ 10
layout(location=11) in float texture_id_in;

//out
out vec2 uv;
out vec3 normal;
out float texture_id;




void main()
{
	gl_Position = vec4(pos_in, 1.0) * mat_mvp;
	uv = uv_in;
	normal = (mat_model * vec4(normal_in, 0.0)).xyz;
	texture_id = texture_id_in;
}
