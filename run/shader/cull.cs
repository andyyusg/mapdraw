#version 430

layout (local_size_x = 1, local_size_y = 1) in;

//struct
struct aabb_s
{
	//vec4 make this struct 16 bytes aligned, causing single float data misalign, so use float[4] instead
	float 	aabb_center[4];		//world space aabb center
	float	bounding_radius;
};

struct mat_s
{
	mat4	mvp;
	mat4	model;
};

struct draw_param_s
{
	uint			count;					//indices count
	uint			primCount;				//instances count
	uint			firstIndex;				//first index offset
	uint			baseVertex;				//first vertex offset
	uint			baseInstance;
};

struct test_s
{
	float			dist_xz;
	float			dist_yz;
};


//shader storage buffer object
layout(std430, binding=0) buffer sys_info
{
	//float entities[];
	mat4	mat_camera;
	mat4	mat_proj;
	float	aspect_ratio;
	float	fov_half_sin;
	float	fov_half_cos;
}buf_sys;

layout(std430, binding=1) buffer cull_info
{
	aabb_s	aabb[];
}buf_aabb;


layout(std430, binding=2) buffer mat_info
{
	mat_s	mat[];
}buf_mat;

layout(std430, binding=3) buffer draw_info
{
	draw_param_s	param[];
}buf_draw;

layout(std430, binding=4) buffer test
{
	test_s	test[];
}buf_test;


void main()
{
	uint instance = gl_GlobalInvocationID.x;	
	
	//show instance
	buf_draw.param[instance].primCount = 1;

	//mat
	mat4 mat = buf_sys.mat_camera * buf_sys.mat_proj;
	buf_mat.mat[instance].mvp = transpose(buf_mat.mat[instance].model) * mat;
}

