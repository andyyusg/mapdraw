#version 430


//in
in vec2 uv;


//out
out vec4 FragColor;


//uniform
uniform sampler2D texture_sampler;


void main()
{
	vec4 color = texture(texture_sampler, uv);
	if(color.r < 0.4f)
		discard;
	
	FragColor = vec4(color.rrr, 1);
}