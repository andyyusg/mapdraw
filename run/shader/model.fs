#version 430


//uniform
layout(location=2) uniform vec3 light_normal;

//texture
uniform sampler2DArray texture_sampler;


//in
in vec2 uv;
in vec3 normal;
in float texture_id;


//out
out vec4 color;







void main()
{
	//dot(v1, v2) = |v1| * |v2| * cos
	//change range from -1 ~ 1 to 0.2 * 0.8 ~ 2.2 * 0.8
	float factor = (dot(light_normal, normal) + 1.4) * 0.7;
	
	//color = vec4(1.f, 1.f, 1.f, 1.f) * factor;
	color = texture(texture_sampler, vec3(uv,texture_id)) * factor;
}