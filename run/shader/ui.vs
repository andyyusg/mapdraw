#version 430


//in
layout(location=0)
in vec3 pos;

layout(location=1)
in vec2 uv_in;


//out
out vec2 uv;


//uniform
uniform mat4 mat_transform;


void main()
{
	gl_Position = mat_transform * vec4(pos, 1.f);
	uv = uv_in;
}